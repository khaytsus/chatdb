#!/bin/bash

# Simple script to manually create an association between a new and an old
# nick in case you're positive a new nick is an old user you like to track
# for whatever reason but there's no direct corelation.

old=$1
new=$2

path="${HOME}/bin/chatdb/data/znc/Libera/#trolls/"
ts=`date +%Y-%m-%d`

if [ "${old}" == "" ] || [ "${new}" == "" ]; then
    echo "$0 old new"
    exit
fi

echo "[23:59:58] *** ${old} is now known as ${new}" >> "${path}/${ts}.log"
echo "[23:59:59] <zncstalker> ${old} is now known as ${new}" >> "${path}/${ts}.log"

echo "Added ${old} == ${new} to ${path}"

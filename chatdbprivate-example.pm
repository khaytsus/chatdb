package chatdbprivate;

use strict;
use warnings;

# This module you can put any variables you want to keep private and make
# sure they never wind up in a git repo and lets you share your files with
# less or no redaction.  query.pl and chatb.pl, and eveh chatdb.pm use
# this file, so they can share or have unique variables here.  Just be
# careful they don't conflict.

# Channels to exclude from results using --excludechannels in querydb.pl
# This is just a comma delimited string of regex's so it is handled the
# same way as the excludechannels parameter.
our $excludeChans = '^#secret$';

# Define the database parameters
our %db = (
    hostname     => 'localhost',
    username     => 'chatdb',
    password     => 'chatdb',
    db           => 'chatdb',
    table        => 'chatdb_sphinx',
    lastreftable => 'chatdb_lastref',
);

our $proxycheck = '1';
our $proxycheck_key="your-key";
#our $pcTwo="https://some-secondary-proxycheck.com/";

1;

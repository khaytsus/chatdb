#!/usr/bin/perl

use strict;
use warnings;
use IPC::Open3;

my $querybin = "/home/wally/bin/chatdb/querydb.pl";

# Default to 24h, but any -ts args will over-ride this
my @baseargs = ( '-ts', '24h' );
my @optionalargs = ( '-noheaders' );

# queries are formatted "-somearg value -anotherarg|Comment for query"
my @queries = (
    '-username root|Looking for root users',
    '-m mayhaps|Looking for Artfaith mayhaps',
    '-clonescan -ts 3d -clones 5|Looking for clones',
    );

foreach my $query (@queries)
{
    # Split args and comment
    my ($args, $comment ) = split(/\|/, $query);
    # Split args on spaces to create proper arguments
    # TODO: What to do if I want to have spaces in arguments, like -m 'this is a test'
    my @queryargs = split(/\ /, $args );
    print "----- $comment -----\n\n";

    local (*OUT);
    my $pid = open3( '<STDIN', *OUT, undef, $querybin, @baseargs, @queryargs, @optionalargs );
    my @out = <OUT>;
    close(OUT);
    my $output = join( '', @out );

    print "$output\n";
}

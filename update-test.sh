#!/bin/bash

testfile="${HOME}/.import_test"
stopfile="${HOME}/.stopimport"

# Allow time for the db to flush
sleep 60s

# Test our import status and notify if needed
last=`~/bin/chatdb/querydb.pl -testimport ${1} -verbose --color 0 -nolog`

rc=$?
if [ ${rc} -ne 0 ]; then
    if [ ! -e ${testfile} ]; then
        touch ${testfile}
        #last=`~/bin/chatdb/querydb.pl -last -color 0`
        echo "querydb import test has failed [${rc}]- ${last}" | mail -s "Import test has failed" wally@theblackmoor.net
        touch ${stopfile}
    fi
else
    if [ -e ${testfile} ]; then
        /bin/rm -f ${testfile}
        echo "querydb import test is passing again" | mail -s "Import test passing again" wally@theblackmoor.net
        /bin/rm -f ${stopfile}   
    fi   
fi

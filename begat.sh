#!/bin/bash

# Simple script to trace through the stalker log file to see how a nick
# came to be.  I usually use this to figure out where some "log contamination"
# happened to blacklist to clean up output or just to see how a given
# nick was associated with others in the list.

file="${HOME}/bin/chatdb/chatdb-stalker.log"

stalked=`head -3 ${file} | grep Stalking | cut -f 4 -d " "`
term=$1
blacklist=${@:2}
lastterm=""

if [ "${stalked}" == "" ] || [ "${term}" == "" ]; then
    echo "$0 stalked searchterm [blacklist]"
    exit
fi

#echo "[$stalked] [$term] [$blacklist]"

existstest=`grep -i ${term} ${file}`
if [ "${existstest}" == "" ]; then
    echo "${term} not found in log; exiting"
    exit
fi

echo -n "${term} -> "

while [ "${term}" != "${stalked}" ] && [ "${term}" != "" ]; do
    if [ "${blacklist}" != "" ]; then
        line=`grep -vi "${blacklist}" "${file}" | grep -i "${term}" | head -1`
    else
        line=`grep -i "${term}" "${file}" | head -1`
    fi
    term=""
    #echo -n "[${line}] "
    if [[ ${line} =~ .*Found\ hostname.* ]]; then
        if [[ ${line} =~ .*from\ nick.* ]]; then
            term=`echo ${line} | cut -f 6 -d " "`
            date=`echo ${line} | cut -f 7 -d " "`
            #echo -n "from hostname term: [$term] ->"
        elif [[ ${line} =~ .*from\ accountname.* ]]; then
            term=`echo ${line} | cut -f 6 -d " "`
            term="${term} (account)"
            date=`echo ${line} | cut -f 7 -d " "`
            #echo -n "from hostname term: [$term] ->"
        fi
    fi
    if [[ ${line} =~ .*Found\ nick.* ]]; then
        if [[ ${line} =~ .*from\ hostname.* ]]; then
            term=`echo ${line} | cut -f 7 -d " "`
            date=`echo ${line} | cut -f 8 -d " "`
            #echo -n "from hostname term: [$term] ->"
        elif [[ ${line} =~ .*from\ accountname.* ]]; then
            term=`echo ${line} | cut -f 7 -d " "`
            term="${term} (account)"
            date=`echo ${line} | cut -f 8 -d " "`
            #echo -n "from accountname term: [$term] ->"
        else
            term=`echo ${line} | cut -f 9 -d " " | cut -f 1 -d ")"`
            date=`echo ${line} | cut -f 5 -d " "`
            #echo -n "nick term found nick: [$term] ->"
        fi
    fi

    # If our term is repeating, short-circuit
    if [ "${term}" == "${lastterm}" ]; then
        echo "Resetting term"
        term=""
    fi

    if [ "${term}" != "${stalked}" ] && [ "${term}" != "" ] ; then
        echo -n "$term ${date} -> "
        lastterm=${term}
    fi
done
echo "${stalked}"

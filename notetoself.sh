#!/bin/bash

# Simple script to take note of something and put it in the database which
# could be found later using querydb.pl -m notetoself or -nick zncnote

note=$1

path="${HOME}/bin/chatdb/data/znc/Libera/#notetoself/"

#mkdir "${path}"

date=`date "+%Y-%m-%d"`
time=`date "+%H:%M:%S"`

if [ "${note}" == "" ] || [ $# -gt 1 ]; then
    echo "$0 \"note\" (must be wrapped in quotes!)"
    exit
fi

echo "[${time}] <zncnote> notetoself: ${note}" >> "${path}/${date}.log"

echo "Added note to ${path}${date}.log"
#!/bin/bash

# Path to our logs
parent="${HOME}/bin/chatdb/data/znc/"
#parent="/home/wally/.znc/moddata/log/khaytsus"
# Networks we use to compress
nets="Libera efnet p2p slashnet twitch Libera_Aurus"

# Get current date
date=`date +%Y-%m-%d`

# bzip2 all the things
for net in ${nets}; do
    echo "Compressing logs for ${net}..."
    cd "${parent}/${net}"
    pwd
    for file in */*.log; do
        #echo ${file}
        basename=`basename -- "${file}"`
        #echo "${basename}"
        if [ "${basename}" == "${date}.log" ]; then
            echo "Not touching todays log: ${file}"
        else
            if [ -e "${file}" ]; then
                pbzip2 -9 -f -- "${file}"
            fi
        fi
    done
done

#!/bin/sh

# Update logs

file="${HOME}/bin/chatdb/chatdb-update.log"
cleandelta="${HOME}/bin/chatdb/cleandelta.pl"

db="chatdb"
table="chatdb_sphinx"
cfg="${HOME}/bin/chatdb/.maria.cnf"

testfile="${HOME}/.import_test"

# How frequently is this ran (crontab etc), add 5 minutes for safety
freq="20"

ts=`date +"%Y-%m-%d %H:%M:%S"`
echo "[${ts}] Starting chatdb update.." >> ${file}

hour=`date +"%H"`
min=`date +"%M"`
day=`date "+%d"`

start=`date +%s`

if [ "$1" != "" ]; then
    echo "Argument passed to $0; wrong program?"
    exit
fi

logtype="znc"

if [ "${logtype}" == "irssi" ]; then 
    parent="${HOME}/bin/chatdb/data/all/"
    nets="libera EFNet p2p slashnet"
    # My irssi logs are on my local server
    rsync -va user@server:~/storage/irclogs/ ~/bin/chatdb/data/all

    echo ""

    for net in ${nets};
    do
        cd ${parent}/${net}
        ts=`date +"%Y-%m-%d %H:%M:%S"`
        echo "[${ts}] Updating ${net}.." >> ${file}
        if [ ${hour} -eq 2 ] && [ ${min} -lt 8 ]; then
            # Process all logs once a day, just in case we missed anything somehow
            # shouldn't I be nuking chatdb_lastref here for this to do anything useful?
            echo "[${ts}] Doing a full log parse on ${net}.." >> ${file}
            ~/gps/bin/orderfiles log ~/bin/chatdb/chatdb.pl
        else
            # This runs every 15 minutes, so parse every file modified within the last 20 to be safe
            find . -mmin -${freq} -iname '*.log' -exec ~/bin/chatdb/chatdb.pl {} \;
        fi
    done
elif [ "${logtype}" == "znc" ]; then
    parent="${HOME}/bin/chatdb/data/znc/"
    nets="Libera efnet freenode p2p slashnet twitch"
    # My znc logs are remote, so pull them down
    rsync -a \
          myznchost.domain.com:/home/me/.znc/moddata/log/khaytsus/ \
        ${parent}

    for net in ${nets}; do
        find ${parent}/${net} -mmin -600 -iname '*.log' -exec ~/bin/chatdb/chatdb.pl {} \;
    done
else
    echo "logtype unknown?  [${logtype}]"
    exit
fi

# Do database analysis once a day
if [ ${hour} -eq 2 ] && [ ${min} -lt 8 ]; then
    echo "[${ts}] Doing database analysis parse.." >> ${file}
    mysql --defaults-extra-file=${cfg} -D ${db} -se "analyze table ${table};"
    echo "[${ts}] Counting rows in our database.." >> ${file}
    mysql --defaults-extra-file=${cfg} -D ${db} -se "select count(*) from ${table};"
fi

# Rebuild index completely once a month
if [ ${hour} -eq 3 ] && [ ${min} -lt 8 ] && [ ${day} -eq 1 ]; then
    ts=`date +"%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] Rebuilding all indices" | tee -a ${file}
    sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --all --rotate
else
    # But any other time, just update our delta tables
    # No need to create deltas and immediately rebuild everything, so this
    # lives in an else condition
    ts=`date +"%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] Updating Sphinx delta tables.." | tee -a ${file}

    deltacount=`echo 'select count(*) from delta;' | mysql -h0 -P9306 | tail -1`

    # Update the Sphinx delta index
    /bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate delta >>${file}
    /bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate nickdelta >>${file}

    # Must sleep 1s or we get the same value each time.  Might need longer for some people
    sleep 1s
    newdeltacount=`echo 'select count(*) from delta;' | mysql -h0 -P9306 | tail -1`
    count=$((newdeltacount-deltacount))

    echo "Sphinx delta count went up ${count} (${deltacount} to ${newdeltacount})" | tee -a ${file}
fi

# And if it's 3am and not first of the month, just merge the deltas
if [ ${hour} -eq 3 ] && [ ${min} -lt 8 ] && [ ${day} -ne 1 ]; then
    ts=`date +"%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] Merging Sphinx delta tables.." | tee -a ${file}

    # Merge the delta into the primary index
    sphinxcount=`echo 'select count(*) from message;' | mysql -h0 -P9306 | tail -1`
    /bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate --merge message delta --merge-dst-range deleted 0 0 >>${file}
    /bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate --merge nick nickdelta --merge-dst-range deleted 0 0 >>${file}

    sleeps 1s
    newsphinxcount=`echo 'select count(*) from message;' | mysql -h0 -P9306 | tail -1`

    count=$((newsphinxcount-sphinxcount))
    echo "Sphinx message count went up ${count} (${sphinxcount} to ${newsphinxcount})" | tee -a ${file}

    # Update the sph_counter so our delta resets
    perl ${cleandelta}
fi

# Test our import status and notify if needed
~/bin/chatdb/querydb.pl -testimport --color 0
rc=$?
if [ ${rc} -ne 0 ]; then
    if [ ! -e ${testfile} ]; then
        touch ${testfile}
        echo "querydb import test has failed" | mail -s "Import test has failed" you@yourdomain.com
    fi
else
    /bin/rm -f ${testfile}
fi

ts=`date +"%Y-%m-%d %H:%M:%S"`
# Get end time and calculate how long it took
end=`date +%s`
runtime=$((end-start))

echo "[${ts}] Chatdb update completed, took ${runtime} seconds" | tee -a ${file}

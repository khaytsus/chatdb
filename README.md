Store IRC logs into a database for easy querying and analysis using irssi, znc, and MySQL/MariaDB

---

## Overview
This is a collection of Perl scripts intended to collect IRC logs into a single database and make it easy to query those logs for specific data or to analyze them for patterns such as tracking nick changes or simply to find something you recall someone mentioning without grepping a bunch of log files.

## Scope
Short term this is specific to my irssi, my logs, my formats.  But I've attempted to make things where you can drop in new regular expressions and it should just work for different log formats.  How easy that would be to use in an entirely different irc client I don't know but I'm certainly willing to take pull requests if you add a section for your client.

Support for ZNC's log file format is usable, but not completely finished, tested, and documented yet.

## Collection of Files
The collection consists of multiple files to accomplish tasks, separating out the data collection bits from the data analysis bits.

* chatdb.pl 
  * Parses a filename passed to it and inserts the data into the database
* querydb.pl 
  * Queries data back out of the database
* chatdb.pm 
  * Contains example database connection data and my personal irssi regular expressions
    * Yes, I realize it has database info.  It's not real ;)
  * Also contains many shared subroutines that chatdb.pl and querydb.pl need
* begat.sh
  * Handy script to find the lineage of a nick from stalker output, usually to clean up the results and add a blacklisted nick/host in a query.
* bin folder
  * Examples of scripts I use to update the database (see more below)
* Examples folder
  * Examples of preconfigured configs

## Database storage tool high-level features/info (chatdb.pl)

* Handles duplicate lines (ie:  errors by irrsi) by creating a unique hash
  * Duplicate hashes are not added to the database
* Creates the database table if it does not exist
  * Must already have the database created, permissions granted, etc
  * Creates required indicies
    * MySQL Full text message index is not default but is easily added
* Stores all supported records in the database
* Logs information to a file

### Database storage tool usage

Importing log lines is heavily dependent upon the log format.  ZNC (1.6.x?) is supported, as is IRSSI using the "Angelic" theme.  I am 95% sure that the theme affects the logging format in IRSSI, which makes this all a mess.  It may be worth looking at the log_theme option in irssi and creating a theme separate from the client theme in case you wish to change it in the future but not break the log parsing.  Creating new regular expressions isn't difficult but it's tedious.  There are also other minor differences between different log types, such as the maximum nick length stored in IRSSI is different than that of ZNC etc.  All of this is defined in chatdb.pm

FYI, theoretically you could import from multiple sources without duplication.  However the reality is if your ZNC is on a different host than your IRSSI, the timestamps might vary slighty.  Or the nick length stored might vary slightly, etc.  Which will lead to duplicates in your database.  So I recommend sticking to one method.  If you switch, only import new data, do not parse data which was already imported from the other log type.

As for duplication; as I mention in the section below this program handles duplicate data being fed into it as long as it is an exact duplicate.  For example, when storing a line a user said, it takes the nick, message, channel name, date, and timestamp and combines them into a string then creates a checksum from that string using base64'ed MD5.  In my testing, I found that I could save a reasonable amount of disk space without false duplicate matches by using the first 10 characters of the hash, so chatdb.pl stores only the first 10 characters in the database.  When storing the lines, chatdb.pl does not attempt to check for an existing hash, it just lets MySQL reject the line as the hash is a unique key index and MySQL handles this itself.

#### chatdb.pm 

You will need to update chatdb.pm to match the database credentials you have set up and if you are not using or have not set up Sphinx you *will* need to set the $useSphinx variable to 0.

#### Importing data

The method to import the data depends on the data itself, so I have broken how I import from IRSSI and ZNC below.

For both examples I am using rsync to get the data to the local machine, because both irssi and znc run on a different machine than my workstation where I store the database.  irssi is within my home network, znc is on a remote server.  Setting up ssh keys is essential to make this work automatically if you are setting things up this way.

In general how this all works is every time you parse logs, you're most likely re-parsing already imported logs which are ignored if they already exist in the database and any new lines are added.  This isn't the most efficient method, but given irssi maintains a log file for a month and znc for a day this is the best I could come up with.  I may in the future improve this area if I think of a better way to do it.

To minimize the logs I'm parsing, when we hit a new months I (currently manually; script to come!) bzip the old log files so that the script only parses the most recent files.

The import scripts are somewhat smart in that they only parse recently modified files but generally it still means every time the script runs it's parsing logs for every channel you're in.

I have an example file for irssi and znc, as they work slightly different from each other.

And you need to set the type of log in chatdb.pm so that it knows how to parse the filename as well as the specific log regular expressions and other things which differ between irssi and znc.

##### IRSSI
irssi logs contain everything they need inside of the log filename or the logs themselves for network, channel, date, etc.  So you can parse the files directly using chatdb.pl with no additional metadata needed.  So the update-irssi.sh mostly just automates the rsync and passing the log files into chatdb.pl

For initial testing, set-up the chatdb.pm correctly and run chatdb.pl with the log filename, such as ./chatdb.pm "#fedora freenode2018-10.log"  

##### ZNC
ZNC logs you need to take the path itself into account as well to know the date, channel etc.  So in the example update-znc.sh the find command is used to pass the full path and filename into chatdb.pl so that it may parse it properly.

For initial testing, set the chatdb.pm correctly and run chatdb.pm with the log filename **including** the full path, such as ./chatdb.pm /home/wally/bin/chatdb/data/znc/home/wally/.znc/users/khaytsus/moddata/log/#fedora/2018-10-01.log

##### Log file paths
In general you'll likely have to tweak a few things to make sure that the script gets the correct date, network, and channelname.  Depending upon where your logs come from, your log formats, etc, this may vary.  You may have to update one or all three of the following in chatdb.pm:

The variables fileInfoInPath and logFileRegex as well as the logic in the function getInfoFromPath.

As I get more feedback from folks using these scripts I continue to try to make them more generic and more easily configured.

### Database tests

I'll be creating some rudamentary tests to make sure I don't break anything, but for now I have some manual tests.

## Query tool high-level features/info (querydb.pl)

* Query the database by server, channel, time, nick, username, hostname, message, network, whatever using command-line flags.
  * Command line flags are parsed in context of meaning, such as --message='%term%' will do a LIKE query, where --message='\*+term\*' will do a MATCH query (requires fulltext index)
  * Another example; --timestamp="2018-10-01" for that specific date, but fuzzy thigns like --timestamp="10 days" will create a range of dates
  * -ts 10d will also do the same thing but more abbreviated
* Search around a matching result to see N lines before and after for that channel
  * By default shows 'blocks' of text, but optionally fill in the gaps to get a complete picture of the results
* Search for corelations around nick (like stalker.pl for irssi)
  * Corelates nicks, hosts those nicks have used, and then new nicks from those hosts
  * Recurses until it finds no new data or runs out of iterations
  * Very handy tool; but results must be analyzed to be trusted (chatdb-stalker.log)
* When searching for nicks that are the maximum possible length, warn the user
  * Public messages have different max than Joins, irssi has different than ZNC, etc
* Pretty print the output (more or less same format that irssi outputs)

### Query command line arguments

Perl's getopt is pretty flexible.  Each of these arguments can be passed by --argument data, --argument='data' -argument="data", or even -arg data as long as "arg" is unique.

If an argument supports % matching, this means that the SQL query executed will be a LIKE statement, ie:  a substring.  This will likely slow down the query but is necessary to do in some cases.  Most things support % which make sense, like nick, message, hostname, etc, so I am not explicitely calling them out in each one below.

For message specifically, using \*term\* is a special type of search that's only valid if there is a full-text index for the message column and in which case it does a MATCH(message) AGAINST(term) which is much faster than a LIKE '%term%' query.  See the MySQL Full Text Index section

By default, all data output is "pretty printed" in a log-like format to make it easy to read.

The first list is about querying specific IRC log data

* nick
  * You can query for multiple nicks by separating with a comma
  * If nick argument is equal or longer than the max nick length, script will warn you.  You may add -nt to the command line to automatically truncate the argument down to maximum size
    * ie:  -nick thisisareallylongnick would never match because it's simply not stored in the database.  If nick max is 11, -nt would truncate this effectively down to -nick thisisareal which is what would have been stored in the DB
    * Keep in mind that JOIN/PART, potentially others, have a different limit
  * n is a shortcut
* filternick
  * Nick to filter from results
  * Multiple nicks can be separated with a comma
* message
  * Message to search for, partial searches if using Sphinx are default, if not using Sphinx use match
  * m is a shortcut
* match
  * --match '+term' is the same as --message '\*+term\*' and is used for the MySQL fulltext index if in use
* weight
  * --weight 1 will instruct querydb.pl to use Sphinx's search weight results as the sort of the returned message id's and therefore the order of returned lines
    * By default it sorts by date as this is typically more relevant for this program
* sphinxresults
  * Over-ride the maximum sphinx results defined in chatdb.pm
* channel
  * Narrow results to specific channel
  * Most likely need quotes in shells, such as '#channel'
  * ch is a shortcut
  * You can query for multiple channels by separating with a comma
* orignick
  * The meaning of this column in the database is primarily for nick changes, such as old->new, "old" would be the orignick
* username
 * Search by username
* account
  * Search by account name.  Empty string will show results without accounts
* hostname
  * You can query for multiple hostnames by separating with a comma
* fuzzyhost
  * Replace components of hostname with wildcards
  * Attempts to do intelligent things with IPv4, IPv6, or normal hostnames
* network
* action
  * These are defined as $actionenums in chatdb.pm, JOIN, PART, SAY, etc
  * A comma separated list is accepted here, such as 'say,emote'
* say
  * Shortcut for --action say
* actionnick
  * Whom an action is performed against, such as a KICK, the person kicked would be stored in actionnick
* nojoinpart
  * Hide all join/part/quit messages
  * nj is also an alias for nojoinpart
* nonick
  * Hide all nick changes
* ignoredonly
  * Limit results to ignored users
* highlights
  * Highlights terms or phrases defined by commas
  * Matches phrases in messages or exact nicks
  * You can predefine terms in the module in @highlightWords or pass space or comma separated words which over-ride the defaults
* highlightsonly
  * Shows only messages with highlight words included
  * Cannot be used with the -message parameter, this over-rides it
* excludechannels
  * Exclude a comma-separated list of regex's to filter out returned channels
* sql
  * Allows you to specify freeform SQL

This is a list of arguments primarily about narrowing search results

* timestamp
  * Also has alias of "ts"
  * Supported input
    * YYYY-MM-DD
    * Fuzzy dates (see below)
* tsrange
  * --tsrange="2018-10-01 2018-10-15" would limit the results to those dates, inclusive
  * A single YYYY-MM-DD value would do a search from that date until today; ie: if no end date is specified it defaults the end date to "today"
  * 2018 is shorthand for "2018-01-01 2018-12-31", ie: all year
  * YYYY-MM-DD HH:MM:SS YYYY-MM-DD HH:MM:SS is supported for very specific time ranges as well
  * HH:MM:SS HH:MM:SS will capture a time range assuming today (will also back up to previous day as needed)
* today
  * Shortcut for -ts today
* 1hour/onehour
  * Shortcut for -ts '1 hour'
* tzadjust
  * Adjust the timestamp shown in results.  Must use the UTC offset such as '-0400' or America/Louisville' type of format
* relative
  * Show the relative time delta in lines to make it easy to see how long ago the line occurred
* aroundmins
    * If specified, show results from the given timestamp with aroundmins time before and after
    * For example, this can also handle simple timestamps such as -ts 13:00 -aroundmins 15 to create a range from 12:45 to 13:15 on todays date

Search for a conversation between two nicks
* convo
  * Search for conversations between two nicks, ie:  Nick1 or Nick2 mentions Nick1 or Nick2.  It's not perfect, but can help find conversations between two nicks.  Requires Sphinx.

This is a list of "utility" arguments, or specific ways to manipulate output

* around N
  * Shows N lines of context around matches
    * See examples for a sane explaination of what this does, but basically lets you see what happened before and after a match or matches in the database
    * Limited to a default in chatdb.pm in the maxrows variable; ie:  if more than N matches you will need to narrow your query further
  * Optional argument -aroundchat
    * More or less removes limitations in results
    * Stops showing results in "sections" and rather shows one continuous output
  * Optional argument -aroundmins
    * Default is set in chatdb.pm to 120, but you can change this to any amount of time you need, such as in a fairly quiet channel or slow responses
* order
  * specify the column to use to order the results
  * Can also do multi column, like -order 'channel asc, timestamp asc'
    * This would sort by timetamp but keeping channels together
  * Alias:  sort
* ascending
  * Sorts in ascending order
* descending
  * Sorts in descending order
* channelgroup
  * Groups results into channels
  * Can be very handy to review activity over the last N hours across all channels (with example excluding a few that you don't care about), such as..
    * querydb.pl -ts 4h --channelgroup -action 'say,emote' --excludechannels '#one,#two,##three'
* anonymous
  * Anonymizes nicks, could be useful to pastebin data without including specific nicks
  * Does not attempt to anonymize accounts, hostnames, nor any nicks within messages
  * Most likely best to use this on say or -nojoin etc to avoid expected data leakage
  * Does NOT do anything to the message portion, only the nick from the SQL database, so there could be cases where corelation of nicks is possible.
* colornicks
  * Defaults to colorize nicks consistently, uses 256 colors, 0 to disable
* dumpactivity
  * Dump activity of the results of the query, such as from a stalker result
  * Currently works in stalker and most normal queries
  * If you want to restrict the "secondary" results pass the intended action to -dump, such as -dump say would filter to action=say

Abuse Detection
* clonescan
  * Find hostnames with multiple nicks coming from it
  * Can also be used to find users bouncing in a specific channel, example
    * querydb.pl -clonescan -ts 1d -clones 5 -channel '#channel'
  * clones option adjusts how many nicks are required to return a result
    * Default clones is defined in module
  * Timeframe must be limited
* proxycheck
  * Must be configured in module, but enable or explicitely disable proxychecking
  * Also has pc alias
Stalker specific arguments

* stalker
  * Corelates nick and hosts to come up with a list of all nicks this user has used
  * See Stalker section for complete info
    * Can narrow query timeframe with --timestamp, --server etc
* blacklist
  * Space separated list of nicks and/or hosts to blacklist when using stalker
* noblacklist
  * Disable all blacklisting
* maxhosts
  * Over-ride the default maximum hosts to match when stalking.  Default is defined in chatdb.pm
* maxnicks
  * Same as maxhosts; limit the number of nicks we find and over-ride the chatdb.pm default
* maxloops
  * Over-ride the number of iterations to look for new nicks and/or hosts when stalking.  Default is defined in chatdb.pm
* deep
  * Perform a deep stalker query that keeps looking for more nick changes
* onlynicks
  * Does not attempt to find more nicks based on joins (ie:  Nick changes only)
  * Defaults to perform defaultDeepLoops (in module) nick lookups, specify a value to over-ride
* onlyhosts
  * Does not attempt to find more nicks based on nicks (ie: Joins only)
* cleanstalker
  * Wipes the stalker log before stalking; makes tracing nicks/hosts to blacklist etc easier
* excludeage
  * Set/override module setting for excluding Stalker results due to their age
* maxage
  * Set/override module setting for maximum age to use for Stalker

Similar nick search

* nicksearch
  * Does a soundex query on the passed in nick to find similar nicks, such as if you know someones nick was close to "kaytsus" it would most likely find "khaytsus" and other similar nicks which can help you find the exact nick you're looking for
  * This is a stand-alone query; does not feed into any other queries.  IE:  Use this to find an exact nick, then query using the nick you found in subsequent queries.

Some high level statistical queries

* ignorestats
  * Find some basic stats on an ignored user, like the first line seen when they were ignored
  * Passing the --around N parameter will also show lines around the result
* nickstats
  * Prints information on all of the various actions nick has done (slow!)
  * Does not work well with truncated nicks
* listchannels
  * Prints list of channels we have seen this nick in (slow!)
* channeluserstats
  * Dumps a list of user stats for a given channel; ie:  top talkers
  * It collapases Nick and Nick_ together, but that's as smart as it gets
* channelpercent
  * Percentage of users to consider when calculating users to evaluate for channeluserstats statistics
* userchannelstats
  * Dumps a list of channel stats for a given user; ie:  top channels talked in
  * If you pass --userchannelstats "*"; ie a * instead of a nick, it will show activity for all users and give overall channel stats
* activitystats
  * Statistics on user, channel, or entire network activity (can be very slow!)
    * Years, months, days, days of week they are actively
    * Nick, channel, or network must be specified with their appropriate parameters
    * If network is specified the statistics will be for the given network
    * Can narrow query timeframe with --timestamp
* commonuserstats
  * Common users between two channels
    * '#chanone, #chantwo'
* countsounds
  * Count sound byte commands in channel
* stats
  * Prints overall database and table statistics

Other misc arguments

* columns
  * If you only want specific column(s) output you can specify them here
    * --columns="nick,message"
  * When specified, no "pretty printing" is possible
* first
  * Prints the first N matches of a result, ordered by date
  * Defaults to 1 if no value given
* last
  * Prints last N matches of a result, or the last lines input into the database if used by itself
  * Defaults to 1 if no value given
* verbose
  * Increase verbosity of output
  * Currently used with stalker to provide more verbose output and will add the list of nicks found in whatever other query executed.
* terse
  * Decrease verbosity of output
  * Currently only used with stalker to provide nick1,nick2,nick3 type output to use for additional queries easier
* sphinx
  * This will enable or disable the use of Sphinx at runtime.  The default is in chatdb.pm.
* color
  * This will allow you to disable color printing of lines.  Will not print color unless Term::ANSIColor is installed.
* limit
  * Changes the limit on how many rows of SQL data will be retrieved.  The default is stored in chatdb.pm
* testimport
  * Returns error code if the last line imported is older than maxImportDelta defined in chatdb.pm.
  * Optionally give a numeric value to extend the amount of time allowed, primarily to be used after doing a full Sphinx rebuild or some other time-consuming operation
* debug
  * Enable debug output
  * Stalker sql debug output at > 10
  * Stalker verbose debug output at > 40
  * Only shows the buildSQL SQL statement and exits at > 99
  * More coming sometime
* nolog
  * Don't log query into query log, useful for automated scripts to not pollute log with automated runs
* noheaders
  * Don't print headers/footers, mostly useful for running queries with external scripts or tools and just showing results
* noprint
  * Don't print rows, just return rows matched.
  * Can be used to do queries such as how many lines said in a channel in a year without the overhead of actually printing them.
  
#### Pre-configured configs

If you do a particular query a lot, you could create a module defining the necessary parameters.  You can also add more parameters to the command line; the ones in the module will be defaults but can be over-ridden as needed.  An example module in the Examples folder which shows the last thing the nick 'khaytsus' did.

You use the modules like the following; where the file 'khaytsus.pm' is in the current directory.  You can specify the full relative or absolute path to a module as needed.

`./querydb.pl -config khaytsus.pm` 

And as mentioned, if you need to modify it you can also do that.  In this example, we over-ride the nick to be 'SomeoneElse'

`./querydb.pl -config khaytsus.pm -nick SomeoneElse`

### Query command use cases, examples

The following are just some random use cases or samples of usage

#### Find a message I (khaytsus) said mentioning spam in the ##hamradio channel in the last 7 days, and show me the context of chat around it, using the * matching (requires fulltext index of message; alternatively use '%spam%')
`./querydb.pl -nick khaytsus -channel '##hamradio' -message '*+spam*' -timestamp '7 days' -action say -around 15`

#### If using Sphinx, show messages that have the exact phrase "I love Android" but not including the word "Studio"
`./querydb.pl -message '"i love android" -studio'`

#### Find the last time khaytsus did anything
`./querydb.pl -nick khaytsus -last`

#### Find the last time khaytsus said anything
`./querydb.pl -nick khaytsus -action say -last`

#### Print the last time someone was quieted in #fedora
`./querydb.pl -channel '#fedora' -action mode -message '+q'`

#### Someone mentioned something about going into work late because they slept too late?  Who was it? (Requires full text index)
`./querydb.pl -message '*+sleep +work +late*'`

#### Stalk khaytsus, but blacklist stray nick he shared with someone
`./querydb.pl -stalk khaytsus -blacklist 'SomeOtherGuy'`

### Stalker

Stalker originates from an IRRSI script [symkat.com/stalker](http://www.symkat.com/stalker) that I have used a lot over the years and tweaked a bit (I have my tweaks in my gitlab repos but no use linking it here) and basically it does the following..

Take a nick, and find all hosts this nick has ever connected to IRC with.  Take that lists of hosts, and find all nicks that have used those hosts.  Now repeat this process until you find no new data or you exceed the limit of iterations you've allowed.  chatdb does this by storing all join, parts, and nick changes in the database with the nick, original nick, hostname, etc.  querydb then queries that data and puts it together.

MASSIVE CAVEAT:  You **must** do some sanity checking on the output to make sure it's sane before you treat the output as valid.  Here are a few examples that could make the data invalid.

* If a lot of users come from a shared host it's going to look like all of those users are the same person
* If someone on two different networks share a nick that could make it appear weird
* If someone jokingly changes nicks to someone elses momentarily
* If your dataset is huge (mine is 11+ years) people retire nicks, someone else uses it, etc

And if you aren't seeing enough results, the deep flag can add more by doing additional recursion on finding nick changes based on the current array of nicks on every pass.  But be warned this can very quickly turn into an insane list.  You will need to analyze the output, blacklist, and possibly reduce the maxloops to a lower number to get sane output.

If you're trying to analyze a nick who just changes nick a lot, you can try the nicksonly parameter, possibly paired with the deep parameter.  This will return ONLY nick changes and ignore all joins, so no host corelation is performed.  You may still need to limit maxloops.

There are ways to mitigate these issues that might cause false-positives.  The first way is to use the verbose flag to get more information from the stalker data.  This will show how many days ago the nick was last used and some other information.  Any nicks that are older than stalkerMaxAge are marked with an asterisk (\*) to make them stand out.

If the results are really old it might be best to modify chatdb.pm to eliminate old results using the stalkerAgeExclude and associated stalkerMaxAge variables.  I personally am not exlcuding any results, but I do set the stalkerMaxAge to a reasonable value and then when I look at the ouput I can make a decision which nicks are probably okay if they're not marked as too old.  However, some people use the same host and/or nick for years, so it can be handy to look at pretty old data.

If you do set stalkerAgeExclude, you can over-ride the preset value of stalkerMaxAge by specifying a timestamp range in the query.

You can also look at the chatdb-stalker.log output to see where the results came from.  It provides a great deal of detail, including SQL queries, that you can use to really dig into the output.

One more thing you can do to minimize some results that look questionable is limit maxhosts to a smaller number, say start with 2, see what that looks like.  Another thing to try is to restrict stalker searches to a specific network.

There's also a few variables you can modify to blacklist nicks (nickBlackList) and hostnames (hostBlackList) in chatdb.pm which will ignore nicks and/or hosts that are included in thost lists.  hostBlackList is especially useful for shared hosts, such as irccloud is not unique per user therefore makes the results of its irrelevant for Stalker.  nickBlackList can also be handy for common nicks that people have shared either as a joke, inadvertently, etc.

You can provide a blacklist at runtime with the blacklist parameter rather than modifying the module each time.  If you need to blacklist multiple things, quote them and separate the entries with commas, such as -blacklist 'nick1,nick2,domain.com' would blacklist those three entries from any matches on nicks or hosts.  If you want to disable all blacklisting, -blacklist 0

You can also narrow the timeframe of the results by using --timestamp like you would in any other query.

Another way you can limit the query in some specific situations is by using --channel.  This is not a typical use case but can be handy.

And if you would like to dump the activity of all returned nicks you can add the -dumpactivity parameter which will drop all found nicks back into buildSQL and do a normal query against those nicks.

If you would like to limit the "secondary" query; ie:  the results against the list of nicks found, you can add the sql_action as a -dump parameter, such as -dump say.  

#### How to find overlapping data in Stalker that is likely incorrect

This is tricky..  An overlapping host or nick can create a confusing list of nicks in your output.  The trick is understanding when this is intentional (be it a user who just likes changing nicks or a troll who thinks changing their nick hides them) or when it's an unintentional overlap.  Most often when I see a pattern or nicks and/or hosts drastically change there is an overlap I need to ignore or temporarily blacklist.

The first thing to do is to restrict the search to a single network and see what the results look like then.  I would not recommend doing this all the time, as often the folks you're wanting to "stalk" are on multiple networks (trolls, etc) but you'll also find common nicks reused on different networks.  And the results are sorted in newest to oldest; if you're seeing oddball nicks that are a few years old they might not be valid.  Hard to tell without careful analysis of the chatdb-stalker.log

If you're still seeing bad results the best thing to do is look at the chatdb-stalker.log and starting from the top look at the hostname matches for the initial nick.  From there the script looks for nicks who have used those hosts.  This repeats until no new data is found, so keep looking down the log until you find something that seems odd.  Often you'll find a shared hostname which then joins those sets of nicks from that hostname together.  A nick will do the same; especially a nick like "Test" or such will likely overlap with many other nicks over time.  If you find a common nick or hostname that you never want to be used, add it to the module.  Otherwise, you can use the blacklist parameter to clean up the stalker output and see if it looks more reasonable.  Sometimes getting rid of a single nick can make the output better.

What I tend to do is when I look up a nick and see unexpected nicks, I grep for that unexpected nick in the chatdb-stalker.log and find where it came from, then grep for where it came from, etc, keep going until you trace it all the back up the line.  If you decide it came from somewhere you don't think is a real corelation, add it to the blacklist.  Sometimes it can take quite a few items in a blacklist to really get a clean set of nicks if that is what you are after, such as identifying a known troll etc.

### Fuzzy Dates

The timestamp (or ts) parameter will accept a limited set of fuzzy inputs to make limiting queries by date a bit quicker than the tsrange parameter.  I find myself using the "n days" more than anything else, but some of the others may be useful.

* n hours
  * Specify how many hours back to query, including this hour
* n days
  * Specify the number of days to go back, including today
  * 1 days would be all day yesterday as well as today
  * 0 days would be just today, or use 'today' instead
  * 365 days would be for the past year, etc
* n years
  * Same as days, except in years
* today
  * Only results from the current day
* yesterday
  * Only results from yesterday, midnight to midnight
* n weeks
  * Results from the last n weeks, including today
* last week
  * The previous calendar week; Monday to Sunday (**not** the last 7 days)
* this month
  * The current month, including today
* last month
  * The previous month
* Specific dates
  * Examples: 2022, 2022-08, 2022-08-01
    * Handles padding, so 2022-8 works fine as well
* Specific date ranges
  * "2022-08-01 2022-08-15"
  * "2022-08-01 12:00:00 2022-08-01 14:00:00"
    * Note that the full YYYY-MM-DD HH:MM:SS is required in this format

You can also use some abbreviated shortcuts, such as 2y would be 2 years.  The complete list is:  y (year), mo (months), w (weeks), d (days), m (minutes).  Note that months is mo and minutes is just m because I tend to use minutes more often.

## Colorizing output

If you want really obnoxious colored output you can define that in chatdb.pm.  We use references like $c_c so that Cyan can be Dark Cyan or Light Cyan, depending upon if $lightterm is defined, etc.  $lightterm lets you define if your terminal is a light background or a dark background which should let you toggle depending on your preference and have text with the right color/brightness.  If you do not want to colorize anything just set coloroutput to 0.  If you don't want to color one particular thing, such as for me I do not want to color the typical message text output, set the variable to undef.  This way you could colorize everything, or just specific things, however you want to set it up.  The colors of the $c_X variables are defined in the colorTags function for reference.  If you would like to disable color output at runtime, you can use the --color 0 argument.

## Sphinx Full Text Search

Sphinx is an external search engine which can integrate with other databases in a variety of ways.  I am using sphinx.conf, running the indexer on updates, and then using SphinxQL in querydb.pl to retreive the results.  Sphinx is significantly faster than MySQL's fulltext search, it has a richer search syntax, and it uses less ram.  MySQL's fulltext search is quite fast as long as MySQL is given lots of ram and the fulltext index is in ram; however, it can be pretty slow on initial runs or when there have been long periods between runs.  Sphinx is fast every time.

Setup isn't complicated using an example I've put in the Sphinx directory.  Sphinx does require an 'id' field that is an auto incrementing primary key which I did not originally have in my database schema so any existing databases this would need to be added.  It is created from scratch with the 'id' field for new databases.

### Setup Sphinx

dnf/yum/apt install sphinx, it is likely included in your distrbution already.  For Fedora 29 it is using version 2.2.11 which is an older version as the 3.0 source does not appear to be available yet.  While 3.0 appears to have improvements, I am using 2.2.11 for now.

Look at the sphinx.conf in the Sphinx directory.  It matches the chatdb.pm example for the database in use, so make updates to the host/user/pass/db/table etc as appropriate for your setup.  You will also need to update the path where the Sphinx databases are written in the index sections.  I have several SSD's on my machine, some faster than others, so I picked the fastest one for my index.  

Beyond that I recommend initially not touching much else.  If you are an expert with Sphinx or become one and have suggestions on improvements the the config and/or usage please log an issue in gitlabs as this is the first time I've ever used it.

Place the sphinx.conf in /etc/sphinx or wherever your distribution suggests, start the searchd service which is the service used to query the Sphinx databases.  And of course, set searchd to run at start for your distro.

You will then need build the initial index.  For Fedora, the Sphinx process runs as the 'sphinx' user, so I suggest using su/sudo, such as `sudo -u sphinx /bin/bash` to become that user so permissions are preserved then run:

`indexer -c /etc/sphinx/sphinx.conf --all --rotate`

For me this only takes 3-4 minutes but the size of your database, disk speed, and computer resources will affect this.  If it succeeds, try some querydb.pl and see if it's working.  If Sphinx is enabled in chatdb.pm it will tell you in the results how many results Sphinx returned.  You can play with the -sphinx parameter to turn Sphinx on/off to see how the speed varies if you like.

### Maintain Sphinx database with new chat data 

There are quite a few ways to keep Sphinx updated with data from a MySQL database but I picked the method which is recommended for large datasets.  This creates an index of the last id which was indexed, creates a delta index of the new documents.  You then query both main and delta index for results, which keeps results complete and makes updates super fast.

This command makes a delta of the latest changes.

`/bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate delta`

Every so often you do want to merge the delta into the main index, so I do that nightly in the update script if the time matches, it runs this.

`/bin/sudo -u sphinx /bin/indexer -c /etc/sphinx/sphinx.conf --rotate --merge message delta --merge-dst-range deleted 0 0`

And after the delta is merged into the main index, you need to reset the latest document reference so the next time the delta runs it only picks up new documents.  Unfortunately Sphinx doesn't seem to have a concept of post-merge SQL, so we have to manually do this which I put in a small Perl script which calls cleanDelta in our module.

`perl cleandelta.pl`

I suggest running these by hand at least once to make sure they are operating as expected.  And look at the chatdb-update.log and chatdb.log for any errors, etc, as you set up, tinker, etc.

Note that again, I am using sudo to run indexer so it is ran by the Sphinx user.  You will need to update your sudoer's configuration with something like below for this to work in any cronjobs or automated processes.  I am using the wheel group but you could set it for your personal user as well.  If you have set up sudo to not need a user password at all then you don't have to do anything to sudoers.

`%wheel  ALL=(ALL) NOPASSWD: /bin/indexer`

### Caveats/things to keep in mind with Sphinx

#### Maximum Results and timestamps

There are a few things to remember when using Sphinx.  Since it is a separate entity and they are being filtered in different ways there could be times in which you are artificially limiting results in Sphinx that might not be expected, such as searching for a generic term that a specific user said.  Sphinx will return $sphinxResults number of results (2000 by default) and then those id's will be included in the MySQL query to limit them to only the user you were interested in.  However, if that user also said that query in earlier Sphinx results, you would not see those results.

Said another way; when we ask Sphinx for a result if a time span is included in the query Sphinx will limit the results to that timespan.  But Sphinx does not otherwise limit to other criteria, such as the user, channel, etc.  That is done in MySQL.  So if the term you're wanting to search for is very generic or widely used and you want to find it in a specific context (from a user, in a channel, etc) you'll very likely have to increase the sphinxresults.  Otherwise Sphinx will return 2000 results and those which ALSO match your nick/channel/whatever criteria may not include what you would expect.  You can of course increase the default sphinxresults if you prefer to avoid this issue with the slight tradeoff on speed.

In reality most often one will be limiting the timestamp of a result, but in the case that you might want to see every time you have said the word "drat" or something silly, you can use the sphinxresults flag on querydb.pl and increase the maximum number of Sphinx results.  And of course the better the search terms the more likely to narrow the results in Sphinx to fall below this default of 2000 results and therefore passing all known data into MySQL.  You could also increase the default but that increases time searching in Sphinx as well as time searching in MySQL.

#### Sometimes, grep is better

Sphinx results are very fast to return; meaning that if you are unsure of other parameters to narrow something you're looking for, such as the nick, channel, etc, you could always try just using grep to futher narrow results.  For example I know someone said a phrase, and I know they said Android in it somewhere.
`./querydb.pl --message '"thing I'm looking for"' | grep -i 'android'`

#### Sphinx finds something, but nothing is returned

querydb.pl tells you how many results Sphinx gave as well as how many matches in MySQL there were.  If you restrict other parameters (such as nick, channel, etc) you can sometimes wind up with Sphinx saying it returned results, but overall 0 rows returned.  This means the Sphinx index found something, but you filtered out those results with other parameters.  You could try less parameters, or grep, etc.  It very well may mean that combination of parameters was never logged.  Often I think I know who said something or where it was said but I'm wrong; so per the section above if all else fails, just try grep or a pager and no other restrictions.  If you get too many results from Sphinx you can increase the results or restrict the timeframe.

### Remove old MySQL fulltext index

Don't forget that if you're switching to Sphinx you can get rid of the MySQL fulltext index you previously used.  This will make importing logs more efficient, use less disk space, etc.  You however could keep it around if you prefer, such as to validate Sphinx is doing what you expect etc.  Sphinx does more than MySQL's fulltext matching as far as I know, so there's no need to keep both long-term.

### Convert existing database

If you have a database createdprior to Sphinx support and you want to use Sphinx, you will need to add an 'id' column to your table.  If you do not plan on using Sphinx there is no reason to do this.  chatdb itself does not use the id column unless Sphinx is actively being used.

Back up your existing database and make sure any cronjobs updating it automatically are disabled as this will take a while.  Make sure you don't lose the UTF8 setting on the database if you do this on a copy; my first stab was just to create the new schema then insert rows from the old one, per some Google results but that wound up being LATIN1.  Likely user error; just be careful.  Don't want to spend a lot of time on this if you have potentially lost data in the process (UTF8 -> LATIN1 could mangle text, etc)

You need to do this in two steps or you might run into weirdness, at least I did.  Both steps will take pretty long; I think it was at least an hour for each step for me with around 5.5 million entries in my database.  The first one adds the 'id' column and makes sure it's the first column in the table which is what Sphinx wants.  The second sets it to auto increment etc and as a primary key.  Of course execute these on your chatdb table you're using for Sphinx.

 * ALTER TABLE chatdb_sphinx ADD id INT(11) FIRST;
 * ALTER TABLE chatdb_sphinx MODIFY id INT(11) NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY;

## MySQL Full Text Index

MySQL offers a built-in fulltext search which is simple to set up and use but it requires some optimization of the server, lots of ram, and when the index isn't in ram it can be fairly slow.  However for databases which aren't gigantic it is likely fine to use and requires basically no setup besides telling MySQL to create the index.

If you narrow your searches down to a timeframe of say weeks, the difference in a % (LIKE) query and a * (MATCH) query are not a big deal.  If you're searching the entire database for example, it could matter.  For example, searching for -message='%spam%' took 22 seconds vs -message='\*+spam\*' was an instant set of results.  But it does double or triple your database size and slows imports down.  

Another caveat to full text search is that it requires a lot of ram and some MySQL tuning to be fast and even with that the first searches can be slow.  I've found that when I first do a MATCH search it still takes a while, presumably for the index to load.  Subsequent searches are very fast.

Using LIKE can still return data pretty fast and save a bunch of disk space, and possibly import speed.  However, there are limitations such as being able to search for required terms, or excluding terms.  

### How to efficiently use a MATCH search

If you want to find all messages that have the terms "work" "sleep" and "late", searching for '%work%sleep%late%' will find them as long as they are in that order.  However, '\*+work +sleep +late\*' will find them in any order.  Or if you want to exclude a term, '\*+work +sleep +late -deadline\*' would exclude the term 'deadline' in the results.

More ways to search and other examples can be found at the [MySQL Boolean help](https://dev.mysql.com/doc/refman/5.5/en/fulltext-boolean.htmac) page.  There you can find more information on how to create efficient queries.  

### How to add a full text index

This assumes your database is called 'chatdb' and your table is called 'chatdbtable' just to make it clear in case your db/table names differ.  

Connect to your table in MySQL from the command line, specifying the proper database, user, and password.

`mysql chatdb -uuser -ppassword`

Then run the following SQL to add the full text index.  Note that if your database is large this will take a long time.  For my database of almost 50 million lines it took about an hour.

`ALTER TABLE chatdbtable ADD FULLTEXT(message);`

## Hostname Proxy Checks

If you want to look up hostnames using the services at [ProxyCheck.io](https://proxycheck.io/) copy the chatdbprivate-example.pm to chatdbprivate.pm and add the API key to the file and then any query including a hostname should also print out any information that proxycheck.io has on the hostname.

## Customizing chatdb

Please see the [Customizing](CUSTOMIZING.MD) file for some more information on how to potentially customize chatdb's scripts.  If you use a different client, or a different database server, etc.

This file is also a decent reference point to understand how the regular expressions work.  If you find that the script is missing some matches for you, it might help you to understand how to fix it.

## FAQ / Common Issues
* All of my results come back as "Retrieved 0 results from Sphinx"
  * Sphinx is not set up completely.  The index is not up to date, etc.  If not using Sphinx, set $useSphinx to 0 in chatdb.pm
* I get "Failed to process record in dbh_execute, database said: DBD::mysql::st execute failed: Unknown column 'id' in 'where clause'"
  * You likely are using a database that was set up prior to adding Sphinx support.  Set $useSphinx to 0 in chatdb.pm or make sure you've update your table for Sphinx usage.
* I get "DBI connect('database=;host=0;port=9306','',...) failed: Can't connect to MySQL server on '0' (115)"
  * Sphinx's searchd is not running.  Run it/enable it at boot or set $useSphinx to 0 in chatdb.pm
* I get "Sphinx returned results but MySQL did not"
  * Sphinx found a match in the message parameter, but when MySQL combined the Sphinx IDs with other parameters all of the Sphinx results were effectively filtered out.  Try less restrictive parameters.
* I get "Can't find FULLTEXT index matching the column list" when doing a query
  * You don't have a fulltext index on the message column
* I get lots of "does not map to Unicode" warnings when reading files
  * To avoid doubling the space used in MySQL for storing data, I default to UTF-8 tables and anything that isn't UTF-8 is filtered out on reading from the files
  * Anything that isn't UTF-8 will throw a warning but the line is imported
* Chinese, Japanese, Korean, etc, DBCS/Double Byte/Wide characters are not stored
  * See "does not map to Unicode" answer above
* Russian text is not stored
  * Russian text shows up as wide characters so they get filtered out, so again, see above

## Requirements
* irssi or znc
* MySQL or MariaDB
* Perl
  * DBI
  * DateTime
  * Getopt
  * FindBin
  * Digest::MD5

## Optional Requirements
 * Sphinx search server
 * TERM::AnsiColor
   * Can colorize output if installed but is not a requirement for use

## Stats


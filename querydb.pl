#!/bin/perl

# Script to query our chatdb

# TODO

## Ban evasion query; kind of like stalker

## Add some verbosity to stalking for debugging next time I need to
### Sometimes it feels like it isn't finding data I expect it to but
### it's usually user error on my part but have to debug to discover it

## Fix remaining instances that could use cleanNicks, some done but incomplete
### Figure out do I need to change : to = in GetOptions, associated ish

## Are their stats I can improve using count instead of parsing rows?

## Add "top channels" query across all known channels

use DBI;
use POSIX qw/ strftime /;
use DateTime;
use strict;
use warnings;
use Encode qw(decode encode);

# Command line arguments
use Getopt::Long qw(GetOptionsFromArray);

# Debugging stuff to remove later
#use Carp;
use Data::Dumper;

# To use a local module
use FindBin;
use lib $FindBin::RealBin;

my $VERSION = '0.1';

use chatdb;

# Use data from chatdb module
my %db = %chatdb::db;

# So we know what our max (possibly truncated) nick is
my $nickmax = $chatdb::nickmax;

# All possible actions, should remove actionenums and create it on the fly sometime
my @allActions = @chatdb::allActions;

# Actions which the nick can be truncated
my @truncatedActions = @chatdb::truncatedActions;

# JOIN messages have a longer nick length, for stalker use mostly
my $stalkerNickMax = $chatdb::stalkerNickMax;

# What is our default number of around lines if not specified
my $aroundlines = $chatdb::aroundlines;

# How many maximum rows will we process in the around query
my $maxrows = $chatdb::maxrows;

# Default maximum for sql rows so we don't return an insane amount
my $sqllimit = $chatdb::sqllimit;

# Limit how many hosts we collect for Stalking
my $maxhosts = $chatdb::maxhosts;

# Only post the max hosts found notice once
my $maxhostsNotice = '1';

# Limit how many maxhost thresholds we can hit in case we do a dumb query
my $maxMaxHosts        = $chatdb::maxMaxHosts;
my $maxMaxHostsCounter = '0';

# Limit how many nicks we collect for Stalking
my $maxnicks = $chatdb::maxnicks;

# Clean stalker logs by default?
my $cleanStalkerLogDefault = $chatdb::cleanStalkerLogDefault;

# Limit how many iterations we scan for new nicks/hosts
my $stalkerMaxLoops = $chatdb::stalkerMaxLoops;

# How many times to look for nicks by default
my $defaultDeepLoops = $chatdb::defaultDeepLoops;

# List of nicks and hostnames to ignore in Stalker
my @hostBlackList = @chatdb::hostBlackList;
my @nickBlackList = @chatdb::nickBlackList;

# Track how many times blacklisting has occurred
my $totalHostBlacklists = '0';
my $totalNickBlacklists = '0';

# How many years old can a result be
my $stalkerMaxAge = $chatdb::stalkerMaxAge;

# Exclude old results?
my $stalkerAgeExclude = $chatdb::stalkerAgeExclude;

# How many nicks do we find in fuzzy nick search
our $nickSearchMax = $chatdb::nickSearchMax;

# Sphinx full-text search
my $useSphinx     = $chatdb::useSphinx;
my %sphinxdb      = %chatdb::sphinxdb;
my $sphinxWeight  = $chatdb::sphinxWeight;
my $sphinxResults = $chatdb::sphinxResults;

# Automatically order by timestamp
my $autoOrder = $chatdb::autoOrder;

# Maximum delta on import before testimport reports a failure
my $maxImportDelta = $chatdb::maxImportDelta;

# How many clones for us to show results by defaults
my $cloneLimitDefault = $chatdb::cloneLimitDefault;

# List of hosts to ignore for clones
my @cloneHostsBlacklist = @chatdb::cloneHostsBlacklist;

# Maximum number of hosts to proxycheck in Stalker -v -v
my $maxProxyCheck = $chatdb::maxProxyCheck;

# Get the timezone used for the stored log lines
my $localtz = $chatdb::localtz;

# Local vars

# Debug
my $DEBUG = '0';

# Define the sql query line so we can manipulate it along the way
my $sql;

# Track how many rows we returned
my $rowsreturned = '0';

# Track how many rows Sphinx returned
my $sphinxRowsReturned = '0';

# Nick Hostname, and Account objects for Stalker
my @nickarray;
my %nickseen;
my $nickcount = '0';

my @hostarray;
my %hostseen;
my $hostcount = '0';

my @accountarray;
my %accountseen;
my $accountcount = '0';

### Arrays to track only new data to make Stalker more efficient
my @newNickArray;
my @newNickChangeArray;
my @newHostArray;
my @newAccountArray;

# Store details about nick/hostname/date/days for later
my %stalkerDetails;

# Invalid nick char; use to concatenate hash data together
my $stalkerSep = '~';

# Track nick we're stalking
my $stalkerNick = '';

# Count sound bytes
my %countSounds;
# !command things to ignore in our countebytes query
my @noCountBytes = @chatdb::noCountBytes;

# Regex to ignore in activitystats, such as scripted/automated commands
my $activityIgnoreRegex = $chatdb::activityIgnoreRegex;

# Keep the list of IDs sphinx returns so we can refer to them later
my @sphinxids;

# Keep the ignored query line ID to use later
my $ignoredid;

# Nicks to store from -dump flags
my @dumpNicks;

# Flag to only print "using time range" once
my $timeRangePrinted = '0';

# Timestamp string to use in various places, built in buildSQL if needed
my $tsSQL = ' ';

# Get todays date to use in a few places
my $today = DateTime->now( time_zone => 'local' );

# Track if we're cycling back for an alternate channel
my $channelTweak = '0';

# Hash to keep track of nicks for the anonymous function
my %anonymousNicks;
my @anonymousList = @chatdb::anonymousList;

# Hash to keep track of colorized nicks
my %colorizedNicks;

# Color nicks by default
my $colornicks = $chatdb::colornicks;

# How many minutes around timestamp or arround queries
my $aroundmins;

# Get our color definitions from chatdb.pm
our $ts_c     = $chatdb::ts_c;
our $chan_c   = $chatdb::chan_c;
our $nick_c   = $chatdb::nick_c;
our $anick_c  = $chatdb::anick_c;
our $onick_c  = $chatdb::onick_c;
our $hn_c     = $chatdb::hn_c;
our $mess_c   = $chatdb::mess_c;
our $around_c = $chatdb::around_c;
our $user_c   = $chatdb::user_c;
our $symbol_c = $chatdb::symbol_c;
our $at_c     = $chatdb::at_c;
our $hl_c     = $chatdb::hl_c;
our $ll_c     = $chatdb::ll_c;
our $c_r      = $chatdb::c_r;
our $c_b      = $chatdb::c_b;
our $bold     = $chatdb::bold;
our $r        = $chatdb::r;

# Highlight words to use later
our @highlightWords;

# Define our globals for config line options and other things
our (
    $sql_nick,        $sql_message,      $sql_channel,
    $sql_orignick,    $sql_username,     $sql_hostname,
    $sql_network,     $sql_action,       $sql_actionnick,
    $sql_columns,     $sql_timestamp,    $sql_enddate,
    $sql_order,       $sql_ascending,    $sql_descending,
    $sql_custom,      $config,           $aroundLines,
    $nickTruncate,    $showLast,         $tableStats,
    $tsRange,         $stalkNick,        $aroundChat,
    $nickSearch,      $sql_match,        $verbose,
    $deepStalk,       $onlyNicks,        $onlyHosts, $blacklist,
    $ignoreStats,     $nickStats,        $activityStats,
    $ignoredOnly,     $usageHelp,        $listChannels,
    $nickConvo,       $sayAction,        $tsToday,
    $tsOneHour,       $color,            $showFirst,
    $highlights,      $channelUserStats, 
    $channelPercent,  $userChannelStats,
    $anonymous,       $terse,            $commonUserStats,
    $cleanStalkerLog, $noBlacklist,      $noJoinPart,
    $noNick,          $dumpActivity,     $testImport,
    $login,           $highlightsonly,   $cloneScan,
    $clones,          $channelGroup,     $excludeChans,
    $sql_Explain,     $sql_Analyze,      $fuzzyHost,
    $tzAdjust, $sql_account, $countSounds, $nolog, $noHeaders, $noPrint,
    $filterNick, $relativeTime
);

# Channels to exclude from queries
my @excludeChans;

# Variables defined in our private module
# Check if we're using proxycheck
my ($proxycheck);

# Load our private module and use variables from it if it exists
my $private_module = eval {
    require chatdbprivate;
    1;
};

# If our module exists, use it
if ( defined ($private_module) && $private_module )
{
    # Hide warnings that show up if the module does not exist
    no warnings 'once';

    # Channels to never leak info from
    $excludeChans = $chatdbprivate::excludeChans;
    
    # Our database creds
    %db = %chatdbprivate::db;
    
    # Our proxy lookup status
    $proxycheck=$chatdbprivate::proxycheck;
}

# Set STDOUt to UTF8
binmode(STDOUT, "encoding(UTF-8)");

# Running this as root?  Naughty
if ( $< == 0 ) {
    die "Do not run this as root!\n";
}

# Shortcut to fail if we have no arguments
die "Usage:  No arguments passed; --help for arguments\n" unless @ARGV;

# Copy the argument list so we can use it in GetOptionsFromArray
my @configoptions = @ARGV;

# Allow passthrough, right now we only care about $config
Getopt::Long::Configure('pass_through');

# Grab our config option
GetOptionsFromArray(
    \@configoptions,
    'config=s' => \$config,
    'help+'    => \$usageHelp
);

# Load a preset config
if ( defined($config) ) {
    loadConfig($config);
}

# Allow --help earlier on
if ( defined($usageHelp) && $usageHelp ) {
    usageHelp();
    exit;
}

# Disable passthrough, so we fail if we get invalid commandline options
Getopt::Long::Configure('no_pass_through');

# Get our remaing command line options
# : means value is optional
GetOptions(
    'nick=s'             => \$sql_nick,
    'n=s'                => \$sql_nick,
    'nt+'                => \$nickTruncate,
    'filternick=s'       => \$filterNick,
    'message=s'          => \$sql_message,
    'm=s'                => \$sql_message,
    'match=s'            => \$sql_match,
    'channel:s'          => \$sql_channel,
    'excludechans:s'     => \$excludeChans,
    'excludechannels:s'  => \$excludeChans,
    'ch:s'               => \$sql_channel,
    'orignick:s'         => \$sql_orignick,
    'username:s'         => \$sql_username,
    'hostname:s'         => \$sql_hostname,
    'fuzzyhost:i'        => \$fuzzyHost,
    'account:s'          => \$sql_account,
    'network:s'          => \$sql_network,
    'action:s'           => \$sql_action,
    'say+'               => \$sayAction,
    'actionnick:s'       => \$sql_actionnick,
    'timestamp=s'        => \$sql_timestamp,
    'ts:s'               => \$sql_timestamp,
    'today+'             => \$tsToday,
    '1hour+'             => \$tsOneHour,
    'onehour+'           => \$tsOneHour,
    'tsrange:s'          => \$tsRange,
    'relative+'          => \$relativeTime,
    'columns:s'          => \$sql_columns,
    'around:i'           => \$aroundLines,
    'limit:i'            => \$sqllimit,
    'aroundchat+'        => \$aroundChat,
    'aroundmins:i'       => \$aroundmins,
    'order:s'            => \$sql_order,
    'sort:s'             => \$sql_order,
    'ascending+'         => \$sql_ascending,
    'descending+'        => \$sql_descending,
    'sql:s'              => \$sql_custom,
    'first:i'            => \$showFirst,
    'last:i'             => \$showLast,
    'stats+'             => \$tableStats,
    'stalker:s'          => \$stalkNick,
    'excludeage:i'       => \$stalkerAgeExclude,
    'maxage:i'           => \$stalkerMaxAge,
    'dumpactivity:s'     => \$dumpActivity,
    'cleanstalker+'      => \$cleanStalkerLog,
    'maxhosts:i'         => \$maxhosts,
    'maxnicks:i'         => \$maxnicks,
    'maxloops:i'         => \$stalkerMaxLoops,
    'nicksearch:s'       => \$nickSearch,
    'verbose+'           => \$verbose,
    'terse+'             => \$terse,
    'deep:i'             => \$deepStalk,
    'onlynicks+'         => \$onlyNicks,
    'onlyhosts+'         => \$onlyHosts,
    'blacklist:s'        => \$blacklist,
    'noblacklist:+'      => \$noBlacklist,
    'ignorestats:s'      => \$ignoreStats,
    'nickstats:s'        => \$nickStats,
    'listchannels:s'     => \$listChannels,
    'channeluserstats:s' => \$channelUserStats,
    'channelpercent:s'   => \$channelPercent,
    'userchannelstats:s' => \$userChannelStats,
    'commonuserstats:s'  => \$commonUserStats,
    'activitystats:+'    => \$activityStats,
    'clonescan+'         => \$cloneScan,
    'clones:s'           => \$clones,
    'channelgroup+'      => \$channelGroup,
    'ignoredonly+'       => \$ignoredOnly,
    'highlights:s'       => \$highlights,
    'highlightsonly+'    => \$highlightsonly,
    'countsounds+'       => \$countSounds,
    'sphinx:i'           => \$useSphinx,
    'weight+'            => \$sphinxWeight,
    'sphinxresults:i'    => \$sphinxResults,
    'help+'              => \$usageHelp,
    'convo=s'            => \$nickConvo,
    'nickconvo=s'        => \$nickConvo,
    'color:i'            => \$color,
    'anonymous+'         => \$anonymous,
    'colornicks:i'       => \$colornicks,
    'nojoinpart+'        => \$noJoinPart,
    'nj+'                => \$noJoinPart,
    'nonick+'            => \$noNick,
    'proxycheck:i'       => \$proxycheck,
    'pc:i'               => \$proxycheck,
    'tzadjust=s'         => \$tzAdjust,
    'testimport:i'       => \$testImport,
    'login:s'            => \$login,
    'debug:i'            => \$DEBUG,
    'explain+'           => \$sql_Explain,
    'analyze+'           => \$sql_Analyze,
    'nolog+'             => \$nolog,
    'noheaders+'         => \$noHeaders,
    'noprint+'           => \$noPrint,
    )
    or die
    "Exiting due to unexpected or unknown parameters.  --help for arguments\n";

# If @ARGV isn't empty at this point, invalid arguments were passed; exit
if (@ARGV) {
    print STDERR 'Invalid arguments found: ';
    foreach my $junk (@ARGV) { print $junk . ' '; }
    print "\n";
    exit;
}

# Log our query/arguments for future reference
# Use our configoptions copy of ARGV since ARGV is consumed already
queryLog( 'QUERY', $login, @configoptions ) unless ( defined($nolog) );

# Highlight words
if ( defined($highlights) && $highlights ne '' ) {

    # Split on commas
    #$highlights =~ s/\ /,/gx;
    @highlightWords = split( /,/, $highlights );

    # Get rid of any empty spaces
    @highlightWords = grep { $_ ne '' } @highlightWords;
}
else {
    @highlightWords = @chatdb::highlightWords;
}

# Excluded channels
if ( defined($excludeChans) && $excludeChans ne '' ) {

    # Split on commas
    @excludeChans = split( /,/, $excludeChans );

    # Get rid of any empty spaces
    @excludeChans = grep { $_ ne '' } @excludeChans;
}

# Attempt to determine if we're outputting to a file and have colors
# defined.  If so, disable color output
if ( !defined($color) && defined($r) && ! $noHeaders ) {
    $color = colorTest($noHeaders);
}

# Nuke color settings if needed
if ( defined($color) && !$color ) {
    clearColorVars();
    $colornicks = '0';
}

if ( defined($usageHelp) && $usageHelp ) {
    usageHelp();
    exit;
}

# Hard-coded queries, no need to buildSQL

# Database stats
if ( defined($tableStats) ) {
    tableStats();
    exit;
}

# Test import status
if ( defined($testImport) ) {
    testLastImport();
    exit;
}

# End of hard-coded queries

# Track our runtime; primarily how long it takes to query the database
my $start = time;

# Just make -dump -say be -dump say, and do this before we buildSQL
if ( defined($dumpActivity) && defined($sayAction) ) {
    $dumpActivity = 'say,emote';
    $sayAction = undef;
}

# If we passed in an sql string to use don't attempt to build one
# Always run buildSQL before even our custom queries so we can get timestamps etc
if ( !defined($sql_custom) ) {
    $sql = buildSQL();
}
else {
    sanitizeSQL($sql_custom);
    $sql = $sql_custom;
}

if ( $DEBUG >= 1 ) {
    print "sql: [$sql]\n";
    exit if $DEBUG >= 99;
}

# Define the queries.  Specific types of queries are defined first
# followed by queries passed in via command line arguments

# Query that shows N lines around each match; uses buildSQL so must be below it
# If we define aroundchat or around, just do the thing
if ( ( defined($aroundLines) || defined($aroundChat) )
    && defined($sql_message) )
{
    # Set sql_around if we hadn't yet
    if ( !defined($aroundLines) ) {
        $aroundLines = $aroundlines;
    }

    aroundSQL($sql);
    exit;
}

# Stalker
if ( defined($stalkNick) ) {

    if ( $stalkNick eq '' && !defined($sql_nick) ) {
        queryLog( 'INFO', $login, 'No nick provided, exiting...' );
        exit;
    }

    # If we get arguments like -nick blah -stalk just do it
    if ( $stalkNick eq '' && $sql_nick ne '' ) {
        $stalkNick = cleanNicks($sql_nick);
    }
    if ( defined($cleanStalkerLog)
        || $cleanStalkerLogDefault && !defined($login) )
    {
        stalkerLog( 'CLEAN', $login,
            "Cleaning out log by request or default\n\n" );
    }

    $stalkNick = cleanNicks($stalkNick);
    stalker($stalkNick);

    exit;
}

# Nick Search
if ( defined($nickSearch) ) {
    nickSearch();
    exit;
}

# Clone Scan
if ( defined($cloneScan) ) {
    cloneScan();
    exit;
}

# Ignored user info
if ( defined($ignoreStats) ) {
    ignoredUserStats();
    exit;
}

# Nick stats
if ( defined($nickStats) ) {
    nickStats();
    exit;
}

# Channel statistics of a given user
if ( defined($userChannelStats) ) {
    userChannelStats();
    exit;
}

# User statistics of a given channel
if ( defined($channelUserStats) ) {
    channelUserStats();
    exit;
}

# Nick channel stats
if ( defined($listChannels) ) {
    listChannels();
    exit;
}

# Common channel stats
if ( defined($commonUserStats) ) {
    commonUserStats();
    exit;
}

# Activity stats
if ( defined($activityStats) ) {
    activityStats();
    exit;
}

# Get proxycheck info if we're looking at a hostname and have a key
# Do this up towards the top because dump bails out before it gets
# much further.  Don't attempt if we're passing a list of hosts.
    if ( $sql_hostname && $proxycheck 
    && $sql_hostname !~ /,/) {
        my $proxyinfo = proxychecker($sql_hostname, $login);
        print $proxyinfo . "\n\n" if ( defined($proxyinfo) );
}

# Make sure we have sane SQL querying for an actual match
# or we're doing a showlast
#if ( $sql =~ /select \* from $db{'table'}.*WHERE.*(\s+id\ IN\s+|\s+MATCH\s+|\s+BETWEEN\s+|\s+\=\s+|\s+LIKE\s+).*/i || defined($showLast) )
#print "[$sql]\n";
if ( $sql =~ /select \* from $db{'table'}.*WHERE.*(\s+(id IN|MATCH|BETWEEN|=|LIKE)\s+).*/i || defined($showLast) )
{
    # if ( defined($sql_Explain) && $sql_Explain ) {
    #     $sql = 'EXPLAIN EXTENDED ' . $sql;
    #     print "sql: [$sql]\n";
    #     my @rows = doQuery($sql);
    #     print Dumper @rows;
    #     exit;
    # }

    # if ( defined($sql_Analyze) && $sql_Analyze ) {
    #     $sql = 'ANALYZE ' . $sql;
    #     print "sql: [$sql]\n";
    #     my @rows = doQuery($sql);
    #     print Dumper @rows;
    #     exit;
    # }

    my @rows = doQuery($sql);
    printRows(@rows);
}
else
{
    die 'Incomplete SQL query; invalid/missing parameters?' . "\n";
}

# Kludge to swap ## and # for channels if no results are found,
# but only if a time range is given and appears to be a real channel
if ( $rowsreturned == 0 )
{
    if ( $sql_channel
        && ! $sql_timestamp
        && ! $sql_enddate )
    {
        queryLog( 'INFO', $login, 'No results found for ' . $hl_c . $sql_channel . $r . ' but not trying alternatives because no timespan defined' );
    }
    elsif ( $sql_channel
        && $sql_channel =~ /^\#/
        && $sql_timestamp
        && $sql_enddate )
    {
        my $orig_channel = $sql_channel;

        # Replace ##channel with #channel
        if ( $sql_channel =~ /^\#\#(.*)$/ )
        {
            $sql_channel = '#' . $1;
        }
        # Replaces #channel with ##chanel
        else
        {
            $sql_channel = '#' . $sql_channel;
        }

        # Set this so we don't print time range message again
        $channelTweak = '1';

        # Reprocess our SQL to see if we find results now
        $sql = buildSQL();
        my @rows = doQuery($sql);
        printRows(@rows);

        print "\n";

        # Let the user know if we still failed, or if they failed
        if ( $rowsreturned > 0 )
        {
            queryLog( 'INFO', $login, $hl_c . $orig_channel . $r . ' appears to be incorrect, found results using ' . $hl_c . $sql_channel . $r);
        }
        else
        {
            queryLog( 'INFO', $login, 'No results found for ' . $c_r . $orig_channel . $r . ' or ' . $c_r . $sql_channel . $r);
        }
    }
}

if ( $sphinxRowsReturned > 0 && $rowsreturned == 0 ) {
    queryLog( 'INFO', $login, 'Sphinx returned results but MySQL did not.' );
}

# If we are querying for sounds, print the results here
if ( $countSounds )
{
    my $maxLen = '0';
    my $maxCount = '20';
    my $count = '1';

    # Find the maximum name length for formatting
    foreach my $name ( keys %countSounds )
    {
        my $len = length($name);
        $maxLen = $len if ( $len > $maxLen );
    }

    if ( $maxLen )
    {
        queryLog( 'HEADER', $login, 'Printing top ' . $hl_c . $maxCount . $r . ' sound bytes in channel ' . $hl_c . $sql_channel . $r );
        print "\n";

        foreach my $name (sort { $countSounds{$b} <=> $countSounds{$a} } keys %countSounds)
        {
            printf "%-${maxLen}s %s\n", $name, $countSounds{$name};
            $count++;
            last if ( $count > $maxCount );
        }
    }
    else
    {
        queryLog( 'HEADER', $login, 'No sound bytes found in channel ' . $hl_c . $sql_channel . $r );
    }
}
                
# Print final statistics

my $finalend     = time;
my $finalelapsed = $finalend - $start;

my $finalelapsedstr
    = 'Took '
    . $finalelapsed
    . ' seconds to query; '
    . $rowsreturned
    . ' total rows returned';

print "\n";

if ( $rowsreturned == $sqllimit ) {
    queryLog( 'DEBUG', $login,
              'Reached limit of '
            . $sqllimit
            . ' in query.  Use --limit to change if needed.' );
    print STDERR "\n";
}

queryLog( 'HEADER', $login, $finalelapsedstr ) unless $noHeaders;

exit;

# Function to combine options into a sql query
sub buildSQL {

    my $tempsql = '';

    # If set, put 'add' in the query
    my $and = 0;

    # If set, add 'where' to sql query
    my $where = 1;

    # Build initial query from columns requested
    if ( defined($sql_columns) ) {
        $tempsql = 'select ' . $sql_columns . ' from ' . $db{'table'};
    }

    # Otherwise, return all columns
    else {
        $tempsql = 'select * from ' . $db{'table'};
    }

# For each query type, if '%' is part of the string a LIKE match will be used
# Some columns, such as timestamp, can be handled quicker/better and are manipulated to use their index

    # We use this in a few places, so let's check it first thing
    if ( defined($sql_timestamp) && $sql_timestamp eq "" ) {
        die "Timestamp parameter provided but appears to be invalid\n";
    }

    # Convo query
    if ( defined($nickConvo) && defined($useSphinx) ) {
        $sql_nick = cleanNicks($nickConvo);
        my @splitnicks = split( ',', $sql_nick );
        $sql_message = $splitnicks[0] . ' | ' . $splitnicks[1];
    }

    # nick query
    if ( defined($sql_nick) && $sql_nick ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        # Add ' where ' to the tempsql if we haven't yet
        $tempsql .= ' where ' if $where == 1;
        $where--;

        $tempsql .= nickSQL($sql_nick, 'nick');
    }

    # filter nick query
    if ( defined($filterNick) && $filterNick ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        # Add ' where ' to the tempsql if we haven't yet
        $tempsql .= ' where ' if $where == 1;
        $where--;

        $tempsql .= nickSQL($filterNick, 'filternick');
    }

    # channel query
    # Make sure we didn't pass a channel; may have not quoted it
    if ( defined($sql_channel) && $sql_channel eq '' ) {
            queryLog( 'INFO', $login, 'Channel specified but empty; perhaps not quoted?' );
       		exit;
       }
       
    if ( defined($sql_channel) && $sql_channel ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        my @splitchans     = split( ',', $sql_channel );
        my $splitchancount  = @splitchans;
        my $or              = '';
        my $loopcount       = 1;

        # Group the query if we have more than one channel
        if ( $splitchancount > 1 ) {
            $tempsql .= '(';
        }

        # Iterate over channels separated by commas
        foreach my $splitchan (@splitchans) {

            # Determine if we need to add 'or' to the end or not
            if ( $loopcount < $splitchancount ) {
                $or = ' or ';
            }
            else {
                $or = ' ';
            }
            $loopcount++;

            if ( $splitchan =~ /\%/x ) {

                # Need to escape any special characters using , for channels
                $splitchan =~ s/_/\,_/g;
                $splitchan =~ s/\[/\,\[/g;
                $splitchan =~ s/\]/\,\]/g;
                $tempsql .= '( channel like \'' . $splitchan . '\' ESCAPE \',\') ' . $or;
            }
            else {
                $tempsql .= '( channel = \'' . $splitchan . '\' ) ' . $or;
            }
        }

        # Close the group
        if ( $splitchancount > 1 ) {
            $tempsql .= ') ';
        }
    }

    # orignick query
    if ( defined($sql_orignick) && $sql_orignick ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        if ( $sql_orignick =~ /\%/x ) {

            # Need to escape any special characters
            $sql_orignick =~ s/_/\#_/g;
            $sql_orignick =~ s/\[/\#\[/g;
            $sql_orignick =~ s/\]/\#\]/g;
            $tempsql
                .= 'orignick like \'' . $sql_orignick . '\' ESCAPE \'#\' ';
        }
        else {
            $tempsql .= 'orignick = \'' . $sql_orignick . '\' ';
        }
    }

    # username query
    if ( defined($sql_username) && $sql_username ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        if ( $sql_username =~ /^(.*)\@(.*)$/x ) {
            queryLog( 'HEADER', $login,
                'WARNING:  Removing hostname.' );
            $sql_username = $1;
        }

        if ( $sql_username =~ /\%/x ) {
            $tempsql .= 'username like \'' . $sql_username . '\' ';
        }
        else {
            $tempsql .= 'username = \'' . $sql_username . '\' ';
        }
    }

    # hostname query
    if ( defined($sql_hostname) && $sql_hostname ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        my @splithosts     = split( ',', $sql_hostname );
        my $splithostcount  = @splithosts;
        my $or              = '';
        my $loopcount       = 1;

        # Group the query if we have more than one host
        if ( $splithostcount > 1 ) {
            $tempsql .= '(';
        }

        # Save the cleaned up hosts to use other places as $sql_hostname
        # Specifically removing usernames
        my @cleanSplitHosts;

        # Iterate over hosts separated by commas
        foreach my $splithost (@splithosts) {

            # Determine if we need to add 'or' to the end or not
            if ( $loopcount < $splithostcount ) {
                $or = ' or ';
            }
            else {
                $or = ' ';
            }
            $loopcount++;

            if ( $splithost =~ /^(.*)\@(.*)$/x ) {
                $splithost = $2;
            }

            # Clean up stray brackets
            $splithost =~ s/[\[\]]//g;
            $splithost =~ s/[\(\)]//g;

            # Replace * with % for wildcards
            $splithost =~ s/\*/%/x;

            # Remove octets to make host lookups more fuzzy
            if ( defined($fuzzyHost) )
            {
                # Default to 1 if not set
                $fuzzyHost = 1 unless $fuzzyHost;
                my $newhost = fuzzyhost($splithost, $fuzzyHost);
                
                # If we have fuzzied the name, use it
                if ( $newhost ne $splithost )
                {
                    $splithost = $newhost;
                    queryLog('HEADER', $login, 'Fuzzied hostname to ' . $splithost);
                    print "\n";
                }
            }

            if ( $splithost =~ /\%/x ) {

                $tempsql .= '( hostname like \'' . $splithost . '\' ESCAPE \',\') ' . $or;
            }
            else {
                $tempsql .= '( hostname = \'' . $splithost . '\' ) ' . $or;
            }

            push(@cleanSplitHosts, $splithost);

        }

        # Close the group
        if ( $splithostcount > 1 ) {
            $tempsql .= ') ';
        }

        # Replace sql_hostname with the cleaned-up hostnames
        $sql_hostname = join(',', @cleanSplitHosts);
    }

    # network query
    if ( defined($sql_network) && $sql_network ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        if ( $sql_network =~ /\%/x ) {
            $tempsql .= 'network like \'' . $sql_network . '\' ';
        }
        else {
            $tempsql .= 'network = \'' . $sql_network . '\' ';
        }
    }

    # account query
    if ( defined($sql_account) ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        # Handle literal '' or "" as input
        if ( $sql_account eq "''" || $sql_account eq '""' )
        {
            $sql_account = '';
        }

        if ( $sql_account =~ /\%/x ) {
            $tempsql .= 'account like \'' . $sql_account . '\' ';
        }
        else {
            $tempsql .= 'account = \'' . $sql_account . '\' ';
        }
    }

    # action say shortcut
    if ( defined($sayAction) ) {
        $sql_action = 'say,emote';
    }

    # action query
    if ( defined($sql_action) && $sql_action ne '' ) {

        # Clean any spaces out first
        $sql_action =~ s/\ //g;

        # Strip off the trailing comma
        $sql_action =~ s/\,$//;

        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        # Allow a comma separated list of modes
        my @splitactions     = split( ',', $sql_action );
        my $splitactioncount = @splitactions;

        if ( $splitactioncount == 1 ) {

            # If we're asking for an action which is invalid, fail
            if ( !grep( /^$sql_action$/i, @allActions ) ) {
                die $sql_action
                    . ' is not in the valid list of actions, exiting' . "\n";
            }

            $tempsql .= 'action = \'' . $sql_action . '\' ';
        }
        else {
            my $or        = '';
            my $loopcount = '1';

            $tempsql .= ' ( ';

            foreach my $splitaction (@splitactions) {

                # If we're asking for an action which is invalid, fail
                if ( !grep( /^$splitaction$/i, @allActions ) ) {
                    die $splitaction
                        . ' is not in the valid list of actions, exiting'
                        . "\n";
                }

                # Determine if we need to add 'or' to the end or not
                if ( $loopcount < $splitactioncount ) {
                    $or = ' or ';
                }
                else {
                    $or = ' ';
                }

                $tempsql .= 'action = \'' . $splitaction . '\'' . $or;

                $loopcount++;
            }
            $tempsql .= ' ) ';
        }
    }

    # actionnick query
    if ( defined($sql_actionnick) && $sql_actionnick ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        if ( $sql_actionnick =~ /\%/x ) {
            $sql_actionnick =~ s/_/\#_/g;
            $sql_actionnick =~ s/\[/\#\[/g;
            $sql_actionnick =~ s/\]/\#\]/g;
            $tempsql
                .= 'actionnick like \''
                . $sql_actionnick
                . '\' ESCAPE \'#\' ';
        }
        else {
            $tempsql .= 'actionnick = \'' . $sql_actionnick . '\' ';
        }
    }

    # Force a timeframe on commonuserstats if not already specified
    if ( defined($commonUserStats) ) {
        if ( !defined($tsToday) && !defined($sql_timestamp) ) {

            # Force it to be today
            queryLog( 'HEADER', $login,
                'Warning:  Time range not specified, defaulting to today' );
            $tsToday = '1';
        }
    }

    # Today timestamp shortcut
    if ( defined($tsToday) ) {
        $sql_timestamp = 'today';
    }

    # One hour timestamp shortcut
    if ( defined($tsOneHour) ) {
        $sql_timestamp = '1 hour';
    }

    # Try getting a date from our fuzzy date logic first thing
    # but only if $sql_enddate is not defined, as that implies
    # we already know the full range to use
    if ( defined($sql_timestamp) & !defined($sql_enddate) ) { 

        # Build time range around timestamp and aroundmins if specified
        # Also verify sql_timestamp seems to be a timestamp and not a 
        # fuzzy value 
        if ( defined($aroundmins) && $aroundmins && $sql_timestamp =~ m/^[0-9\ :-]+$/ )
        {
            my $s_ts = $sql_timestamp;

            # If HH:MM is passed in, append seconds
            if ( $s_ts =~ m/^\d\d:\d\d$/ )
            {
                $s_ts .= ':00';
            }

            # If HH:MM:SS, append todays date
            if ( $s_ts =~ m/^\d\d:\d\d:\d\d$/ )
            {
                my ($t_date, undef) = split(/ /, dt2str($today));
                $s_ts = $t_date . ' ' . $s_ts;
            }

            # Check that we built a sane timestamp and assign to tsRange
            if ( $s_ts =~ m/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/ )
            {
                my ( $tsstart, $tsend ) = tsSpan( $s_ts, $aroundmins );
                $tsRange = "$tsstart $tsend";
            }
            else
            {
                queryLog( "WARN", $login, "Didn't build a valid timestamp from input: " . $hl_c . "$sql_timestamp" . $r);
                exit;
            }
        }
        # Fuzzy or exact timestamp provided, process it as it is
        else
        {
            my $fuzzydate = fuzzyDate($sql_timestamp);

            if ( defined($fuzzydate) ) {

                # If our output is a range, put it in sql_tsrange
                if (   $fuzzydate =~ m/^\d\d\d\d-\d\d-\d\d \d\d\d\d-\d\d-\d\d$/
                    || $fuzzydate
                    =~ m/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d \d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/
                    )
                {
                    $tsRange = $fuzzydate;

                    # This was a fuzzy input; nuke it
                    $sql_timestamp = '';
                }

                # If our output is a single date, put it in sql_timestamp
                if ( $fuzzydate =~ m/^\d\d\d\d-\d\d-\d\d$/ ) {
                    $sql_timestamp = $fuzzydate;
                }
            }
        }
    }

    # Shortcut for start/end timestamp range
    if ( defined($tsRange) ) {
        if ( $tsRange =~ m/^(\d\d\d\d-\d\d-\d\d) (\d\d\d\d-\d\d-\d\d)$/ ) {
            $sql_timestamp = $1;
            $sql_enddate   = $2;
        }
        elsif ( $tsRange
            =~ m/^(\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d) (\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d)$/
            )
        {
            $sql_timestamp = $1;
            $sql_enddate   = $2;
        }
        elsif ( $tsRange =~ m/^(\d\d\d\d-\d\d-\d\d)$/ )
        {
            my $dtend = DateTime->now( time_zone => 'local' )->add( days => 1 );
            ( $sql_enddate, undef ) = split( / /, dt2str($dtend) );
            $sql_timestamp   = $tsRange;
        }
        elsif ( $tsRange =~ m/^(\d\d\d\d)$/ )
        {
            $sql_timestamp = $1 . '-01-01';
            my $endyear = $1 + 1;
            $sql_enddate   = $endyear . '-01-01';
        }
        else {
            queryLog( 'HEADER', $login,
                'Warning:  tsrange specified but invalid match, use "YYYY-MM-DD YYYY-MM-DD"'
            );
        }

        if ( $timeRangePrinted == 0 )
        {
            print 'Using time range of ' . $sql_timestamp . ' to ' . $sql_enddate . "\n\n" unless ( $channelTweak || $noHeaders );
            $timeRangePrinted++;
        }
    }

    # If we have a sane looking timestamp, build the query
    if ( defined($sql_timestamp) && $sql_timestamp ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' where ' if $where == 1;
        $where--;

        if ( $sql_timestamp =~ /\%/x ) {
            $tempsql .= 'timestamp like \'' . $sql_timestamp . '\' ';
            queryLog( 'HEADER', $login,
                'Warning; timestamp matches are slow.  Use YYYY-MM-DD if you need results from a single day'
            );
        }
        else {

            # If passed in an exact timestamp with no end-date, use it
            if ( $sql_timestamp =~ m/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/
                && !defined($sql_enddate) )
            {
                $tempsql .= 'timestamp = \'' . $sql_timestamp . '\' ';
            }

            # If passed in a YYYY-MM-DD string, make it a fast query
            elsif ( $sql_timestamp =~ m/^\d\d\d\d-\d\d-\d\d$/
                | $sql_timestamp =~ m/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/ )
            {

                # And if enddate is specified, use that as the end date
                if ( defined($sql_enddate) ) {
                    $tempsql
                        .= 'timestamp BETWEEN \''
                        . $sql_timestamp
                        . '\' AND \''
                        . $sql_enddate . '\' ';
                }

                # Otherwise, add a day so we can use BETWEEN
                else {
                    my $startday = str2dt($sql_timestamp);
                    my $nextday  = $startday->clone();
                    $nextday->add( days => 1 );
                    my ( $startdate, $starttime )
                        = split( / /, dt2str($startday) );
                    my ( $nextdate, $nexttime )
                        = split( / /, dt2str($nextday) );
                    $tempsql
                        .= 'timestamp BETWEEN \''
                        . $startdate
                        . '\' and \''
                        . $nextdate . '\' ';
                }
            }

            # If it's not YYYY-MM-DD or a partial string, literal match
            else {
                # Only generate the SQL if our timestamp looks sane
                if ( $sql_timestamp =~ m/^\d\d\d\d-\d\d-\d\d.*/ ) {
                    $tempsql .= 'timestamp = \'' . $sql_timestamp . '\' ';
                }
                else {
                    die "Timestamp appears to be invalid\n";
                }
            }
        }
        $tsSQL = tsSQL();
    }

    # match query (shortcut for message using MATCH) for MySQL fulltext search
    # Seems to be fine for Sphinx searches too; ie:  does not affect results
    if ( defined($sql_match) && $sql_match ne '' ) {
        $sql_message = '*' . $sql_match . '*';
    }

    # If looking for highlights, just update sql_message
    if ( defined($highlightsonly) ) {
        my $hlcount = @highlightWords;
        if ( $hlcount > 0 ) {
            if ( defined($sql_message) && $sql_message ne '' ) {
                queryLog( 'HEADER', $login,
                    'Warning; using -highlights over-rides -message' );
            }

            # Wrap highlight words with \* so we match in Sphinx
            my @newHighlightWords;
            foreach my $hl (@highlightWords) {
                push( @newHighlightWords, '\*' . $hl . '\*' );
            }
            $sql_message = join( ' | ', @newHighlightWords );
        }
        else {
            die "Highlight query requested by highlightWords is empty\n";
        }
    }

    # message query
    if ( defined($sql_message) && $sql_message ne '' ) {
        if ($and) {
            $tempsql .= 'and ';
        }

        $and++;
        $tempsql .= ' where ' if $where == 1;
        $where--;

        # If using Sphinx, need to generate a list of IDs to use then query
        # for those IDs, not the message column directly in the database
        if ( defined($useSphinx) && $useSphinx ) {
            my $addComma = '0';

            # Sphinx can't handle slashes, replace with spaces
            $sql_message =~ s/\//\ /g;

            # Sphinx needs ' escaped
            $sql_message =~ s/'/\\'/g;

            # If we forget and use %, change those to * which is
            # what Sphinx uses for wildcards
            $sql_message =~ s/%/*/g;

            @sphinxids = searchSphinx( 'message', $sql_message );
            my $sphinxids = @sphinxids;

            # Combine the delta index results
            my @deltasphinxids = searchSphinx( 'delta', $sql_message );
            my $deltaids       = @deltasphinxids;
            @sphinxids          = ( @sphinxids, @deltasphinxids );
            $sphinxRowsReturned = @sphinxids;
            queryLog( 'HEADER', $login,
                      'Retrieved '
                    . $sphinxRowsReturned . ' (M:'
                    . $sphinxids . ' / D:'
                    . $deltaids
                    . ') results from Sphinx' );
            print STDERR "\n";

            my $idcount = @sphinxids;
            if ( $idcount > 0 ) {
                $tempsql .= ' id IN (';
                foreach my $s_id (@sphinxids) {
                    if ($addComma) {
                        $tempsql .= ', ';
                    }
                    $tempsql .= $s_id->{'id'};
                    $addComma = '1';
                }
                $tempsql .= ') ';
            }
            else {
                # No need to proceed any further
                exit;
            }
        }

        # If not using Sphinx, proceed with SQL queries
        else {
            # Like query
            if ( $sql_message =~ /\%/x ) {
                queryLog( 'HEADER', $login,
                    'Warning; message like queries can be slow...' );
                print STDERR "\n";
                $tempsql .= 'message like \'' . $sql_message . '\' ';
            }

            # Match query
            elsif ( $sql_message =~ /^\*(.*)\*$/x ) {
                $sql_message = $1;
                my $searchMode = 'BOOLEAN';

                # If we see + or - assume this query includes search operators
                # and this is super fast compared to pretty much anything else
                if ( $sql_message =~ m/[+-]/x ) {
                    queryLog( 'HEADER', $login,
                        'Full text search operators in use for matching...' );
                    print STDERR "\n";
                }
                else {
                    queryLog( 'HEADER', $login,
                        'Full text search using generic terms or phrase (warning; can be slow)'
                    );
                    print STDERR "\n";
                }

                $tempsql
                    .= 'MATCH(message) AGAINST(\''
                    . $sql_message
                    . '\' IN '
                    . $searchMode
                    . ' MODE) ';
            }

            # Exact query
            else {
                $tempsql .= 'message = \'' . $sql_message . '\' ';
            }
        }
    }

    # Ignored users query
    if ( defined($ignoredOnly) ) {
        if ($and) {
            $tempsql .= 'and ';
        }
        $and++;

        $tempsql .= ' ignored = 1 ';
    }

    #Exclude channels from general queries
    if ( @excludeChans)
    {
        foreach my $excludedChannel (@excludeChans)
        {
            # Some situations we may get here with no query as of
            # yet, so make sure we specify WHERE or AND appropriately
            my $term = ' AND ';
            if ( $tempsql !~ /where/i )
            {
                $term = ' WHERE ';
            }
            $tempsql .= $term . ' ( channel <> \'' . $excludedChannel . '\') '
        }
    }

    # order query
    if ( defined($sql_order) ) {
        $tempsql .= 'order by ' . $sql_order . ' ';
    }

    # Ascending sort
    if ( defined($sql_ascending) ) {
        $tempsql .= 'asc ';
    }

    # Descending sort
    if ( defined($sql_descending) ) {
        $tempsql .= 'desc ';
    }

    # Last line inserted in database
    if ( defined($showLast) ) {
        my $limit = $showLast > 0 ? $showLast : 1;
        $tempsql .= ' order by timestamp desc limit ' . $limit;
    }

    # First line inserted in database
    if ( defined($showFirst) ) {
        queryLog( 'HEADER', $login,
            'Warning; this can be slow; sometimes faster to not use -first and instead pipe into head'
        );
        print STDERR "\n";
        my $limit = $showFirst > 0 ? $showFirst : 1;
        $tempsql .= ' order by timestamp asc limit ' . $limit;
        # Hack; inject "ignore index (tsidx)" inside the statement, which
        # makes ascending queries like this MUCH faster
        $tempsql =~ s/$db{'table'} where/$db{'table'} ignore index \(tsidx\) where/;
    }

    # If autoOrder is set to 1, order by timestamp automatically
    if ( defined($autoOrder) && $autoOrder ) {

        # Unless we already have an order
        if ( $tempsql !~ /order by/i ) {
            $tempsql .= ' order by timestamp asc';
        }
    }

    # Limit the output
    if ( defined($sqllimit) && !defined($showLast) && !defined($showFirst) ) {
        $tempsql .= ' limit ' . $sqllimit;
    }

    # And add a semicolon at the end
    $tempsql .= ';';

    # And if we're grouping by channel, tweak our SQL a bit
    if ( defined($channelGroup ) )
    {
        $tempsql =~ s/order by timestamp/order by channel,timestamp/;
    }

    return $tempsql;
}

# Build SQL for nick queries
sub nickSQL {
    my ($nicktext, $querytype) = @_;

    my $returnsql;

    # Build intermediate operators that match the requested query type
    my ($andor, $equal, $like);

    if ( $querytype eq 'nick' )
    {
        $andor = 'OR';
        $equal = '=';
        $like = 'LIKE';
    }

    if ( $querytype eq 'filternick' )
    {
        $andor = 'AND';
        $equal = '<>';
        $like = 'NOT LIKE';
    }

    # Get rid of leading @ or + (opped or voiced copy/paste)
    $nicktext = cleanNicks($nicktext);

    my @splitnicks     = split( ',', $nicktext );
    my $splitnickcount = @splitnicks;
    my $groupand       = '';
    my $loopcount      = 1;

    # Group the query if we have more than one nick
    if ( $splitnickcount > 1 ) {
        $returnsql .= '(';
    }

    # Iterate over nicks separated by commas
    foreach my $splitnick (@splitnicks) {

        # Determine if we need to add our $andor to the end or not
        if ( $loopcount < $splitnickcount ) {
            $groupand = $andor;
        }
        else {
            $groupand = ' ';
        }
        $loopcount++;

        # Only trunate/pattern match with -nt if nick is really too long
        if ( defined($nickTruncate)
            && length($splitnick) >= $nickmax )
        {
            $splitnick = substr( $splitnick, 0, $nickmax );
            $splitnick .= '%';
        }

        if ( $splitnick =~ /\%/x ) {

            # Need to escape any special characters
            $splitnick =~ s/_/\#_/g;
            $splitnick =~ s/\[/\#\[/g;
            $splitnick =~ s/\]/\#\]/g;
            $returnsql
                .= '( nick ' . $like . ' \''
                . $splitnick
                . '\' ESCAPE \'#\' ' . $andor . ' orignick ' . $like . ' \''
                . $splitnick
                . '\' ESCAPE \'#\' )'
                . $groupand;
        }
        else {
            if ( length($splitnick) > $nickmax && !defined($stalkNick) ) {

             # If action is defined and if it's in a list of actions which
             # are possibly truncated, warn the user.
                my $trunWarn = 0;
                if ( defined($sql_action)
                    && grep( /^$sql_action$/i, @truncatedActions ) )
                {
                    $trunWarn = 1;
                }

                if ($trunWarn) {
                    queryLog( 'HEADER', $login,
                              'WARNING:  '
                            . $splitnick
                            . ' may be truncated in database and not match; use -nt flag'
                    );
                }
            }
            $returnsql
                .= '( nick ' . $equal . ' \''
                . $splitnick
                . '\' ' . $andor . ' orignick ' . $equal . ' \''
                . $splitnick . '\' )'
                . $groupand;
        }
    }

    # Close the group
    if ( $splitnickcount > 1 ) {
        $returnsql .= ') ';
    }

    return $returnsql;
}

# Pretty format the returned results, basically more or less like the input irssi logs
sub printRows {
    my (@rows) = @_;

    # Reverse our row array if we're showing the last N values
    if ($showLast)
    {
        @rows = reverse(@rows);
    }

    # Update rowsreturned
    $rowsreturned = $rowsreturned + @rows;

    return if $noPrint;

    # Store the last timestamp for human readable output later
    my $last_ts = 0;

    # Keep track of day changes
    my $last_date;
    my $prev_date = '';
    my $today_flag = '0';

    # If we defined the columns, just print the line as-is
    if ( defined $sql_columns ) {
        my @columns = split( ',', $sql_columns );
        foreach my $row (@rows) {
            foreach my $col (@columns) {
                print $row->{$col} . ' ';
            }
            print "\n";
        }
    }

    # Otherwise, parse it out nicely
    else {
        foreach my $row (@rows) {

            # Bail if the channel is in the excluded list
            my $excluded = '0';

            foreach my $excludedChannel (@excludeChans)
            {
                next unless $excludedChannel;

                if ( $row->{'channel'} =~ $excludedChannel )
                {
                    $excluded++;
                }
            }

            # Bail out of this row if we found a match
            if ( $excluded )
            {
                # Remove the result from how many lines we report
                $rowsreturned--;
                next;
            }

            # Specialized query; counting soundbytes on channels
            if ( $countSounds && $sql_channel )
            {
                my $maxLen = '20';
                if ( $row->{'message'} =~ /^!(\w+)/ )
                {
                    my $byte = $1;
                    # Sanity check and don't store other stuff
                    if ( length($byte) <= $maxLen )
                    {
                        # If this one is in our noCountBytes array don't count it
                        my $skipByte = '0';
                        foreach my $noCount ( @noCountBytes )
                        {
                            $skipByte++ if ( lc($noCount) eq lc($byte) );
                        }
                        $countSounds{ $byte }++ unless ( $skipByte );
                    }
                }

                # No need to go any further
                next;
            }

            # If dumpactivity is specified, store an array of nicks that match
            # Also if verbose is defined so we can dump just the list of nicks
            # If using anonymous, just do not push into dumpNicks
            if ( defined($dumpActivity) || defined($verbose) ) {
                if ( $row->{'nick'} ne '' && $row->{'nick'} !~ /^\$/ && ! $anonymous )
                {
                    push( @dumpNicks, $row->{'nick'} );
                }
            }

            # Make a visual difference when a line is from an ignored user
            # but only if they're saying something
            my $nick_start = $ll_c . '<' . $r;
            my $nick_end   = $ll_c . '>' . $r;
            if ($row->{'ignored'}
                && (   $row->{'action'} eq "SAY"
                    || $row->{'action'} eq "EMOTE" )
                )
            {
                $nick_start = $ll_c . '(' . $r;
                $nick_end   = $ll_c . ')' . $r;
            }

            # And create consistent markup for host and accounts
            my $host_start = $ll_c . ' [' . $r;
            my $host_end   = $ll_c . ']' . $r;

            my $account_start = $ll_c . ' (' . $r;
            my $account_end   = $ll_c . ')' . $r;

            my $row_username = '';

            # Pull timestamp out to manipulate it as necessary
            my $ts = $row->{'timestamp'};

            # Adjust timezone on this timestamp if needed
            if ( defined($tzAdjust) && $tzAdjust ne "" )
            {
                my $dt = str2dt($ts);
                # Set what timezone this timestamp is currently in
                $dt->set_time_zone($localtz);
                # Set what it should be
                $dt->set_time_zone($tzAdjust);
                $ts = dt2str($dt);
            }

            # Redact information from some fields when anonymous
            my $username_text;
            my $hostname_text;
            my $orignick_text;
            my $account_text;

            if ( $anonymous )
            {
                $orignick_text = 'redacted_o';
                $username_text = 'redacted_u';
                $hostname_text = 'redacted_h';
                $account_text = 'redacted_a';
            }
            else
            {
                $orignick_text = $row->{'orignick'};
                $username_text = $row->{'username'};
                $hostname_text = $row->{'hostname'};
                if ($row->{'account'})
                {
                    $account_text = $row->{'account'};
                }
            }
            
            # Pull out row strings so we can manipulate/color them
            my $row_timestamp  = $ts_c . $ts . $r;
            my $row_channel    = $chan_c . $row->{'channel'} . $r;
            my $row_orignick   = $onick_c . $orignick_text . $r;
            my $row_hostname   = $hn_c . $hostname_text . $r;
            my $row_message    = $mess_c . $row->{'message'} . $r;
            my $row_account    = '';
            if ($row->{'account'})
            {
                $row_account = $account_start . $account_text . $account_end . $r;
            }

            $last_ts = $ts;
            ($last_date, undef) = split(/ /, $ts);

            # Create relative timestamp if being used
            my $relativeTimeStr = '';
            if ( $relativeTime )
            {
                my $rel = relativeTimeDelta($today, $row->{'timestamp'});
                $relativeTimeStr = ' (' . $ts_c . $rel . $r . ')';
            }
            
            # Assign the row_nick value based on if Anonymous is set
            my $nickText = $row->{'nick'};
            my $actionNickText = $row->{'actionnick'};

            # Workaround for potential bug in database
            if ( $nickText =~ /^\$/ )
            {
                # Special case; $a: means account name
                if ( $nickText =~ /^\$a:(.*)$/ )
                {
                    if ( $row_account eq '' )
                    {
                        $row_account = $account_start . $1 . $account_end;
                    }
                    
                }
                $nickText = '';
            }

            if ( defined($anonymous) && $anonymous && $nickText ) {
                $nickText = anonymizeNick($nickText);
            }

            if ( defined($anonymous) && $anonymous && $actionNickText ) {
                $actionNickText = anonymizeNick($actionNickText);
            }

            if ( defined($colornicks) && $colornicks && $nickText ) {
                $nickText = colorizeNick($nickText);
            }

            my $row_nick = $nick_c . $nickText . $r;

            my $row_actionnick = $anick_c . $actionNickText . $r;

            # Add highlighting for any set highlight words
            # First highlight entire line if the nick matches
            my $nickHighlight = '0';
            my $plainNick = $row->{'nick'};
            # Workaround for potential bug in database
            if ( $plainNick =~ /^\$/ )
            {
                $plainNick = '';
            }
            if ( grep( /^\Q$plainNick\E$/i, @highlightWords ) ) {
                    $nickHighlight = '1';
                    $row_message = $hl_c . $row->{'message'} . $r;
            }

            # Highlight sql_message contents separately unless it's already
            # done via a nick highlight
            unless ($nickHighlight)
            {
                # Highlight the exact $sql_message in case it doesn't
                # match when split below
                if ( defined($sql_message) && $sql_message ne '' )
                {
                    $row_message =~ s/(\Q$sql_message\E)/$hl_c$1$r/ig;
                }

                # Sphinx will find matching messages that aren't exactly
                # in $sql_message, so highlight each word in $sql_message
                if ( defined($sql_message) && $sql_message ne '' )
                {
                    # Split on space and dash, maybe should include more chars
                    my @sql_message_words = split(/[\ -]/, $sql_message);
                    foreach my $sql_message_word (@sql_message_words) {
                        $row_message =~ s/(\Q$sql_message_word\E)/$hl_c$1$r/ig;
                    }
                }
            }

        # If we're showing lines around, color string if the Sphinx ID matches
            if ( defined($aroundLines) ) {
                if (@sphinxids) {
                    foreach my $s_id (@sphinxids) {
                        if ( $row->{'id'} == $s_id->{'id'} ) {
                            $row_message = $around_c . $row->{'message'} . $r;
                        }
                    }
                }

                # If we're coming from an ignorestat query, highlight
                elsif ( defined($ignoredid) && $row->{'id'} == $ignoredid ) {
                    $row_message = $around_c . $row->{'message'} . $r;
                }
            }

            #if ( defined($row->{'username'}) && $row->{'username'} ne '' )
            if ( $row->{'username'} ne '' ) {
                $row_username
                    = $user_c . $username_text . $r . $at_c . '@' . $r;
            }

            # Filter out printed rows for convo query
            if ( defined($nickConvo) && $nickConvo ne '' ) {
                my $convoNick = $row->{'nick'};
                # Workaround for potential bug in database
                if ( $convoNick =~ /^\$/ )
                {
                    $convoNick = '';
                }
                $nickConvo =~ s/\ //g;
                my ( $nickone, $nicktwo ) = split( ',', cleanNicks($nickConvo) );
                if ( lc($nickone) eq lc($convoNick) ) {
                    next if ( $row_message !~ /.*\Q$nicktwo\E.*/i );
                }
                if ( lc($nicktwo) eq lc($convoNick) ) {
                    next if ( $row_message !~ /.*\Q$nickone\E.*/i );
                }
            }

            # Highlight the timestamp if it's a new day
            if ( $prev_date ne '' && $prev_date ne $last_date )
            {
                # Highlight if today
                my ($todaystr,undef) = split('T', $today);
                my $colorstr = $c_b;
                if ( $last_date eq $todaystr )
                {
                    $today_flag++;
                }

                $row_timestamp  = $colorstr . $ts . $r;
            }

            # Highlight all timestamps from today if showing multiple days
            if ( $today_flag )
            {
                $row_timestamp  = $hl_c . $ts . $r;
            }

            if ( $row->{'action'} eq "SAY" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );

                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $ll_c . ' ['
                    . $r
                    . $row_channel
                    . $ll_c . '] '
                    . $r
                    . $nick_start
                    . $row_nick
                    . $nick_end . ' '
                    . $row_message . "\n";
            }

            elsif ( $row->{'action'} eq "EMOTE" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $ll_c . ' ['
                    . $r
                    . $row_channel
                    . $ll_c . '] '
                    . $r
                    . $row_nick . ' '
                    . $row_message . "\n";
            }

            elsif ( $row->{'action'} eq "JOIN" ) {
                next if defined($noJoinPart);
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $symbol_c . ' --> '
                    . $r
                    . $row_nick
                    . $row_account
                    . $host_start
                    . $row_username
                    . $row_hostname
                    . $host_end . ' '
                    . $row_channel . "\n";

            }

            elsif ( $row->{'action'} eq "PART" ) {
                next if defined($noJoinPart);
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $symbol_c . ' <-- '
                    . $r
                    . $row_nick
                    . $host_start
                    . $row_hostname
                    . $host_end . ' '
                    . $ll_c . '('
                    . $r
                    . $row_message
                    . $ll_c . ') '
                    . $r
                    . $r
                    . $row_channel . "\n";
            }

            elsif ( $row->{'action'} eq "QUIT" ) {
                next if defined($noJoinPart);

                # If this was a real K-Line, highlight it
                if ( $row->{'message'} =~ /^K-Lined$/ )
                {
                    $row_message = $bold . $c_r . $row->{'message'} . $r;
                }
                
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $symbol_c . ' *-- '
                    . $r
                    . $row_nick
                    . $ll_c . ' ('
                    . $r
                    . $row_message
                    . $ll_c . ') '
                    . $r
                    . $row_channel . "\n";
            }

            elsif ( $row->{'action'} eq "KICK" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $symbol_c . ' <-X '
                    . $r
                    . $row_nick
                    . ' kicked by '
                    . $row_actionnick
                    . $ll_c . ' ('
                    . $r
                    . $row_message
                    . $ll_c . ') '
                    . $r
                    . $row_channel . "\n";
            }

            elsif ( $row->{'action'} eq "NICK" ) {
                next if defined($noNick);
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $symbol_c . ' <-> '
                    . $r
                    . $row_orignick
                    . ' is now '
                    . $row_nick . "\n";
            }

            elsif ( $row->{'action'} eq "MODE" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );

                # Print the data that's available; it can depend on the
                # mode etc what is actually populated
                print $date . ' '
                        . $time
                        . $relativeTimeStr
                        . ' mode/'
                        . $row_channel . ' '
                        . $row_message;
                print  ' ' . $nick_start
                        . $row_nick
                        . $nick_end if ( $row->{'nick'} && $nickText );
                print $host_start
                         . $row_hostname
                         . $host_end if ( $row->{'hostname'} );
                print $row_account if ( $row_account );
                print ' by ' . $row_actionnick . "\n";
            }

            elsif ( $row->{'action'} eq "TOPIC" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . ' Topic for '
                    . $row_channel
                    . ' changed by '
                    . $row_actionnick . ' to: '
                    . $row_message . "\n";
            }

            elsif ( $row->{'action'} eq "NOTICE" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time 
                    . $relativeTimeStr
                    . ' -'
                    . $row_nick . ':'
                    . $row_channel . '- '
                    . $row_message . "\n";
            }

            elsif ( $row->{'action'} eq "QUERY" ) {
                my ( $date, $time ) = split( / /, $row_timestamp );
                print $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $ll_c . ' ['
                    . $r
                    . $row_channel
                    . $ll_c . '] '
                    . $nick_start
                    . $row_nick
                    . $nick_end . ' '
                    . $row_message . "\n";
            }

            # Nothing specific matches; print something
            else {
                my ( $date, $time ) = split( / /, $row_timestamp );
                print '* '
                    . $date . ' '
                    . $time
                    . $relativeTimeStr
                    . $symbol_c . ' ?-- '
                    . $r
                    . $row_nick
                    . $ll_c . ' ('
                    . $r
                    . $row_message
                    . $ll_c . ') ('
                    . $r
                    . $row_channel
                    . $ll_c . ')'
                    . $r . "\n";
            }

            ($prev_date, undef) = split(/ /, $ts);

        }
    }

    # Dump any nicks we found in this query
    # Kind of silly here, but if verbose basically nicks are separated
    # by space, terse separated by commas which is useful to search
    # again for other things
    if ( ( ! $testImport ) && ! defined($nolog) &&
         ( defined($verbose) || defined($terse) ) ) {
        # De-duplicate array
        my @cleanDumpNicks = uniqueArray(@dumpNicks);

        my $nickCount = @cleanDumpNicks;

        print "\n";

        if ( $nickCount > 0 )
        {
            my $dumpList;
            $dumpList = join( ' ', @cleanDumpNicks ) if $verbose;
            $dumpList = join( ',', @cleanDumpNicks ) if $terse;
            queryLog( 'INFO', $login,
                      'Nicks found in this query: '
                    . $hl_c
                    . $dumpList
                    . $r );
        }
    }

    # If dumpactivity is specified, clean up and re-query against the nicks
    # we gathered earlier
    if ( defined($dumpActivity) ) {
        dumpUsers(@dumpNicks);
    }

    if ( defined($showLast) && $last_ts ) {
        my $last_dt     = str2dt($last_ts);
        my $deltastring = tsHumanDelta( $today, $last_dt );

        print "\n" . $deltastring . "\n";
    }

    return;
}

# Find N lines around the passed-in parameters
sub aroundSQL {
    my ($tempsql) = @_;
    my @rows      = doQuery($tempsql);
    my $rows      = @rows;
    my @chatrows;

    # Get default setting for aroundmins if not specified
    unless ( $aroundmins )
    {
        $aroundmins = $chatdb::aroundmins;
    }

    # Restrict our results, unless we're looking for all context (aroundchat)
    if ( $rows > $maxrows && !defined($aroundChat) ) {
        queryLog( 'INFO', $login,
                  $c_r
                . 'More than '
                . $maxrows
                . ' rows returned ('
                . $rows
                . "), refine your query.  Exiting."
                . $r );
        exit;
    }

    my $currentrow = 1;

    # If no lines are given, use the default specified in the module
    if ( $aroundLines == 0 ) {
        $aroundLines = $aroundlines;
    }

    # We really want one more than what we passed in so we get N on each side
    $aroundLines++;

    # Really should do this in a more generic fashion, but let's handle action
    # so we can filter only on SAY and not join/parts etc
    # Also have to handle multiple options here, just like in buildSQL
    my $actionSQL = '';
    if ( defined($sql_action) ) {
        my @splitactions     = split( ',', $sql_action );
        my $splitactioncount = @splitactions;

        if ( $splitactioncount == 1 ) {

            # If we're asking for an action which is invalid, fail
            if ( !grep( /^$sql_action$/i, @allActions ) ) {
                die $sql_action
                    . ' is not in the valid list of actions, exiting' . "\n";
            }

            $actionSQL .= 'and action = \'' . $sql_action . '\' ';
        }
        else {
            my $or        = '';
            my $loopcount = '1';

            $actionSQL .= ' and ( ';

            foreach my $splitaction (@splitactions) {

                # If we're asking for an action which is invalid, fail
                if ( !grep( /^$splitaction$/i, @allActions ) ) {
                    die $splitaction
                        . ' is not in the valid list of actions, exiting'
                        . "\n";
                }

                # Determine if we need to add 'or' to the end or not
                if ( $loopcount < $splitactioncount ) {
                    $or = ' or ';
                }
                else {
                    $or = ' ';
                }

                $actionSQL .= 'action = \'' . $splitaction . '\'' . $or;

                $loopcount++;
            }
            $actionSQL .= ' ) ';
        }
    }

    foreach my $row (@rows) {

        # If there are multiple rows; prefix them to separate them out a bit
        if ( $rows > 1 && !defined($aroundChat) ) {
            print '---------- ROW ' . $currentrow . " ----------\n";
            $currentrow++;
        }

        # Now, build a custom sql query to get the rows before
        my $channel = $row->{'channel'};

        # Get the time span we're interested in
        my $ts = $row->{'timestamp'};
        my ( $tsstart, $tsend ) = tsSpan( $ts, $aroundmins );

        # Get the top N rows using the channel and timestamp from the match
        my $topsql
            = 'select * from '
            . $db{'table'}
            . ' where channel = \''
            . $channel
            . '\' and timestamp between \''
            . $tsstart
            . '\' and \''
            . $ts . '\''
            . $actionSQL
            . ' order by timestamp desc limit '
            . $aroundLines;

        print "aroundSQL topsql: [$topsql]\n" if $DEBUG;
        my @startrows = reverse( doQuery($topsql) );

        # Get the last N rows
        my $bottomsql
            = 'select * from '
            . $db{'table'}
            . ' where channel = \''
            . $channel
            . '\' and timestamp between \''
            . $ts
            . '\' and \''
            . $tsend . '\''
            . $actionSQL
            . ' order by timestamp asc limit '
            . $aroundLines;

        print "aroundSQL bottomsql: [$bottomsql]\n" if $DEBUG;
        my @endrows = doQuery($bottomsql);

        # Get rid of the first row; same as our last row above
        shift(@endrows);

        # Put them together and print them
        my (@allrows) = ( @startrows, @endrows );
        my @uniquerows = uniqueDBRows(@allrows);
        if ( !defined($aroundChat) ) {
            printRows(@uniquerows);
        }
        else {
            push( @chatrows, @allrows );
        }

        # If there are more sections to print, newline
        if ( $currentrow <= $rows && !defined($aroundChat) ) {
            print "\n";
        }
    }

    # If we asked for around chat, clean things up and output
    if ( defined($aroundChat) ) {
        my @uniquerows = uniqueDBRows(@chatrows);
        printRows(@uniquerows);

        # Update how many rows we found
        $rowsreturned = @uniquerows;
    }
    return;
}

# Search Sphinx against the column specified for the passed in string;
# returns the id's that match
sub searchSphinx {
    my ( $index, $string ) = @_;

    # Leaving this in, some day maybe I'll figure this out
    # These chars in sphinx need escaping, however, everything
    # I try it's as if they aren't in ther query at all so it
    # does not seem to matter
    # \\ ( ) | - ! @ ~ " & / ^ $ =
    #$string =~ s/(\\|\(|\)|\||\-|\!|\@|\~|\"|\&|\/|\^|\$)/\\$1/g;

    # Get our list of id's that match this string
    my $sphinxsql
        = 'select id from ' . $index . ' where match(\'' . $string . '\')';

    # If a range of time is defined, limit our sphinx results to match
    if ( defined($sql_timestamp) && defined($sql_enddate) ) {

        # Get the start and end epoch timestamp
        # Convert to local time so Sphinx and MySQL timestamps match up
        my $sphinxstart = str2dt($sql_timestamp)->set_time_zone('local');
        my $sphinxend   = str2dt($sql_enddate)->set_time_zone('local');
        my $startEpoch  = $sphinxstart->epoch;
        my $endEpoch    = $sphinxend->epoch;
        $sphinxsql
            .= ' and ( timestamp >= '
            . $startEpoch
            . ' and timestamp <= '
            . $endEpoch . ')';
    }

    # Sphinx orders by the weight by default but a sane default seems to be
    # ordering by date but you can over-ride with the weight parameter
    unless ( defined($sphinxWeight) && $sphinxWeight eq '1' ) {
        $sphinxsql .= ' order by timestamp desc';
    }

    # Extend Sphinx's artificial limit of 20 results; set in chatdb.pm
    $sphinxsql
        .= ' limit '
        . $sphinxResults
        . ' OPTION max_matches='
        . $sphinxResults

        #. ',ranker=sph04'
        . ';';

    if ( $DEBUG >= 99 ) {
        print "sphinxsql: [$sphinxsql]\n\n";
    }

    my @ids = doSphinxQuery($sphinxsql);

    return @ids;
}

# Find all nicks this nick has used.  Finds all hostnames associated with
# this nick first, then all nicks assocated with those hostnames
sub stalker {
    my @nicksPassed = @_;

# Take the incoming array, flatten, remove spaces, and init our array with it
# Handles commas, spaces, or comma spaces for input, such as stalk -terse output
    my $nickList = join( '', @nicksPassed );

    # Convert any spaces to commas
    $nickList =~ s/\ /,/g;

    # Convert any double commas to single commas
    $nickList =~ s/,,/,/g;
    @nickarray = split( ',', $nickList );

    # Add this nick into the array to loop on.  For an array, this basically
    # just assigns the first nick in the list.
    ($stalkerNick) = @nickarray;

    # Create a time sorted array for stalkerSay, including passed-in list
    my @stalkerSayNicks = @nickarray;

    # Check the the initial nick for length
    if ( length($stalkerNick) > $stalkerNickMax ) {
        queryLog( 'HEADER', $login,
                  'WARNING!  '
                . $stalkerNick
                . ' is > than '
                . $stalkerNickMax
                . ' characters and may be truncated.' );
    }

    my $lastnickcount = '1';
    my $lasthostcount = '0';
    my $stalkerBreak  = '0';
    my $stalkerLoops  = '0';

    my $nickLoop = '1';

    # If noblacklist is specified clear out blacklists before starting
    if ( defined($noBlacklist) ) {
        @hostBlackList = ();
        @nickBlackList = ();
    }

    # Blacklist anything specified on command line
    if ( defined($blacklist) ) {

        # Replace any spaces with commas
        $blacklist =~ s/\ /,/g;

        # Split up entries for adding to arrays
        my @tmpBlacklist = split( ',', $blacklist );

        # Remove any empty entries, such as double space, double commas, etc
        @tmpBlacklist = grep { $_ ne '' } @tmpBlacklist;

        # Add them to both host and nick blacklists for simplicity
        push( @hostBlackList, @tmpBlacklist );
        push( @nickBlackList, @tmpBlacklist );
    }

    my $ts = logtimestamp();
    stalkerLog( 'LOGONLY', $login,
        '[' . $ts . '] Stalking ' . $stalkerNick . "\n" );

    # Put reference in our main log too
    my $extra
        = ' (excluding data beyond ' . $hl_c . $stalkerMaxAge . $r . ' days)'
        if $stalkerAgeExclude;

    queryLog( 'HEADER', $login,
        'Stalking ' . $hl_c . $stalkerNick . $r . $extra );

    # Remove any nicks that aren't in our database
    @nickarray = cleanStalkerNicks(@nickarray);

# Populate newNickArray with nicks to start with initially
@newNickArray = @nickarray;

# Need to pre-populate this if using onlyNicks
@newNickChangeArray = @nickarray if defined($onlyNicks);

 # To make sure we've found all nicks and hosts, we loop over the entire
 # thing until we find no new hosts, no new nicks, or we exceed our loop limit
    while ( $stalkerLoops <= $stalkerMaxLoops && $stalkerBreak == 0 ) {
        $lastnickcount = $nickcount;
        $lasthostcount = $hostcount;

        # Find all the hosts these nicks have used
        # Populates @hostarray @newHostArray and @newAccountArray
        unless ( defined($onlyNicks) ) {
            #foreach my $nick (@nickarray) {
            foreach my $nick (@newNickArray) {
                stalkerHostsFromNick($nick);
            }
            # Copy newNickArray this to use in stalkerNickChanges
            # This way we can clean out newNickArray and not have dups
            # Pick up new nicks from the end of our loop
            @newNickChangeArray = @newNickArray;
            undef @newNickArray;
        }

        #unless ( defined($onlyHosts) ) {
            # Now, get the nicks associated with each hostname we found
            # Populates @nickarray and @newNickArray
            #foreach my $hostname (@hostarray) {
            foreach my $hostname (@newHostArray) {
                stalkerNicksFromHost($hostname);
            }

            # Nothing else uses hosts in this loop, clean out array
            undef @newHostArray;
        #}

        # Populates nickarray newNickArray hostarray newHostArray
        unless ( defined($onlyNicks) ) {
            #foreach my $account (@accountarray) {
            foreach my $account (@newAccountArray) {
                stalkerDataFromAccount($account);
            }
            # Add newly found nicks for stalkerNickChanges
            @newNickChangeArray = (@newNickChangeArray, @newNickArray);
            # Clear out this as it's no longer needed
            undef @newAccountArray;
        }
# If a deep search is requested, also get all matching nicks from our current nick list
# from nick changes.  This can get crazy very fast.
# We always run this maxNickLoops times unless hostsOnly

        unless ( defined($onlyHosts) ) {
            # Determine how many times to look for nicks from $deepStalk
            my $maxNickLoops = $defaultDeepLoops;
            $maxNickLoops = $deepStalk if ( defined($deepStalk) && $deepStalk > 0 );

            if ( $nickLoop <= $maxNickLoops ) {

                # Make a copy first; modifying what we loop over is a bad idea
                #my @tempnickarray = @nickarray;
                my @tempnickarray = @newNickChangeArray;
                # Populates @nickarray and @newNickArray
                foreach my $nick (@tempnickarray) {
                    # And make sure there are no more nicks to add to our array
                    stalkerNickChanges($nick);
                }
                $nickLoop++;
            }

            # This array can be cleared out unless we're using onlyNicks,
            # in which case it'd never retain the previous list of nicks
            # to look for data for.  No efficiency gained here currently.
            if ( defined($onlyNicks))
            {
                @newNickChangeArray = @nickarray;
            }
            else
            {
                undef @newNickChangeArray;
            }
        }

        $hostcount = @hostarray;
        $nickcount = @nickarray;
        $accountcount = @accountarray;

        $stalkerLoops++;

        # Break out of our loop if our host and nick counts aren't changing
        $stalkerBreak++ if (   $hostcount == $lasthostcount
            && $nickcount == $lastnickcount );

        # If our list is getting too big, go ahead and bail out
        if ( $nickcount > $maxnicks ) {
            stalkerLog( 'INFO', $login,
                      'Nick count ('
                    . $nickcount
                    . ') has exceeded maximum of '
                    . $maxnicks
                    . "\n" );
            $stalkerBreak++;
        }

        if ( $maxMaxHostsCounter > $maxMaxHosts ) {
            stalkerLog( 'LOGONLY', $login,
                      'Maximum, maximum hosts found ('
                    . $maxMaxHosts
                    . '), aborting '
                    . "\n" );
            $stalkerBreak++;
        }   
    }

    # Make sure we have the final count
    $nickcount = @nickarray;
    $hostcount = @hostarray;

    # Magic number to show N/A for days if we don't otherwise know
    my $fakeDays = '999999';

    # Now that all nicks are collected, print them out
    if ( $hostcount > 0 || $nickcount > 1 ) {

        if ( $totalNickBlacklists > 0 || $totalHostBlacklists > 0 ) {
            print "\n";
            my $blstring
                = $hl_c
                . $stalkerNick
                . $r
                . ' included blacklists.  (Blacklisted '
                . $totalNickBlacklists
                . ' nicks and '
                . $totalHostBlacklists
                . ' hosts)' . "\n";
            stalkerLog( 'INFO', $login, $blstring );
            print $r;
        }

        # Create a colored string to print to console
        my $string
            = "\n"
            . 'Found '
            . $hl_c
            . $nickcount
            . $r
            . ' nicks on '
            . $hl_c
            . $hostcount
            . $r
            . ' hosts: '
            . $hl_c;

        stalkerLog( 'INFO', $login, $string );

      # Right now our nickarray winds up with more entries than our
      # stalkerDetails hash, so, attempt to combine the two so we don't
      # miss entries.  (TBD:  Why?  Nick only maybe?)
      # Seems to not happen unless we run out hosts to check, so -maxhosts 100
      # loses data, but -maxhosts 1000 does not.  Still unexpected.
      # Feels like I'm working around a bug somewhere else
        my @sdArray;
        foreach my $nickKey ( keys %stalkerDetails ) {
            my ( $days, $nick ) = split( $stalkerSep, $nickKey );
            push( @sdArray, $nick );
        }

        foreach my $nick (@nickarray) {
            my $regex = quotemeta($nick);
            if ( !grep( /^$regex$/i, @sdArray ) ) {
                my $key = $fakeDays . $stalkerSep . $nick;
                $stalkerDetails{$key} = '1' . $stalkerSep . 'N/A';
            }
        }

        # Print found nicks sorted by age
        foreach my $nickKey ( sort keys %stalkerDetails ) {
            my ( $days, $nick ) = split( $stalkerSep, $nickKey );
            push( @stalkerSayNicks, $nick );
            if ($terse) {
                stalkerLog( 'INFO', $login, $nick . ',' );
            }
            else {
                # Remove leading zeros
                $days =~ s/^0+(?=\d)//;
                my $dayMarker = '';
                if ( $days >= $stalkerMaxAge && $days != $fakeDays ) {
                    $dayMarker = '*';
                }
                $days = 'N/A' if $days == $fakeDays;
                stalkerLog( 'INFO', $login,
                    $nick . ' (' . $days . $dayMarker . ') ' );
            }
        }
        print $r;

        if ( $accountcount > 0 )
        {
            print "\n";
            # Create a colored string to print to console
            my $string
                = "\n"
                . 'Found '
                . $hl_c
                . $accountcount
                . $r
                . ' accounts: '
                . $hl_c;

            stalkerLog( 'INFO', $login, $string );

            my $accountlist;
            $accountlist = join(' ', @accountarray) unless ($terse);
            $accountlist = join(',', @accountarray) if ($terse);
            stalkerLog( 'INFO', $login, $accountlist );
            print $r;
        }
    }

   # No hosts or nicks found, and nothing blacklisted.  Likely an invalid nick
    elsif (( $hostcount == 0 || $nickcount == 1 )
        && $totalNickBlacklists == 0
        && $totalHostBlacklists == 0 )
    {
        # Create a colored string to print to console
        my $string
            = "\n"
            . $hl_c
            . $stalkNick
            . $r
            . ' may not be a valid nick.  No additional nicks or hosts found and no blacklisting has been done.';
        if ( length($stalkNick) == $nickmax ) {
            $string
                .= ' Nick is '
                . $hl_c
                . $nickmax
                . $r
                . ' chars long, may be '
                . $c_r
                . 'truncated'
                . $r
                . '?  Try -nt';
        }
        stalkerLog( 'INFO', $login, $string );
        print $r;
    }

    # If we found no real history, just print the nick itself
    elsif ($hostcount == 0
        || $nickcount == 1
        && ( $totalNickBlacklists > 0 || $totalHostBlacklists > 0 ) )
    {
        # Create a colored string to print to console
        my $string
            = "\n"
            . 'Did not find any additional history on '
            . $hl_c
            . $stalkNick
            . $r
            . ' (Blacklisted '
            . $totalNickBlacklists
            . ' nicks and '
            . $totalHostBlacklists
            . ' hosts)';
        stalkerLog( 'INFO', $login, $string );
        print $r;
    }
    else {
        stalkerLog( 'INFO', $login,
            "\n" . 'Found no matches for ' . $stalkNick );
    }

    # Dump detailed information on each match if in verbose mode
    if ($verbose) {
        stalkerLog( 'INFO', $login,
            "\n\n" . 'Detailed history for ' . $hl_c . $stalkerNick . $r . "\n\n" );

        # Copy nickarray and remove entries for stalkernicks
        # so our output is more complete
        my @nickarray_remain = @nickarray;
        
        foreach my $nickKey ( sort keys %stalkerDetails ) {
            my $dayMarker = '';
            my ( $days, $nick ) = split( $stalkerSep, $nickKey );

            # Remove this nick from the array
            my $regex = quotemeta($nick);
            @nickarray_remain = grep {!/$regex/i} @nickarray_remain;

            # Remove leading zeros
            $days =~ s/^0+(?=\d)//;
            my ( $hostname, $nickts )
                = split( $stalkerSep, $stalkerDetails{$nickKey} );
            if ( $days >= $stalkerMaxAge && $days != $fakeDays ) {
                $dayMarker = '*';
            }
            $days = 'N/A' if $days == $fakeDays;
            stalkerLog( 'INFO', $login,
                      $nick . ' '
                    . $hostname . ' ('
                    . $days . ' days'
                    . $dayMarker . ', '
                    . $nickts
                    . ")\n" );
        }

        # If we have nicks still in this array print them out to be complete
        my $nicks_remain = @nickarray_remain;
        if ( $nicks_remain )
        {
            my $nicks = join(', ', @nickarray_remain);
            print "\n";
            stalkerLog( 'INFO', $login, 'The following nicks were not accounted for in Stalker details.  This is likely due to capitalization: ' . $nicks );
            print "\n";
        }

        # Show all found hosts
        my $hostList = join( ' ', @hostarray );

        if ( $hostList && $verbose == 1 )
        {
            stalkerLog( 'INFO', $login,
                "\n" . 'Complete host list: ' . $hl_c . $hostList . $r . "\n" );
        }
        elsif ( $hostList && $verbose > 1 && $hostcount > $maxProxyCheck)
        {
            print "\nToo many hosts to do proxy check ($hostcount > $maxProxyCheck)\n";
            stalkerLog( 'INFO', $login,
                "\n" . 'Complete host list: ' . $hl_c . $hostList . $r . "\n" );
        }
        elsif ( $hostList && $verbose > 1 && $hostcount <= $maxProxyCheck )
        {
            print "\n";
            for (@hostarray)
            {
                my $proxy = proxychecker($_, $login);
                print $ts_c . $_ . $r . "\n";
                print $proxy . "\n\n";
            }
        }
        else
        {
            print "\n";
        }
    }

    my $stalkend     = time;
    my $stalkelapsed = $stalkend - $start;

    # Decrement stalkerRuns to match how many real loops we did
    my $loopsRan   = $stalkerLoops - 1;
    my $elapsedstr = "\n";

    if ( !$verbose ) {
        $elapsedstr .= "\n";
    }

    $elapsedstr
        .= 'Took '
        . $stalkelapsed
        . ' seconds to stalk ' . $hl_c
        . $stalkerNick . $r . ' in '
        . $loopsRan
        . ' loops' . ' ('
        . $stalkerMaxLoops . ' max)' . ' ('
        . $maxMaxHostsCounter
        . ' max hosts hit)' . "\n";

    stalkerLog( 'INFO', $login, $elapsedstr );

    my $stalkerSayNickCount = @stalkerSayNicks;

    # Dump activity from these users
    if ( $stalkerSayNickCount == 0 ) {
        print "\n";
        stalkerLog( 'INFO', $login,
            'Not dumping activity, no nicks found in query.' );
        print "\n";
    }
    elsif ( defined($dumpActivity) ) {
        dumpUsers( @stalkerSayNicks );
    }

    return;
}

# Get all of the nicks that this host has used
sub stalkerNicksFromHost {
    my ($hostname) = @_;
    print "stalkerNicksFromHost hostname: [$hostname]\n" if ( $DEBUG >= 40 );
    my $nicktempsql
        = 'select DISTINCT nick,account,timestamp from '
        . $db{'table'}
        . ' where action = \'JOIN\' and '
        . '  hostname = \''
        . $hostname . '\' ';

    # Limit to timerange if specified
    $nicktempsql .= $tsSQL;

    if ( defined($sql_network) ) {
        $nicktempsql .= 'and network = \'' . $sql_network . '\' ';
    }

    $nicktempsql .= 'order by timestamp desc;';

    my @nickrows = doQuery($nicktempsql);
    foreach my $nickrow (@nickrows) {
        my $n = $nickrow->{'nick'};
        # Workaround for potential bug in database
        if ( $n =~ /^\$/ )
        {
            next;
        }
        my $acct = $nickrow->{'account'};
        unless ( $nickseen{ uc($n) } ) {

            # Exclude any nicks or accounts in our blacklist
            my $blflag = '0';
            foreach my $nickbl (@nickBlackList) {
                if ( $n =~ /^$nickbl$/i || ( $acct && $acct =~ /^$nickbl$/i ) ) {
                    $blflag++;
                    $totalNickBlacklists++;
                }
            }
            if ( $blflag == 0 ) {
                my ( $nickts, undef )
                    = split( / /, $nickrow->{'timestamp'} );

                # How many days old is this nick in the db?
                my $nicktd = str2dt($nickts);
                my $days   = $today->delta_days($nicktd)->delta_days();
                if (   $stalkerAgeExclude == 1
                    && $days > $stalkerMaxAge )
                {
                    stalkerLog( 'LOGONLY', $login,
                              'Ignored old nick: '
                            . $n
                            . ' from hostname '
                            . $hostname . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" )
                        if $DEBUG;
                }
                elsif ($stalkerAgeExclude == 0
                    && $days > $stalkerMaxAge
                    && $nickcount <= $maxnicks )
                {
                    my $regex = quotemeta($n);
                    push( @nickarray, $n )
                        unless ( grep( /^$regex$/, @nickarray ) );
                    push( @newNickArray, $n )
                        unless ( grep( /^$regex$/, @newNickArray ) );
                    print "stalkerNicksFromHost push n: [$n]\n"
                        if ( $DEBUG >= 40 );
                    $nickseen{ uc($n) }++;
                    $nickcount = @nickarray;

                    # Collect all data into hash to potentially display later
                    # Add leading zeros so sort works nicely
                    my $detailskey
                        = sprintf( "%06d%s%s", $days, $stalkerSep, $n );
                    my $detailsdata = $hostname . $stalkerSep . $nickts;
                    $stalkerDetails{$detailskey} = $detailsdata;

                    stalkerLog( 'LOGONLY', $login,
                              'Found nick (old): '
                            . $n
                            . ' from hostname '
                            . $hostname . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" );
                }
                elsif ( $nickcount <= $maxnicks ) {
                    my $regex = quotemeta($n);
                    push( @nickarray, $n )
                        unless ( grep( /^$regex$/, @nickarray ) );
                    push( @newNickArray, $n )
                        unless ( grep( /^$regex$/, @newNickArray ) );
                    print "stalkerNicksFromHost push 2 n: [$n]\n"
                        if ( $DEBUG >= 40 );
                    $nickseen{ uc($n) }++;
                    $nickcount = @nickarray;

                    # Collect all data into hash to potentially display later
                    # Add leading zeros so sort works nicely
                    my $detailskey
                        = sprintf( "%06d%s%s", $days, $stalkerSep, $n );
                    my $detailsdata = $hostname . $stalkerSep . $nickts;
                    $stalkerDetails{$detailskey} = $detailsdata;

                    stalkerLog( 'LOGONLY', $login,
                              'Found nick ('
                            . $stalkerNick . '): '
                            . $n
                            . ' from hostname '
                            . $hostname . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" );
                }
            }
            else {
                queryLog( 'INFO', $login, $n . ' nick matches in blacklist' )
                    if ( $verbose && $DEBUG > 0 );
            }
        }
    }

    return;
}

# Get all of the nicks and hosts that this account has used
sub stalkerDataFromAccount {
    my ($accountname) = @_;
    print "stalkerDataFromAccount accountname: [$accountname]\n" if ( $DEBUG >= 50 );
    my $nicktempsql
        = 'select DISTINCT nick,timestamp from '
        . $db{'table'}
        . ' where action = \'JOIN\' and '
        . '  account = \''
        . $accountname . '\' ';

    # Limit to timerange if specified
    $nicktempsql .= $tsSQL;

    if ( defined($sql_network) ) {
        $nicktempsql .= 'and network = \'' . $sql_network . '\' ';
    }

    $nicktempsql .= 'order by timestamp desc;';
    my @nickrows = doQuery($nicktempsql);
    foreach my $nickrow (@nickrows) {
        my $n = $nickrow->{'nick'};
        # Workaround for potential bug in database
        if ( $n =~ /^\$/ )
        {
            next;
        }
        unless ( $nickseen{ uc($n) } ) {

            # Exclude any nicks in our blacklist
            my $blflag = '0';
            foreach my $nickbl (@nickBlackList) {
                if ( $n =~ /^$nickbl$/i ) {
                    $blflag++;
                    $totalNickBlacklists++;
                }
            }
            if ( $blflag == 0 ) {
                my ( $nickts, undef )
                    = split( / /, $nickrow->{'timestamp'} );

                # How many days old is this nick in the db?
                my $nicktd = str2dt($nickts);
                my $days   = $today->delta_days($nicktd)->delta_days();
                if (   $stalkerAgeExclude == 1
                    && $days > $stalkerMaxAge )
                {
                    stalkerLog( 'LOGONLY', $login,
                              'Ignored old nick: '
                            . $n
                            . ' from accountname '
                            . $accountname . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" )
                        if $DEBUG;
                }
                elsif ($stalkerAgeExclude == 0
                    && $days > $stalkerMaxAge
                    && $nickcount <= $maxnicks )
                {
                    my $regex = quotemeta($n);
                    push( @nickarray, $n )
                        unless ( grep( /^$regex$/, @nickarray ) );
                    push( @newNickArray, $n )
                        unless ( grep( /^$regex$/, @newNickArray ) );
                    print "stalkerDataFromAccount push n: [$n]\n"
                        if ( $DEBUG >= 50 );
                    $nickseen{ uc($n) }++;
                    $nickcount = @nickarray;

                    # Collect all data into hash to potentially display later
                    # Add leading zeros so sort works nicely
                    my $detailskey
                        = sprintf( "%06d%s%s", $days, $stalkerSep, $n );
                    my $detailsdata = $accountname . $stalkerSep . $nickts;
                    $stalkerDetails{$detailskey} = $detailsdata;

                    stalkerLog( 'LOGONLY', $login,
                              'Found nick (old): '
                            . $n
                            . ' from accountname '
                            . $accountname . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" );
                }
                elsif ( $nickcount <= $maxnicks ) {
                    my $regex = quotemeta($n);
                    push( @nickarray, $n )
                        unless ( grep( /^$regex$/, @nickarray ) );
                    push( @newNickArray, $n )
                        unless ( grep( /^$regex$/, @newNickArray ) );
                    print "stalkerDataFromAccount push 2 n: [$n] [$accountname]\n"
                        if ( $DEBUG >= 50 );
                    $nickseen{ uc($n) }++;
                    $nickcount = @nickarray;

                    # Collect all data into hash to potentially display later
                    # Add leading zeros so sort works nicely
                    my $detailskey
                        = sprintf( "%06d%s%s", $days, $stalkerSep, $n );
                    my $detailsdata = $accountname . $stalkerSep . $nickts;
                    $stalkerDetails{$detailskey} = $detailsdata;

                    stalkerLog( 'LOGONLY', $login,
                              'Found nick ('
                            . $stalkerNick . '): '
                            . $n
                            . ' from accountname '
                            . $accountname . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" );
                }
            }
            else {
                queryLog( 'INFO', $login, $n . ' nick matches in blacklist' )
                    if ( $verbose && $DEBUG > 0 );
            }
        }
    }

    # Get hostnames from the specified account
    my $hosttempsql
        = 'select DISTINCT hostname,timestamp from '
        . $db{'table'}
        . ' where action = \'JOIN\' and account = \'' . $accountname . '\' ';

    # Limit to timerange if specified
    $hosttempsql .= $tsSQL;

    if ( defined($sql_network) ) {
        $hosttempsql .= 'and network = \'' . $sql_network . '\' ';
    }

    if ( defined($sql_channel) ) {
        $hosttempsql .= 'and channel = \'' . $sql_channel . '\' ';
    }

    $hosttempsql .= 'order by timestamp desc;';

    print "stalkerDataFromAccount host sql: [$hosttempsql]\n" if $DEBUG >= 10;
    my @hostrows = doQuery($hosttempsql);

    foreach my $hostrow (@hostrows) {
        my $hn = $hostrow->{'hostname'};
        my $an = $hostrow->{'account'};
        if ( $hn ne '' && $verbose ) {
            if ( $hn =~ m/.*@(.*)/x ) {
                queryLog( 'HEADER', $login,
                    'Warning!  Hostname with @ found: ' . $hn );
            }
        }

        # Gather hosts
        unless ( $hostseen{$hn} || $hn eq '' || $hn eq '*' ) {

            # In case a nick has tons of hosts, limit how many we will use
            my $hostsfound = @hostarray;
            if ( $hostsfound >= $maxhosts ) {
                if ( $maxhostsNotice && !$DEBUG ) {
                    stalkerLog( 'LOGONLY', $login,
                        'Maximum hosts found (' . $maxhosts . ")\n" );
                    $maxhostsNotice = '0';
                }
                if ($DEBUG) {
                    stalkerLog( 'LOGONLY', $login,
                              'Maximum hosts found ('
                            . $maxhosts
                            . '), skipping '
                            . $hn
                            . "\n" );
                }

                # Make sure we're not in some nasty loop
                $maxMaxHostsCounter++;
            }
            else {
                # Exclude any hosts in our blacklist
                my $blflag = '0';
                foreach my $hostbl (@hostBlackList) {
                    if ( $hn =~ /$hostbl/i ) {
                        $blflag++;
                        $totalHostBlacklists++;
                    }
                }
                if ( $blflag == 0 ) {
                    my ( $hostts, undef )
                        = split( / /, $hostrow->{'timestamp'} );

                    # How many days old is this host in the db?
                    my $hosttd = str2dt($hostts);
                    my $days   = $today->delta_days($hosttd)->delta_days();
                    if (   $stalkerAgeExclude == 1
                        && $days > $stalkerMaxAge )
                    {
                        stalkerLog( 'LOGONLY', $login,
                                  'Ignored old host: '
                                . $hn
                                . ' from accountname '
                                . $accountname . ' ('
                                . $hostts . ') ('
                                . $days
                                . ' days)'
                                . "\n" )
                            if $DEBUG;
                    }
                    else {
                        push( @hostarray, $hn );
                        push( @newHostArray, $hn );
                        print "stalkerDataFromAccount push host: [$hn]\n"
                            if ( $DEBUG >= 50 );
                        $hostseen{$hn}++;
                        stalkerLog( 'LOGONLY', $login,
                                  'Found hostname: '
                                . $hn
                                . ' from accountname '
                                . $accountname . ' ('
                                . $hostts . ')'
                                . "\n" );
                    }
                }
                else {
                    queryLog( 'INFO', $login,
                        $hn . ' hostname matches in blacklist' )
                        if ( $verbose && $DEBUG > 0 );
                }
            }
        }
    }

    return;
}

# Remove any nicks that were passed in that we don't have in our database
sub cleanStalkerNicks {
    my @passedArray = @_;

    # Array of nicks known in our database
    my @knownArray;

    # Array of nicks that were passed and known
    my @newArray;

    my $nickSQL = '';

    # create sql
    foreach my $nick (@passedArray) {
        $nickSQL .= 'nick = \'' . $nick . '\' OR ';
    }

    # Lazy; remove trailing OR
    $nickSQL =~ s/\ OR\ $//;

    # Get hostnames from the specified nick
    my $tempsql
        = 'select DISTINCT nick from '
        . $db{'table'}
        . ' where '
        . $nickSQL . ';';

    my @nickrows = doQuery($tempsql);

    # Create array of nicks returned from database
    foreach my $nickrow (@nickrows) {
        my $rownick = $nickrow->{'nick'};
        # Workaround for potential bug in database
        if ( $rownick =~ /^\$/ )
        {
            next;
        }
        push( @knownArray, $rownick );
    }

    foreach my $nick (@passedArray) {
        my $regex = quotemeta($nick);
        if ( grep( /^$regex$/i, @knownArray ) ) {

            # We've seen this nick, store it
            push( @newArray, $nick );
        }
    }

    return @newArray;
}

# Get all of the hosts and accounts that this nick has used
sub stalkerHostsFromNick {
    my ($nick) = @_;
    print "stalkerHostsFromNick nick: [$nick]\n" if ( $DEBUG >= 40 );

    # Sanitize the nick for the SQL server
    $nick = db_cleanText($nick);

    # Get hostnames and accounts from the specified nick
    my $hosttempsql
        = 'select DISTINCT hostname,account,timestamp from '
        . $db{'table'}
        . ' where action = \'JOIN\' and ';

    if ( defined($nickTruncate)
        && length($nick) >= $nickmax )
    {
        # Escape some special characters in nick in LIKE statements
        my $nick_esc = $nick;
        $nick_esc =~ s/_/\#_/g;
        $nick_esc =~ s/\[/\#\[/g;
        $nick_esc =~ s/\]/\#\]/g;
        $hosttempsql .= '( nick like \'' . $nick_esc . '\' ESCAPE \'#\') ';
    }
    else {
        $hosttempsql .= ' nick = \'' . $nick . '\' ';
    }

    # Limit to timerange if specified
    $hosttempsql .= $tsSQL;

    if ( defined($sql_network) ) {
        $hosttempsql .= 'and network = \'' . $sql_network . '\' ';
    }

    if ( defined($sql_channel) ) {
        $hosttempsql .= 'and channel = \'' . $sql_channel . '\' ';
    }

    $hosttempsql .= 'order by timestamp desc;';

    print "stalkerHostsFromNick\n$hosttempsql\n" if $DEBUG >= 10;
    my @hostrows = doQuery($hosttempsql);

    foreach my $hostrow (@hostrows) {
        my $hn = $hostrow->{'hostname'};
        my $an = $hostrow->{'account'};
        if ( $hn ne '' && $verbose ) {
            if ( $hn =~ m/.*@(.*)/x ) {
                queryLog( 'HEADER', $login,
                    'Warning!  Hostname with @ found: ' . $hn );
            }
        }

        # Gather hosts
        unless ( $hostseen{$hn} || $hn eq '' || $hn eq '*' ) {

            # In case a nick has tons of hosts, limit how many we will use
            my $hostsfound = @hostarray;
            if ( $hostsfound >= $maxhosts ) {
                if ( $maxhostsNotice && !$DEBUG ) {
                    stalkerLog( 'LOGONLY', $login,
                        'Maximum hosts found (' . $maxhosts . ")\n" );
                    $maxhostsNotice = '0';
                }
                if ($DEBUG) {
                    stalkerLog( 'LOGONLY', $login,
                              'Maximum hosts found ('
                            . $maxhosts
                            . '), skipping '
                            . $hn
                            . "\n" );
                }

                # Make sure we're not in some nasty loop
                $maxMaxHostsCounter++;
            }
            else {
                # Exclude any hosts in our blacklist
                my $blflag = '0';
                foreach my $hostbl (@hostBlackList) {
                    if ( $hn =~ /$hostbl/i ) {
                        $blflag++;
                        $totalHostBlacklists++;
                    }
                }
                if ( $blflag == 0 ) {
                    my ( $hostts, undef )
                        = split( / /, $hostrow->{'timestamp'} );

                    # How many days old is this host in the db?
                    my $hosttd = str2dt($hostts);
                    my $days   = $today->delta_days($hosttd)->delta_days();
                    if (   $stalkerAgeExclude == 1
                        && $days > $stalkerMaxAge )
                    {
                        stalkerLog( 'LOGONLY', $login,
                                  'Ignored old host: '
                                . $hn
                                . ' from nick '
                                . $nick . ' ('
                                . $hostts . ') ('
                                . $days
                                . ' days)'
                                . "\n" )
                            if $DEBUG;
                    }
                    else {
                        push( @hostarray, $hn );
                        push( @newHostArray, $hn );
                        print "stalkerHostsFromNick push: [$hn]\n"
                            if ( $DEBUG >= 40 );
                        $hostseen{$hn}++;
                        stalkerLog( 'LOGONLY', $login,
                                  'Found hostname: '
                                . $hn
                                . ' from nick '
                                . $nick . ' ('
                                . $hostts . ')'
                                . "\n" );
                    }
                }
                else {
                    queryLog( 'INFO', $login,
                        $hn . ' hostname matches in blacklist' )
                        if ( $verbose && $DEBUG > 0 );
                }
            }
        }

        # Gather accounts
        unless ( !defined($an) || $accountseen{$an} || $an eq '' ) {
            my ( $accountts, undef )
                = split( / /, $hostrow->{'timestamp'} );

            # How many days old is this account in the db?
            my $accounttd = str2dt($accountts);
            my $days   = $today->delta_days($accounttd)->delta_days();
            if (   $stalkerAgeExclude == 1
                && $days > $stalkerMaxAge )
            {
                stalkerLog( 'LOGONLY', $login,
                          'Ignored old account: '
                        . $an
                        . ' from nick '
                        . $nick . ' ('
                        . $accountts . ') ('
                        . $days
                        . ' days)'
                        . "\n" )
                    if $DEBUG;
            }
            else {
                push( @accountarray, $an );
                push( @newAccountArray, $an );
                print "stalkerHostsFromNick push: [$an]\n"
                    if ( $DEBUG >= 40 );
                $accountseen{$an}++;
                stalkerLog( 'LOGONLY', $login,
                          'Found account: '
                        . $an
                        . ' from nick '
                        . $nick . ' ('
                        . $accountts . ')'
                        . "\n" );
            }
        }
    }

    return;
}

# Get all of the nicks that this nick has changed to or from
sub stalkerNickChanges {
    my ($nick) = @_;

    # Make sure our nick is clean for using in a query
    $nick = db_cleanText($nick);

    my $nicktempsql
        = 'select DISTINCT nick,orignick,timestamp from '
        . '( select DISTINCT nick,orignick,timestamp from '
        . $db{'table'}
        . ' where action = \'NICK\' and '
        . '  nick = \''
        . $nick . '\' ';

    # Limit to timerange if specified
    $nicktempsql .= $tsSQL;

    if ( defined($sql_network) ) {
        $nicktempsql .= 'and network = \'' . $sql_network . '\' ';
    }

    if ( defined($sql_channel) ) {
        $nicktempsql .= 'and channel = \'' . $sql_channel . '\' ';
    }

    $nicktempsql .= ' union ';

    $nicktempsql
        .= 'select DISTINCT nick,orignick,timestamp from '
        . $db{'table'}
        . ' where action = \'NICK\' and '
        . '  orignick = \''
        . $nick . '\' ';

    # Limit to timerange if specified
    $nicktempsql .= $tsSQL;

    if ( defined($sql_network) ) {
        $nicktempsql .= 'and network = \'' . $sql_network . '\' ';
    }

    if ( defined($sql_channel) ) {
        $nicktempsql .= 'and channel = \'' . $sql_channel . '\' ';
    }

    $nicktempsql .= ') x ';
    $nicktempsql .= 'order by timestamp desc;';

    print "stalkerNickChanges\n$nicktempsql\n" if $DEBUG >= 10;

    my @nickrows = doQuery($nicktempsql);
    foreach my $nickrow (@nickrows) {
        my $n  = $nickrow->{'nick'};
        # Workaround for potential bug in database
        if ( $n =~ /^\$/ )
        {
            next;
        }
        my $on = $nickrow->{'orignick'};

        unless ( $nickseen{ uc($n) } && $nickseen{ uc($on) } ) {

            # Exclude any nicks in our blacklist
            my $blflag = '0';

            # For right now don't blacklist at all in this..  Testing.
            # Allow it when we're using onlynicks mode
            # Commented out for now; not sure why I restricted this, maybe
            # to match Stalker better.  Still really need to do more analysis.

            #if ($onlyNicks) {
            foreach my $nickbl (@nickBlackList) {

              # For nick changes, only check the orignick for nick changes
              # However; think about this more..  This causes a lot of stalker
              # spam with nick vs orignick.  Blacklist is blacklist.  Hmmm.
                if ( $on =~ /^$nickbl$/i || $n =~ /^$nickbl/i ) {
                    $blflag++;
                    $totalNickBlacklists++;
                }
            }

            #}

            if ( $blflag == 0 ) {
                my ( $nickts, undef )
                    = split( / /, $nickrow->{'timestamp'} );

                # How many days old is this nick in the db?
                my $nicktd  = str2dt($nickts);
                my $days    = $today->delta_days($nicktd)->delta_days();
                my $logNick = '';
                if (   $stalkerAgeExclude == 1
                    && $days > $stalkerMaxAge )
                {
                    stalkerLog( 'LOGONLY', $login,
                              'Ignored old nick: '
                            . $n . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days)'
                            . "\n" );
                }
                elsif ($stalkerAgeExclude == 0
                    && $days > $stalkerMaxAge
                    && $nickcount <= $maxnicks )
                {
                    unless ( $nickseen{ uc($n) } ) {
                        push( @nickarray, $n );
                        push( @newNickArray, $n );
                        print "stalkerNickChanges push n: [$n]\n"
                            if ( $DEBUG > 4 );
                        $nickseen{ uc($n) }++;

                     # Collect all data into hash to potentially display later
                     # Add leading zeros so sort works nicely
                        my $detailskey
                            = sprintf( "%06d%s%s", $days, $stalkerSep, $n );
                        my $detailsdata
                            = '(nickonly)' . $stalkerSep . $nickts;
                        $stalkerDetails{$detailskey} = $detailsdata;
                        $logNick = $n;
                    }
                    unless ( $nickseen{ uc($on) } ) {
                        push( @nickarray, $on );
                        push( @newNickArray, $on );
                        print "stalkerNickChanges push on: [$on]\n"
                            if ( $DEBUG > 4 );
                        $nickseen{ uc($on) }++;
                        my $detailskey
                            = sprintf( "%06d%s%s", $days, $stalkerSep, $on );
                        my $detailsdata
                            = '(nickonly)' . $stalkerSep . $nickts;
                        $stalkerDetails{$detailskey} = $detailsdata;

                        $logNick = $on;
                    }
                    $nickcount = @nickarray;
                    stalkerLog( 'LOGONLY', $login,
                              'Warning; old nick: '
                            . $logNick . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days) from '
                            . $nick . ') '
                            . "\n" );
                }
                elsif ( $nickcount <= $maxnicks ) {
                    unless ( $nickseen{ uc($n) } ) {
                        push( @nickarray, $n );
                        push( @newNickArray, $n );
                        print "stalkerNickChanges push n 2: [$n]\n"
                            if ( $DEBUG > 4 );
                        $nickseen{ uc($n) }++;
                        my $detailskey
                            = sprintf( "%06d%s%s", $days, $stalkerSep, $n );
                        my $detailsdata
                            = '(nickonly)' . $stalkerSep . $nickts;
                        $stalkerDetails{$detailskey} = $detailsdata;
                        $logNick = $n;
                    }
                    unless ( $nickseen{ uc($on) } ) {
                        push( @nickarray, $on );
                        push( @newNickArray, $on );
                        print "stalkerNickChanges push on 2: [$on]\n"
                            if ( $DEBUG > 4 );
                        $nickseen{ uc($on) }++;
                        my $detailskey
                            = sprintf( "%06d%s%s", $days, $stalkerSep, $on );
                        my $detailsdata
                            = '(nickonly)' . $stalkerSep . $nickts;
                        $stalkerDetails{$detailskey} = $detailsdata;
                        $logNick = $on;
                    }
                    $nickcount = @nickarray;
                    stalkerLog( 'LOGONLY', $login,
                              'Found nick ('
                            . $nick . '): '
                            . $logNick . ' ('
                            . $nickts . ') ('
                            . $days
                            . ' days) from '
                            . $nick . ') '
                            . "\n" );
                }
            }
            else {
                queryLog( 'INFO', $login,
                          $n . ' or '
                        . $on
                        . ' nick/original nick matches in blacklist' )
                    if ( $verbose && $DEBUG > 0 );
            }
        }
    }

    return;
}

# Do a 'fuzzy' search for a nick
sub nickSearch {

    # If nickSeach is empty, see if sql_nick is set and use it
    if ( $nickSearch eq "" 
        && ( defined($sql_nick) && $sql_nick ne "" )
        )
    {
        $nickSearch = $sql_nick;
    }

    # If still empty, bail out
    if ( $nickSearch eq "" ) {
        die "No nick provided.\n";
    }

    my $tempsql = '';

    # If we have Sphinx, use it, isntant results vs minutes
    if ( defined($useSphinx) && $useSphinx ) {
        queryLog( 'HEADER', $login,
                  'Doing a similar (soundex) search on '
                . $hl_c
                . $nickSearch
                . $r
                . ', using Sphinx..' );
        print STDERR "\n";

        $tempsql = 'select distinct nick from ' . $db{'table'} . ' where ';

        # Filter by channel if specified
        if ( defined($sql_channel) && $sql_channel ) {
            $tempsql .= ' channel = \'' . $sql_channel . '\' and ';
        }

        my $addComma = '0';

        @sphinxids = searchSphinx( 'nick', $nickSearch );
        my $sphinxids = @sphinxids;

        # Combine the delta index results
        my @deltasphinxids = searchSphinx( 'nickdelta', $nickSearch );
        my $deltaids       = @deltasphinxids;
        @sphinxids          = ( @sphinxids, @deltasphinxids );
        $sphinxRowsReturned = @sphinxids;

        if ( $sphinxRowsReturned > 0 ) {
            $tempsql .= ' id IN (';
            foreach my $s_id (@sphinxids) {
                if ($addComma) {
                    $tempsql .= ', ';
                }
                $tempsql .= $s_id->{'id'};
                $addComma = '1';
            }
            $tempsql .= ') ';
        }

        # If we found no rows, just bail at this point
        else {
            queryLog( 'INFO', $login, 'Sphinx did not find any matches.' );
            return;
        }
    }
    else {
        queryLog( 'HEADER', $login,
                  'Doing a similar (soundex) search on '
                . $hl_c
                . $nickSearch
                . $r
                . ', this will likely be slow..' );
        print STDERR "\n";
        $tempsql
            = 'select distinct nick from '
            . $db{'table'}
            . ' where soundex(nick) = soundex(\''
            . $nickSearch . '\') ';

        # Limit to timerange if specified
        $tempsql .= $tsSQL;

        $tempsql .= ' order by timestamp desc limit ' . $nickSearchMax . ';';
    }

    my @nicksfound;
    my $exactNick = '0';

    my @nickrows = doQuery($tempsql);

    foreach my $nickrow (@nickrows) {
        my $nickRowNick = $nickrow->{'nick'};
        # Workaround for potential bug in database
        if ( $nickRowNick =~ /^\$/ )
        {
            next;
        }

        # We get ban masks in here sometimes, filter them out
        next if $nickRowNick =~ /\$a:/;
        if ( lc($nickRowNick) eq lc($nickSearch) ) {
            $exactNick = 1;
        }
        if ( $nickcount <= $maxnicks ) {
            push( @nicksfound, $nickRowNick );
        }
    }

    my $nicklist = join( ' ', sort @nicksfound );
    my $nscount  = @nicksfound;

    # If we found an exact match, call it out
    if ($exactNick) {
        queryLog( 'INFO', $login,
            'Exact match found: ' . $onick_c . $nickSearch . $r );
        print "\n";
    }

    queryLog( 'INFO', $login,
        $nscount . ' Nicks found: ' . $hl_c . $nicklist . $r );
    print "\n";

    my $end     = time;
    my $elapsed = $end - $start;

    my $elapsedstr
        = 'Took ' . $elapsed . ' seconds to search for ' . $nickSearch;

    queryLog( 'INFO', $login, $elapsedstr );

    return;
}

# Clone scanner
sub cloneScan {

    my $clone_limit = $cloneLimitDefault;

    $clone_limit = $clones if ( defined($clones) );

    queryLog( 'HEADER', $login,
          'Doing a clone scan (' . $hl_c 
          . $clone_limit . $r
          . '), this will likely be slow..' );

    my %hosts;
    my %accounts;

    # Build channel SQL
    my $channel_sql = ' ';
    if ( defined($sql_channel) && $sql_channel )
    {
        $channel_sql = ' AND channel = \''. $sql_channel . '\'';
    }

    # Build initial timespan SQL
    my $ts_sql = ' ';
    if ( ! defined($sql_timestamp) || ! defined($sql_enddate) ) {
        print "\n" . 'You must define the initial timeperiod; exiting' . "\n";
        exit;
    }
    else
    {
        $ts_sql = ' AND timestamp BETWEEN \'' . $sql_timestamp
            . '\' AND \'' . $sql_enddate . '\'';
    }

    my $hostsql
        = 'SELECT channel,hostname,account,nick FROM '
        . $db{'table'}
        . ' WHERE ACTION = \'JOIN\''
        . $channel_sql
        . $ts_sql . ';';

    my @hostList = doQuery($hostsql);

    # Track known nick+host combinations to remain unique
    my @known;
    my $knownaccounts;

    for ( @hostList )
    {
        my $host = $_->{'hostname'};
        my $nick = $_->{'nick'};
        # Workaround for potential bug in database
        if ( $nick =~ /^\$/ )
        {
            $nick = '';
        }
        my $channel = $_->{'channel'};
        my $account = $_->{'account'};

        # See if the channel is in the excluded list
        my $excluded = '0';

        foreach my $excludedChannel (@excludeChans)
        {
            next unless $excludedChannel;

            $excluded++ if ( $channel =~ $excludedChannel );
        }

        # Skip this row if excluded
        if ( $excluded )
        {
            next;
        }

        # Loop through blacklist 
        my $isBL = '0';
        foreach my $blHost (@cloneHostsBlacklist)
        {
            if ($host =~ /$blHost/)
            {
                #print "$host matches $blHost\n";
                $isBL++;
            }
        }

        # If we matched a blacklist, skip this host
        next if $isBL;

        # Get rid of any underlines
        $nick =~ s/\_+$//;
        my $regex = quotemeta($nick . $host);

        unless ( grep(/^$regex$/, @known) )
        {
            # If there is an account, create a separate dataset for it
            if ( $account )
            {
                if ( defined($accounts{$account}) )
                {
                    $accounts{$account} .= ' ' . $nick;
                }
                else
                {
                    $accounts{$account} = $nick;
                }
            }

            push(@known,$nick . $host);
            if ( defined($hosts{$host}) )
            {
                $hosts{$host} .= ' ' . $nick;
            }
            else
            {
                $hosts{$host} = $nick;
            }
        }
    }

    my $hosts = keys %hosts;
    if ( $hosts )
    {
        print "\n";
        my $headershown = '0';
        
        # If the number of nicks in this host exceeds the limit, print it out
        foreach my $key (keys %hosts)
        {
            my @nicks = split(' ', $hosts{$key});
            my @cleannicks = uniqueArray(@nicks);
            my $nicks = @cleannicks;
            if ( $nicks >= $clone_limit )
            {
                unless ( $headershown )
                {
                    queryLog( 'HEADER', $login, 'Showing clone ' . $hl_c . 'hosts' . $r );
                    $headershown++;
                }
                print $key  . ' (' . $ts_c . $nicks . $r . '): ' . $hl_c . $hosts{$key} . $r . "\n";
            }
        }

        queryLog( 'HEADER', $login, 'No host clones found' ) unless $headershown;
    }

    my $accounts = keys %accounts;
    if ( $accounts )
    {
        print "\n";
        # Hide header unless there's data
        my $headershown = '0';
        foreach my $key (keys %accounts)
        {
            my @nicks = split(' ', $accounts{$key});
            my @cleannicks = uniqueArray(@nicks);
            my $nicks = @cleannicks;
            if ( $nicks >= $clone_limit )
            {
                unless ( $headershown )
                {
                    queryLog( 'HEADER', $login, 'Showing clone ' . $hl_c . 'accounts' . $r );
                    $headershown++;
                }
                print $key  . ' (' . $ts_c . $nicks . $r . '): ' . $hl_c . $accounts{$key} . $r . "\n";
            }
        }

        queryLog( 'HEADER', $login, 'No account clones found' ) unless $headershown;
    }
    
    return;
}

### Nick stats/queries

# Get information on an ignored nick
sub ignoredUserStats {
    queryLog( 'HEADER', $login,
              'Searching for the first time we see '
            . $hl_c
            . $ignoreStats
            . $r
            . ' ignored, this will likely be slow..' );
    print STDERR "\n";
    my $tempsql
        = 'select * from '
        . $db{'table'}
        . ' where nick = \''
        . $ignoreStats
        . '\' and ignored = \'1\' order by timestamp asc limit 1;';

    my @rows = doQuery($tempsql);

# If we specified the around query, also show us lines around when the user was ignored
    if ( defined($aroundLines) ) {
        foreach my $row (@rows) {
            $ignoredid = $row->{'id'};
            my $query
                = 'select * from '
                . $db{'table'}
                . ' where id IN ('
                . $ignoredid . ')';
            aroundSQL($query);
        }
    }

    # Otherwise, just print the one line
    else {
        printRows(@rows);
    }

    my $end     = time;
    my $elapsed = $end - $start;

    my $elapsedstr
        = 'Took '
        . $elapsed
        . ' seconds to search for information on '
        . $ignoreStats;

    print "\n";

    queryLog( 'INFO', $login, $elapsedstr );

    return;
}

# Get stats on nick
sub nickStats {

    if ( defined($sql_nick) && $sql_nick ne '' ) {
        $nickStats = cleanNicks($sql_nick);
    }

    if ( $nickStats eq "" ) {
        die "No nick provided.\n";
    }

    if ( length($nickStats) > $nickmax ) {
        my $orignick = $nickStats;
        $nickStats = substr( $nickStats, 0, $nickmax );
        queryLog( 'HEADER', $login,
                  $orignick
                . ' may be too long for all stats, truncated to '
                . $nickStats );
    }

    queryLog( 'HEADER', $login,
              'Gathering user statistics on '
            . $nickStats
            . '.  This will be slow..' );
    print STDERR "\n";

    # Filter by channel if specified
    my $channelsql = ' ';
    if ( defined($sql_channel) )
    {
       $channelsql = ' AND CHANNEL = \'' . $sql_channel . '\' ';
    }

    # Nick actions, missing:  Topic
    my @actions = ( 'Say', 'Emote', 'Join', 'Nick', 'Part', 'Quit',
        'Query', 'Notice', 'Mode', 'Topic' );
    # This one requires -actionnick, not -nick
    my @nickactions = ( 'Kick' );
    my $statString  = '';
    my $statIndex   = '0';

    # These actions are tied to the nick column
    foreach my $action (@actions) {
        my $tempsql
            = 'select count(*) as count from '
            . $db{'table'}
            . ' where nick = \''
            . $nickStats
            . '\' and action = \''
            . $action . '\''
            . $channelsql;

        # Limit to timerange if specified
        $tempsql .= $tsSQL;

        $tempsql .= ' order by timestamp asc limit 1;';

        my @rows = doQuery($tempsql);
        $statString
            .= $hl_c
            . $action
            . $r . ': '
            . $hl_c
            . $rows[0]->{'count'}
            . $r . " ";

        # Split the string so it doesn't wrap around at some point
        $statIndex++;
        if ( $statIndex > 5 ) {
            $statIndex = 0;
            $statString .= "\n";
        }
    }

    # These actions are tied to the actionnick column
    foreach my $action (@nickactions) {
        my $tempsql
            = 'select count(*) as count from '
            . $db{'table'}
            . ' where actionnick = \''
            . $nickStats
            . '\' and action = \''
            . $action . '\'';

        # Limit to timerange if specified
        $tempsql .= $tsSQL;

        $tempsql .= ' order by timestamp asc limit 1;';

        my @rows = doQuery($tempsql);
        $statString
            .= $hl_c
            . $action
            . $r . ': '
            . $hl_c
            . $rows[0]->{'count'}
            . $r . " ";

        # Split the string so it doesn't wrap around at some point
        $statIndex++;
        if ( $statIndex > 5 ) {
            $statIndex = 0;
            $statString .= "\n";
        }
    }

    queryLog( 'INFO', $login, $statString );

    my $end     = time;
    my $elapsed = $end - $start;

    my $elapsedstr
        = "Took " . $elapsed . ' seconds to gather stats on ' . $nickStats;

    print "\n";

    queryLog( 'INFO', $login, $elapsedstr );

    return;
}

# Get channel stats on nick
sub listChannels {

    if ( defined($sql_nick) && $sql_nick ne '' ) {
        $listChannels = cleanNicks($sql_nick);
    }

    # Text to put in prints/logs, changes if a list is passed in
    my $logNick = $listChannels;

    if ( $listChannels eq "" ) {
        die "No nick provided.\n";
    }

    # Split a list of nicks out to sane SQL
    my $nickSQL = splitNicks($listChannels, $nickTruncate, $nickmax);

    # If we passed in a list, update logNick to be cleaner
    if ( $listChannels =~ /.*,.*/ )
    {
        my ($tempNick, @foo) = split(/,/,$listChannels);
        my $foo = @foo;
        $logNick = $tempNick . ' (and ' . $foo . ' other nicks)';
    }

    queryLog( 'HEADER', $login,
              'Gathering channel statistics on '
            . $logNick
            . '.  This will be slow..' );
    print STDERR "\n";

    my $tempsql
        = 'select distinct channel from '
        . $db{'table'}
        . ' where ' . $nickSQL
        . ' AND action IN (\'EMOTE\', \'SAY\', \'JOIN\')';

    # Limit to timerange if specified
    $tempsql .= $tsSQL;

    $tempsql .= ';';

    my @rows     = doQuery($tempsql);
    my $rowcount = @rows;

    if ( $rowcount > 0 ) {
        my @channels;
        my $channellist = '';

        # Pull out the channels so we can sort them
        foreach my $row (@rows) {
            my $excluded = '0';
            foreach my $excludedChannel (@excludeChans)
            {
                next unless $excludedChannel;

                $excluded++ if ( $row->{'channel'} =~ $excludedChannel );
            }

            # Push in the channel unless it's in the exclude list
            push( @channels, $row->{'channel'} ) unless $excluded;
        }

        # Sort alphabetically, ignore case
        my @sortedchannels = sort { lc($a) cmp lc($b) } @channels;

        # Count channels below for a real count as there is additional filtering
        my $channelCount = '0';

        # Create string of list
        foreach my $channel (@sortedchannels) {

            # Only include actual channels, no private chats etc
            if ( $channel =~ /^#.*/ ) {
                $channelCount++;
                $channellist .= $channel . ' ';
            }
        }

        queryLog( 'INFO', $login, $hl_c . $channellist . $r );
        print "\n";

        queryLog( 'INFO', $login,
                  'Found '
                . $hl_c
                . $logNick
                . $r . ' in '
                . $hl_c
                . $channelCount
                . $r
                . ' channels' );
        print "\n";
    }
    else {
        queryLog( 'INFO', $login,
            'No channels found for ' . $logNick . ', typo?' );
        print "\n";
    }

    my $end     = time;
    my $elapsed = $end - $start;

    my $elapsedstr
        = 'Took '
        . $elapsed
        . ' seconds to gather channel stats on '
        . $logNick;

    queryLog( 'INFO', $login, $elapsedstr );

    return;
}

# Get activity stats on channel or nick
sub activityStats {
    # Create SQL fragments to assemble
    my ($nickSQL, $channelSQL, $finalSQL);

    # If nick is defined, create the SQL for it
    if ( defined($sql_nick) && $sql_nick ne '' ) {
        if ( length($sql_nick) > $nickmax ) {
            my $orignick = $sql_nick;
            $sql_nick = substr( $sql_nick, 0, $nickmax );
            queryLog( 'HEADER', $login,
                      $orignick
                    . ' may be too long for all stats, truncated to '
                    . $sql_nick );
        }

        $activityStats = cleanNicks($sql_nick);
        $nickSQL = 'nick = \'' . cleanNicks($sql_nick) . '\' ';
    }

    # If channel is defined, create the SQL for it
    if ( defined($sql_channel) && $sql_channel ne '' ) {
        $activityStats = $sql_channel;
        $channelSQL = 'channel = \'' . $sql_channel . '\' ';
    }

    if ($sql_network)
    {
        $finalSQL = 'network = \'' . $sql_network . '\' ';
        $activityStats = $sql_network;
    }
    elsif ( $nickSQL && $channelSQL )
    {
        $finalSQL = $nickSQL . ' AND ' . $channelSQL;
        $activityStats = $sql_nick . ' on ' . $sql_channel;
    }
    elsif ( $nickSQL )
    {
        $finalSQL = $nickSQL;
        $activityStats = $sql_nick;
    }
    elsif ( $channelSQL )
    {
        $finalSQL = $channelSQL;
        $activityStats = $sql_channel;
    }
    else
    {
        die "No nick, channel, or network provided.\n";
    }

    my $queryLine = 'Gathering activity statistics on ' . $activityStats;

    queryLog( 'HEADER', $login,
              $queryLine
            . '.  This will be slow..' );
    print STDERR "\n";

    # We really care about EMOTE SAY QUERY, other modes?
    # JOIN KICK MODE NICK NOTICE PART QUIT TOPIC

    my $tempsql
        = 'select channel,timestamp,message from '
        . $db{'table'}
        . ' WHERE '
        . $finalSQL
        . 'AND (action = \'EMOTE\' '
        . 'OR action = \'SAY\' '
        . 'OR action = \'QUERY\') ';

    # Limit to timerange if specified
    $tempsql .= $tsSQL;

    $tempsql .= ';';

    # Create a consistent string for no data found
    my $noDataString = 'No data found for ' . $activityStats;

    my @rows = doQuery($tempsql);

    my $rowsfound = @rows;

    if ( $rowsfound > 0 ) {

        # Hashes to collect the data in for processing later
        my %yearstats;
        my %monthstats;
        my %hourstats;
        my %daystats;

        # Init the hour stats so they're consistent even if not
        # populated with actual data
        my @hours = (0..23);
        for (@hours)
        {
            # Push in 0's, pad value to two digits as needed
            $hourstats{sprintf("%02s", $_)} = '0';
        }

        # And same for days, 1-7, not 0-6!
        my @days = (1..7);
        for (@days)
        {
            # Push in 0's
            $daystats{"$_"} = '0';
        }

        # And same for months
        my @months = (1..12);
        for (@months)
        {
            # Push in 0's
            $monthstats{sprintf("%02s", $_)} = '0';
        }

        foreach my $row (@rows) {
            my $ts = $row->{'timestamp'};

            # Adjust timezone on this timestamp if needed
            if ( defined($tzAdjust) && $tzAdjust ne "" )
            {
                my $dt = str2dt($ts);
                # Set what timezone this timestamp is currently in
                $dt->set_time_zone($localtz);
                # Set what it should be
                $dt->set_time_zone($tzAdjust);
                $ts = dt2str($dt);
            }

            # See if the channel is in the excluded list
            my $excluded = '0';

            foreach my $excludedChannel (@excludeChans)
            {
                next unless $excludedChannel;

                $excluded++ if ( $row->{'channel'} =~ $excludedChannel );
            }

            # Exclude scripted output
            $excluded++ if ( $row->{'message'} && $row->{'message'} =~ m/$activityIgnoreRegex/ );

            # Only include if it's not excluded
            unless ( $excluded )
            {
                my ( $date, $time )        = split( ' ', $ts );
                my ( $year, $month, $day ) = split( '-', $date );
                my ( $hour, $min, $sec )   = split( ':', $time );
                my $dt  = str2dt($ts);
                my $dow = $dt->day_of_week;

                # Store the data to iterate over later
                $yearstats{$year}++;
                $monthstats{$month}++;
                $hourstats{$hour}++;
                $daystats{$dow}++;
            }
        }

        # Make sure we actually have data before continuing
        my $yearCount = keys %yearstats;
        unless ( $yearCount )
        {
            queryLog( 'INFO', $login, $noDataString );
            return;
        }

        # Must pass hashes as references if local
        print "Year stats\n";
        hashStats( \%yearstats );
        print "\n";

        print "\nMonth stats\n";
        hashStats( \%monthstats, 'month' );
        print "\n";

        print "\nHour stats\n";
        hashStats( \%hourstats );
        print "\n";

        print "\nDay stats\n";
        hashStats( \%daystats, 'day' );
        print "\n\n";

        my $end     = time;
        my $elapsed = $end - $start;

        my $elapsedstr
            = 'Took '
            . $elapsed
            . ' seconds to gather '
            . $rowsfound
            . ' rows of activity stats on '
            . $activityStats;

        queryLog( 'INFO', $login, $elapsedstr );
        print "\n";
    }
    else {
        queryLog( 'INFO', $login, $noDataString );
    }

    return;
}

### Channel stats/queries

# Channel activity stats; ie:  statistics on all users in a specific channel
# a bit like pisg, but only top users, no silly caps or slap stats
# TODO: Refactor this into getting distinct channels and count(*) in a
# loop; would it be faster than parsing every row?
sub channelUserStats {

    if ( defined($sql_channel) && $sql_channel ne '' ) {
        $channelUserStats = $sql_channel;
    }

    if ( $channelUserStats eq "" ) {
        die "No channel provided.\n";
    }

    if ( $channelUserStats !~ /\#.*/ ) {
        queryLog( 'WARN', $login,
            $channelUserStats . ' does not look like a channel?' );
        print "\n";
    }

    # Track total percent
    my $totalPercent = '0';

    # Maximum number of users to analyze by default
    my $maxUsers = '20';

    # How many active nicks during timeframe
    my $activeNicks = '0';

    # Track nick case
    my %nickOrig;

    queryLog( 'HEADER', $login,
              'Gathering channel user statistics on '
            . $hl_c
            . $channelUserStats
            . $r
            . '.  This may be slow for large date ranges..' );
    print STDERR "\n";

    my $noDataString = 'No rows returned.';

    # See if the channel is in the excluded list
    my $excluded = '0';

    foreach my $excludedChannel (@excludeChans)
    {
        next unless $excludedChannel;

        $excluded++ if ( $channelUserStats =~ $excludedChannel );
    }

    # Bail out if excluded
    if ( $excluded )
    {
        queryLog( 'INFO', $login, $noDataString );
        print "\n";
        return;
    }

    my $blackListSQL = '';

    # Exclude any nicks that are blacklisted
    if ( defined($blacklist) ) {

        # Replace any spaces with commas
        $blacklist =~ s/\ /,/g;

        # Split up entries for adding to arrays
        my @nickBL = split( ',', $blacklist );

        # Remove any empty entries, such as double space, double commas, etc
        @nickBL = grep { $_ ne '' } @nickBL;

        my $blList;
        foreach my $blNick (@nickBL)
        {
            $blList .= '\'' . $blNick . '\',';
        }

        # Trip off trailing comma
        $blList =~ s/\,$//;

        # Create SQL substring
        $blackListSQL = ' AND ( nick NOT IN (' . $blList . ') ) ';
    }

    my $tempsql
        = 'select nick from '
        . $db{'table'}
        . ' WHERE channel = \''
        . $channelUserStats . '\' '
        . 'AND (action = \'EMOTE\' '
        . 'OR action = \'SAY\') ';

    if ( defined($sql_nick) ) {
        $tempsql .= 'and nick = \'' . cleanNicks($sql_nick) . '\' ';
    }

    if ( $blackListSQL)
    {
        $tempsql .= $blackListSQL;
    }

    # Limit to timerange if specified
    $tempsql .= $tsSQL;

    $tempsql .= ';';

    my @rows = doQuery($tempsql);

    my $rowsfound = @rows;

    if ( $rowsfound > 0 ) {

# Get number of active users during this timeframe and calculate maxUsers
# if channelPercent has been defined
        if ( defined($channelPercent) && $channelPercent )
        {
            my $countsql
                = 'select count(DISTINCT nick) as count from '
                . $db{'table'}
                . ' WHERE channel = \''
                . $channelUserStats . '\' '
                . 'AND (action = \'EMOTE\' '
                . 'OR action = \'SAY\') ';

            # Limit to timerange if specified
            $countsql .= $tsSQL;

            $countsql .= ';';

            my @countrow = doQuery($countsql);

            $activeNicks = $countrow[0]->{'count'};

            $maxUsers = int ( $activeNicks * ( $channelPercent / 100) );
        }

        foreach my $row (@rows) {
            my $nick = $row->{'nick'};

            # Workaround for potential bug in database
            if ( $nick =~ /^\$/ )
            {
                next;
            }

            # Store lowercase nick so caps changes don't duplicate users
            $/ = '_';
            chomp($nick);
            $nickseen{ lc($nick) }++;
            # Store a reference to the original nick
            $nickOrig{ lc($nick) } = $nick;
        }
    }
    else {
        queryLog( 'INFO', $login, $noDataString );
        print "\n";
        return;
    }

    # If we specified the nick, only need to print the single line
    if ( defined($sql_nick) && $sql_nick ) {

        # Seems inefficient to foreach over a single key, but this way we
        # don't assume anything about capitalization etc
        foreach my $key ( keys %nickseen ) {
            print $hl_c . $nickseen{$key} . ' ' . $key . $r . "\n";
        }
    }

    # Otherwise, print the top stats
    else {
        # Color of the first entry
        my $hl = $hl_c;

        # Other vars we need to use in the loop
        my $usersLength = length($maxUsers);
        my $counter     = '1';
        my $length      = '0';

        # Create a reasonable timestamp range string to append
        my $tsString = '';
        if ( defined($sql_timestamp) && $sql_timestamp ne '' ) {
            $tsString = ' for ' . $sql_timestamp;
            if ( defined($sql_enddate) && $sql_enddate ne '' ) {
                $tsString .= ' to ' . $sql_enddate;
            }
        }

        queryLog( 'INFO', $login,
                  'Top '
                . $hl_c
                . $maxUsers
                . $r
                . ' user statistics on '
                . $hl_c
                . $channelUserStats
                . $r
                . $tsString );
        print "\n";

        # Sort the hash by the largest value
        foreach my $nickKey (
            sort { $nickseen{$b} <=> $nickseen{$a} }
            keys %nickseen
            )
        {
            # Get the biggest value length so we can format nicely
            if ( $length == 0 ) {
                $length = length( $nickseen{$nickKey} );
            }

            # Track the total percent
            $totalPercent += ( $nickseen{$nickKey} / $rowsfound );

            if ( $terse )
            {
                print $nickseen{$nickKey} . "," . $nickKey . "\n";
            }
            else
            {
                my $percent = sprintf( "%.1f", (( $nickseen{$nickKey} / $rowsfound ) * 100 ) + .5);
                printf "%s%${usersLength}d - %${length}d (%4s%%) - %s%s\n", $hl, $counter,
                $nickseen{$nickKey}, $percent, $nickOrig{$nickKey}, $r;
            }
            $counter++;
            $hl = '';

            # Bail out if we've printed out our maximum users
            last if ( $counter > $maxUsers );
        }
    }

    # Create nice percent string
    my $totalPercentString = sprintf("%.1f", $totalPercent * 100);

    # Create optional active nicks string if we specified channelPercent
    # in order to calcualte it
    my $activeNicksString = '';
    $activeNicksString = ' of ' . $hl_c . $activeNicks . $r . ' (' . $channelPercent . '%) active' if ( $activeNicks );

    print "\n";
    queryLog( 'INFO', $login, $hl_c . $maxUsers . $r . $activeNicksString . ' users contributed to ' . $hl_c . $totalPercentString . '%' . $r . ' of the ' . $hl_c . $rowsfound . $r . ' lines said in ' . $hl_c . $channelUserStats . $r );

    return;
}

# Stats for how many times the given user has chatted across channels
# TODO: Refactor this into getting distinct channels and count(*) in a
# loop; would it be faster than parsing every row?
sub userChannelStats {

    if ( defined($sql_nick) && $sql_nick ne '' ) {
        $userChannelStats = cleanNicks($sql_nick);
    }

    if ( $userChannelStats eq "" ) {
        die "No user provided.\n";
    }

    queryLog( 'HEADER', $login,
              'Gathering user channel statistics for '
            . $hl_c
            . $userChannelStats
            . $r
            . '.  This may be slow for large date ranges..' );
    print STDERR "\n";

    my $tempsql;

    # If we pass a splat, don't do any nick, just overall channel stats
    if ( $userChannelStats eq '*' )
    {
        $tempsql
            = 'select channel from '
            . $db{'table'}
            . ' WHERE (action = \'EMOTE\' '
            . 'OR action = \'SAY\' '
            . 'OR action = \'QUERY\') ';
    }
    else
    {
        $tempsql = 'select channel from '
            . $db{'table'}
            . ' WHERE nick = \''
            . $userChannelStats . '\' '
            . 'AND (action = \'EMOTE\' '
            . 'OR action = \'SAY\' '
            . 'OR action = \'QUERY\') ';
    }

    if ( defined($sql_channel) ) {
        $tempsql .= 'and channel = \'' . $sql_channel . '\' ';
    }

    # Limit to timerange if specified
    $tempsql .= $tsSQL;

    $tempsql .= ';';

    my @rows = doQuery($tempsql);

    my $rowsfound = @rows;

    my %channelseen;

    if ( $rowsfound > 0 ) {

        foreach my $row (@rows) {
            my $channel = $row->{'channel'};

            my $excluded = '0';
            foreach my $excludedChannel (@excludeChans)
            {
                next unless $excludedChannel;

                $excluded++ if ( $row->{'channel'} =~ $excludedChannel );
            }

            # Push in the channel unless it's in the exclude list
            unless ( $excluded )
            {
                # Only capture channels, no queries
                if ( $channel =~ /^#.*$/ ) {
                    $channelseen{$channel}++;
                }
            }
        }
    }
    else {
        queryLog( 'INFO', $login, 'No rows returned.' );
        print "\n";
        return;
    }

    # If we specified the channel, only need to print the single line
    if ( defined($sql_channel) && $sql_channel ) {

        # Seems inefficient to foreach over a single key, but this way we
        # don't assume anything about capitalization etc
        foreach my $key ( keys %channelseen ) {
            print $hl_c . $channelseen{$key} . ' ' . $key . $r . "\n";
        }
    }

    # Otherwise, print the top stats
    else {
        # Max number of channels to show
        my $maxChannels = '100';

        # Color of the first entry
        my $hl = $hl_c;

        # Other vars we need to use in the loop
        my $channelsLength = length($maxChannels);
        my $counter        = '1';
        my $length         = '0';

        queryLog( 'HEADER', $login,
                  'Printing top '
                . $hl_c
                . $maxChannels
                . $r
                . ' channel statistics.' );
        print STDERR "\n";

        # Sort the hash by the largest value
        foreach my $chanKey (
            sort { $channelseen{$b} <=> $channelseen{$a} }
            keys %channelseen
            )
        {
            # Get the biggest value length so we can format nicely
            if ( $length == 0 ) {
                $length = length( $channelseen{$chanKey} );
            }

            printf "%s%${channelsLength}d - %${length}d %s%s\n", $hl,
                $counter, $channelseen{$chanKey}, $chanKey, $r;
            $counter++;
            $hl = '';

            # Bail out if we've printed out our maximum channels
            last if ( $counter > $maxChannels );
        }
    }

    return;
}

# What users are common between two channels
sub commonUserStats {

    # Remove any spaces
    $commonUserStats =~ s/\ //g;

    my ( $channelOne, $channelTwo ) = split( /,/, $commonUserStats );

    if ( !defined($channelOne) || !defined($channelTwo) ) {
        die "Must provide both channels for comparison\n";
    }

    queryLog( 'HEADER', $login,
        'NOTE:  This may not be CURRENT nicks, this only looks at nicks that have joined within the given timeframe.  '
            . 'This may be slow for large date ranges..' . "\n\n"
            . 'Gathering common users bewteen '
            . $hl_c
            . $channelOne
            . $r . ' and '
            . $hl_c
            . $channelTwo
            . $r );
    print STDERR "\n";

    # See if the channel is in the excluded list
    my $excluded = '0';

    # Create a single consistent string 
    my $noDataString = 'No rows returned on ' . $channelOne . ' / ' . $channelTwo;

    foreach my $excludedChannel (@excludeChans)
    {
        next unless $excludedChannel;

        $excluded++ if ( $channelOne =~ $excludedChannel ||
        ( $channelTwo =~ $excludedChannel ) );
    }

    # Bail out if excluded
    if ( $excluded )
    {
        # Keep this in sync with the message about 35 lines below!
        queryLog( 'INFO', $login, $noDataString );
        print "\n";
        return;
    }

    # First channel
    my $tempsql
        = 'select distinct nick from '
        . $db{'table'}
        . ' WHERE channel = \''
        . $channelOne . '\' '
        . 'AND action = \'JOIN\' ';

    # Limit to timerange if specified
    $tempsql .= $tsSQL;

    $tempsql .= ';';

    my @rows = doQuery($tempsql);

    my $rowsfound = @rows;

    if ( $rowsfound > 0 ) {

        foreach my $row (@rows) {
            my $nick = $row->{'nick'};

            # Workaround for potential bug in database
            if ( $nick =~ /^\$/ )
            {
                next;
            }

            # Increment the value of this nick so we can compare later
            $nickseen{$nick}++;
        }
    }
    else {
        queryLog( 'INFO', $login, $noDataString );
        print "\n";
        return;
    }

    # Second channel, same as above except against channelTwo
    $tempsql
        = 'select distinct nick from '
        . $db{'table'}
        . ' WHERE channel = \''
        . $channelTwo . '\' '
        . 'AND action = \'JOIN\' ';

    # Limit to timerange if specified
    $tempsql .= $tsSQL;

    $tempsql .= ';';

    @rows = doQuery($tempsql);

    $rowsfound = @rows;

    if ( $rowsfound > 0 ) {

        foreach my $row (@rows) {
            my $nick = $row->{'nick'};
            # Workaround for potential bug in database
            if ( $nick =~ /^\$/ )
            {
                next;
            }
            $nickseen{$nick}++;
        }
    }
    else {
        queryLog( 'INFO', $login,
            'No rows returned on ' . $channelTwo . '.' );
        print "\n";
        return;
    }

    my @nicks;

    foreach my $nick ( sort keys %nickseen ) {
        # If we saw this nick more than once, add it to our final array
        if ( $nickseen{$nick} > 1 ) {
            $nick =~ s/\_$//;
            push( @nicks, $nick );
        }
    }

    my $nickCount = @nicks;

    if ($nickCount) {
        my @uniqueNicks = uniqueArray(@nicks);
        my $nickList = '';
        foreach (@uniqueNicks) {
            $nickList .= $_ . ', ';
        }

        # Strip off the trailing comma
        $nickList =~ s/\,\ $//;

        queryLog( 'INFO', $login, $nickList );

        if ( defined($dumpActivity) ) {
            dumpUsers( @uniqueNicks );
        }
    }

    return;
}

# Re-Query an array of users found during another query as if it's a new
# query
sub dumpUsers
{
    my (@dumpNicks) = @_;

    # De-duplicate array
    my @cleanDumpNicks = uniqueArray(@dumpNicks);

    my $nickCount = @cleanDumpNicks;

    print "\n";

    if ( $nickCount > 0 )
    {
        my $dumpList = join( ' ', @cleanDumpNicks );
        queryLog( 'INFO', $login,
                  'Dumping all activity from these users: '
                . $hl_c
                . $dumpList
                . $r );
        print "\n\n";

        # Bit of a workaround; can't pass -say and -hostname for example,
        # so overload dumpActivity to define a sql_action
        if ( defined($dumpActivity) && $dumpActivity ) {
            $sql_action = $dumpActivity;
        }

        # Clean out previously specified query parameters
        # TBD:  Do more here, hostname is just an example
        # What other possible queries make sense to even combine with dump?
        undef $sql_hostname;
        undef $sql_username;

        # And unset dumpActivity so we don't do this in the secondary query
        undef $dumpActivity;

        # And sql_message
        undef $sql_message;
        
        # And account
        undef $sql_account;

        # Create a list of nicks to build sql against
        $sql_nick = join( ',', @cleanDumpNicks );

        # Re-build our normal sql statement
        $sql = buildSQL();

        # Allow debugging
        if ( $DEBUG ) {
            print "sql: [$sql]\n";
            exit if $DEBUG >= 98;
        }

        # Make sure we have SQL
        if ( $sql eq 'select * from ' . $db{'table'} . ';' ) {
            die 'Incomplete SQL query; invalid/missing parameters?' . "\n";
        }
        else {
            my @dumpRows = doQuery($sql);
            printRows(@dumpRows);
        }
    }
    else
    {
        queryLog( 'INFO', $login, 'No nicks found to dump' );
    }
}

### Utility functions

# Load user-specified config file
sub loadConfig {

    my ($file) = @_;

# If we have a config file parameter passed, load that config file if it exists
    if ($file) {
        if ( -e $file ) {
            queryLog( 'HEADER', $login, 'Loading config file ' . $file );
            print STDERR "\n";
            require $file;
        }
        else {
            queryLog( 'HEADER', $login,
                'Config file ' . $file . ' does not exist, exiting...' );
            exit;
        }
    }

    return;
}

# Anonymize nick row (will not anonymize message data, responses, etc!)
sub anonymizeNick {
    my ($nick) = @_;
    my $fixedNick = '';

    if ( $anonymousNicks{$nick} ) {

        # We already know this nick, just grab the data
        $fixedNick = $anonymousNicks{$nick};
    }
    else {
        # Grab a random nick from our list of anonymous nicks
        my $anonNicks = @anonymousList;
        if ( $anonNicks > 0 ) {

            # Assign this new nick from a random position in our array
            $fixedNick = $anonymousList[ rand($anonNicks) ];

            # Remove it from the array so we don't re-use it
            my $index = 0;
            $index++ until $anonymousList[$index] eq $fixedNick;
            splice( @anonymousList, $index, 1 );
        }
        else {
            # We ran out of anonymous nicks, start creating new ones
            $fixedNick = 'anon' . int( rand(999999) );
        }
        $anonymousNicks{$nick} = $fixedNick;
    }

    return $fixedNick;
}

# Set color values to empty string to avoid uninitialized value issues
sub clearColorVars {
    $ts_c = '';
    $chan_c = '';
    $nick_c = '';
    $anick_c = '';
    $onick_c = '';
    $hn_c = '';
    $mess_c = '';
    $around_c = '';
    $user_c = '';
    $symbol_c = '';
    $at_c = '';
    $hl_c = '';
    $ll_c = '';
    $r = '';
    $c_r = '';
    $c_b = '';
    $bold = '';

    return;
}

# Colorize nicks
sub colorizeNick {
    my ($nick) = @_;

    my $coloredNick = '';

    # Check to see if we have the Term::ANSIColor module so it's not a hard requirement
    my $ansi = eval {
        require Term::ANSIColor;
        Term::ANSIColor->import( ':constants256' );
        1;
    };

    # No ansicolor; just return the nick as we got it
    return $nick unless ( $ansi );

    if ( $colorizedNicks{$nick} ) {

        # We already know this nick, just grab the data
        $coloredNick = $colorizedNicks{$nick};
    }
    else {
        # See if this nick is in highlightWords, if so, use hl_c        
        
        # Lowercase and remove trailing underscores
        my $lcnick = lc($nick);
        $lcnick =~ s/_*$//;

        if ( grep /^\Q$lcnick\E$/, @highlightWords)
        {
            $coloredNick = $hl_c . $nick . $r;
        }
        else
        {
            # Get the numeric value of the lowercased nick to assign
            # it a consistent value
            my @arr = split('', $lcnick);
            my $value = 0;

            foreach my $char (@arr)
            {
                my $ord = ord($char);
                $value += $ord;
            }

            # Modulo at 202, then add in the start value, so we get
            # values from 30-231 for readability
            my $start = '30';
            $value = $value % 202;
            $value += $start;

            my $color = 'ANSI' . $value;
            my $nickColor;
            # Disable refs here temporarily
            {
                no strict 'refs';
                $nickColor = &$color();
            }

            $coloredNick = $nickColor . $nick . $r;
        }

        $colorizedNicks{$nick} = $coloredNick;
    }

    return $coloredNick;
}

# Print stats for activityStats hashes
sub hashStats {
    ( local
 *hash, my $type ) = @_;

    our %hash;
    my $keys = keys %hash;

    # Total up for percentages
    my $total = 0;

    # Find out the maximum value length so we can format nicely
    my $maxlength = 0;

    # Line breaks
    my $output    = 0;
    my $maxoutput = 4;

    # Get the total from the hash to get percents
    foreach my $data ( sort keys %hash ) {
        $total += $hash{$data};
        my $len = length( $hash{$data} );
        if ( $len > $maxlength ) {
            $maxlength = $len;
        }
    }

    foreach my $data ( sort keys %hash ) {
        # Set the default percent color
        my $percentColor = $hl_c;

        my $percentData = $hash{$data} / $total * 100;

        # Lowlight the percent if it's lower than 1%
        if ( $percentData < 1 ) {
            $percentColor = $ll_c;
        }

        # Highlight if greater than 10$
        if ( $percentData > 10 ) {
            $percentColor = $onick_c;
        }

        my $percent = sprintf( "%.1f", $percentData );

        # Break into a new line if we've printed out enouth data already
        if ( $output >= $maxoutput ) {
            print "\n";
            $output = '0';
        }

        if ( defined($type) && $type eq 'month' ) {
            my $month = numToMonth($data);
            printf(
                "%s%s%s [%s%${maxlength}u%s / %s%5s%%%s] ",
                $hl_c,         $month,       $r,
                $hl_c,         $hash{$data}, $r,
                $percentColor, $percent,     $r
            );
        }
        elsif ( defined($type) && $type eq 'day' ) {
            my $day = numToDOW($data);
            printf(
                "%s%s%s [%s%${maxlength}u%s / %s%4s%%%s] ",
                $hl_c,         $day,         $r,
                $hl_c,         $hash{$data}, $r,
                $percentColor, $percent,     $r
            );
        }
        else {
            printf(
                "%s%s%s [%s%${maxlength}u%s / %s%4s%%%s] ",
                $hl_c,         $data,        $r,
                $hl_c,         $hash{$data}, $r,
                $percentColor, $percent,     $r
            );
        }
        $output++;
    }

    return;
}

# Table statistics; requires two queries to get all the data so do it here
sub tableStats {
    # InnoDB rows from table status is very approximate, if you need an
    # accurate row count set this to 1 and it'll do a count(*) (slower!)
    my $realcount = '1';

    my $rowstat   = '';
    my $extratext = '';

    # First timestamp
    my $firstsql = 'select timestamp from chatdb_sphinx order by timestamp asc limit 1;';
    my @firstrows = doQuery($firstsql);
    my ($firstrow) = @firstrows;

    # Generic table stats
    my $statsql    = 'show table status like \'' . $db{'table'} . '\'';
    my @statsrows  = doQuery($statsql);
    my ($statsrow) = @statsrows;

    if ($realcount) {
        queryLog( 'HEADER', $login,
            'Doing real row count, this will be a little slow' );
        print "\n";
        my $countsql  = 'select count(*) as count from ' . $db{'table'} . ';';
        my @countrows = doQuery($countsql);
        my ($countrow) = @countrows;
        $rowstat = $countrow->{'count'};
    }
    else {
        $rowstat   = $statsrow->{'rows'};
        $extratext = ' (approximate)';
    }

    print 'Table statistics' . "\n";
    print 'Create time: ' . $hl_c . $statsrow->{'create_time'} . $r . "\n";
    print 'Update time: ' . $hl_c . $statsrow->{'update_time'} . $r . "\n";
    print 'First row timestamp: ' . $hl_c . $firstrow->{'timestamp'} . $r . "\n";
    $rowstat =~ s/(\d)(?=(\d{3})+(\D|$))/$1\,/g;
    print 'Rows' . $extratext . ': ' . $hl_c . $rowstat . $r . "\n";

    # Size of the database
    my $sizesql
        = 'SELECT table_name AS "Table", ROUND((DATA_LENGTH + INDEX_LENGTH) ';
    $sizesql .= '/ 1024 / 1024) as "Size" from information_schema.tables ';
    $sizesql .= 'where TABLE_SCHEMA = "' . $db{'db'} . '" and TABLE_NAME = "';
    $sizesql
        .= $db{'table'} . '" ORDER BY (DATA_LENGTH + INDEX_LENGTH) DESC;';

    my @sizerows = doQuery($sizesql);
    foreach my $sizerow (@sizerows) {
        if ( $sizerow->{'table'} eq $db{'table'} ) {
            print 'Table size: '
                . $hl_c
                . $sizerow->{'size'} . " MB"
                . $r . "\n";
        }
    }

    return;
}

# Return partial SQL string for time range since this is done in multiple queries
sub tsSQL {
    my $str = ' ';

    # Limit to timerange if specified
    if ( defined($sql_timestamp) && defined($sql_enddate) ) {
        $str
            .= ' and timestamp BETWEEN \''
            . $sql_timestamp
            . '\' AND \''
            . $sql_enddate . '\' ';
    }
    elsif ( defined($sql_timestamp) ) {
        $str .= ' and timestamp LIKE \'' . $sql_timestamp . '\' ';
    }

    # Set stalkerAgeExclude here if needed
    if (   defined($stalkNick)
        && $stalkerAgeExclude
        && $sql_timestamp
        && $sql_enddate )
    {
        $stalkerMaxAge = stalkerAgeFromTS( $sql_timestamp, $sql_enddate );
    }

    return $str;
}

# Test that our data importing is happening as expected
sub testLastImport {

    if ( $testImport =~ /\d+/)
    {
        # Add our optional value for a longer import maximum
        $maxImportDelta += $testImport;
    }

    my $tempsql
        = 'select * from '
        . $db{'table'}
        . ' order by timestamp desc limit 1;';

    my @rows     = doQuery($tempsql);
    my $rowcount = @rows;

    if ( $verbose )
    {
        printRows(@rows);
    }
    
    if ( $rowcount > 0 ) {
        my $ts = $rows[0]->{'timestamp'};

        # Get an epoch time for this row
        my $dt   = str2dt($ts)->set_time_zone('local');
        my $last = $dt->epoch;

        # Get current time and compare them
        my $now   = time;
        my $delta = $now - $last;
        if ( $delta > $maxImportDelta ) {
            queryLog( 'INFO', $login,
                      'testLastImport(): Error; it has been '
                    . $delta
                    . ' seconds since we last saw data (max: '
                    . $maxImportDelta
                    . ")\n" );
            exit 99;
        }
    }
    else {
        print "*** Error: Unable to get a row?\n";
        exit 98;
    }

    return;
}

# Print available flags and some minimal information on each
sub usageHelp {
    print <<"EOH";
List of all flags available to querydb.pl and some terse information.  See README for more details, examples, and usage.

Single or double-dashes should work the same.  Partial options are also fine, such as --de would be fine for --deep.  's' indicates string input, 'i' integer, 'f' is a flag (0/1)

Wrap single quotes around things the shell might mess with, such as --channel '#fedora' or have multiple words like --ts '7 days'

Query parameters to narrow results
--nick (s: nick; shortcut:  -n)
--nt (f: truncate nick if needed)
--filternick (s: nick(s) to filter from query)
--message (s: message text; shortcut: -m)
--match (s: shortcut for using MySQL FullText match in message text)
--channel (s: channel; shortcut: -ch)
--orignick (s: original nick such as nick changes)
--username (s: username of user, such as in join/parts)
--account (s: account name, empty string will show joins without an account)
--hostname (s: hostname of user, such as in join/parts)
--fuzzyhost (i: make lookups fuzzier; replace with wildcards)
--network (s: IRC network)
--action (s: user actions such as join, say, emote etc)
--say (f: shortcut for --action say)
--actionnick (s: user action was taken upon, such as a ban or kick)
--highlights (s: show highlight terms in messages)
--highlightsonly (flag: only show messages matching highlights)
--timestamp (s: specify a timestamp such as a fuzzy string like '7 days' or a specific time stamp like '2018-03-01')
--ts (s: alias of timestamp)
--tsrange (s: specify timestamp range such as "2018-03-01 2018-03-03")
--today (f: alias for -ts today)
--1hour (f: alias for -ts '1 hour', also --onehour works)
--tzadjust (s: Adjust the timestamp shown on results, -0400 or "America/Louisville")
--relative (f: Show relative time delta in lines)
--nojoinpart (f: Disable join/part/quit messages)
--nj (f: alias for -nojoinpart)
--nonick (f: Hide all nick changes)
--aroundmins (i: Show N minutes around match)

Search for discussion around a match
--around (i: show N lines of information around match)
--aroundchat (f: show continuous log rather than broken into sections and removes match limits)

Search for a conversation between two nicks
--convo (s: specify nicks separated by a comma such as "nick1,nick2")

Options to organize, limit or sort results
--limit (i: adjust the limit of the number of results)
--order (s: order by a specific column)
--sort (s: asc or desc)
--ascending (f: shortcut for --sort asc)
--descending (f: shortcut for --sort desc)
--channelgroup (f: group results into channels)
--ignoredonly (f: only show ignored users)
--excludechannels (s: Comma-separated regex of channels to filter out)

Sphinx options to limit or change sorting within Sphinx
--sphinx (f: over-ride default of using Sphinx or not)
--weight (f: force the use of Sphinx's weight over time sort)
--sphinxresults (integer; over-ride the maximum nummber of Sphinx results)

Stalker options
--stalker (s: stalk given nick)
--maxhosts (i: limit the number of hosts to iterate through)
--maxnicks (i: limit the number of nicks to iterate through)
--maxloops (i: limit the number of loops looking for new nicks and/or hosts)
--deep (i: find new matching nicks every loop rather than just the first loop, specify value to over-ride the default value of $defaultDeepLoops)
--onlynicks (f: limit matches to only nick changes, no hostname matches)
--onlyhosts (f: limit matches only to show nicks used by host, ie: joins only)
--blacklist (s: blacklist additional hosts and nicks (applies to both!))
--noblacklist (f: Disable all blacklisting)
--cleanstalker (f: Clean stalker log before stalking)
--excludeage (i: Exclude hits due to age, 0 to disable)
--maxage (i: Stalker max age)

Abuse Detection
--clonescan (f: Find multiple users coming from the same host)
--clones (s: Adjust how many nicks it requires to return a result)
--proxycheck (i: Check a host IP for being part of a proxy)
--pc (i: alias of proxycheck)

Get a list of similar nicks
--nicksearch (s: do a fuzzy search for a nick, useful to find the exact nick for a query)

Stats/Overall info
--first (i: show the first N lines in the database table for a match, default 1)
--last (i: show the last N lines in the database table for a match, default 1)
--stats (f: show some table statistics)
--ignorestats (s: statistics around the ignored user)
--nickstats (s: high level chat stats on user such as number of say, quit, etc)
--listchannels (s: list of channels nick has been seen in)
--channeluserstats (s: User statistics of a given channel)
--channelpercent (s: Optional percentage of users to consider in channeluserstats)
--userchannelstats (s: Channel statistics of a given user)
--activitystats (f: chat time, days, years activity for user and/or channel, or network)
--commonuserstats (s: #channel1,#channel2 common users between two channels)
--countsounds (f: Count sound bytes in channel)

Specialized options
--verbose (f: increase output verbosity in some commands)
--terse (f: decrease output verbosity in some commands)
--sql (s: do a table query of the given sql)
--columns (s: specify exact columns of data to be returned, no pretty print)
--color (f: over-ride coloroutput value in chatdb.pm)
--anonymous (f: Give users anonymous nicks instead of their real nick)
--colornicks (f: over-ride colornick value in chatdb.pm)
--dumpactivity (s: Dump activity of returned results; optional secondary action)
--testimport (i: Validate that we are importing data regularly, with optional value to extend allowed tim)
--debug (i: Change debug level)
--nolog (f: Don't log query into query log)
--noheaders (f: Don't print headers/footers)
--noprint (f: Don't print rows, just return rows matched)
EOH

    return;
}
